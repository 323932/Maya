@extends('admin.layout.layout')
@section('start_form')
    <?= Form::open(['role'=>'form','class'=>'m-0','files'=>true,'method'=>'POST','id'=>'order_form_data']) ?>
@stop
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <?= Html::style('backend/css/datepicker.css') ?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and new">Save & New</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop 
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('user_id')) {{ 'has-error' }} @endif">
                            <label>User <sup class="text-danger">*</sup></label>
                            <?=Form::select('user_id',$user,old('user_id'), ['class' => 'form-control null_value_class select2_2 user_id','id' => 'user_id','placeholder'=>'Select User']);?>
                            <span id="user_id_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('payment_type')) {{ 'has-error' }} @endif">
                            <label>Payment Type <sup class="text-danger">*</sup></label>
                            <?=Form::select('payment_type',['bank transfer'=>'Bank Transfer','check'=> 'Check'],old('payment_type'), ['class' => 'form-control null_value_class select2_2 payment_type','id' => 'payment_type','placeholder'=>'Select Payment Type']);?>
                            <span id="payment_type_error" class="help-inline text-danger"><?= $errors->first('payment_type') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <label>Company Name</label>
                            <?= Form::text('name', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Name','id'=>'name','readOnly'=>true]); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <label>Email</label>
                            <?= Form::text('email', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Email','id'=>'email','readOnly'=>true]); ?>
                            <span id="email_error" class="help-inline text-danger"><?= $errors->first('email') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('mobile')) {{ 'has-error' }} @endif">
                            <label>Mobile</label>
                            <?= Form::text('mobile', null, ['class' => 'form-control null_value_class number_only', 'placeholder' => 'Mobile','maxlength'=>10,'id'=>'mobile','readOnly'=>true]); ?>
                            <span id="mobile_error" class="help-inline text-danger"><?= $errors->first('mobile') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('gstno')) {{ 'has-error' }} @endif">
                            <label>GST No</label>
                            <?= Form::text('gstno', null, ['class' => 'form-control null_value_class', 'placeholder' => 'GST','id'=>'gstno','readOnly'=>true]); ?>
                            <span id="gstno_error" class="help-inline text-danger"><?= $errors->first('gstno') ?></span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Product Detail</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group ">
                        <div class=" table-responsive" id="order_variant">
                            <table class="table m-0 v-top verticle-align">
                                <thead>
                                    <tr>
                                        <th style="border:none;" width="150px">Brand<sup class="text-danger">*</sup></th>
                                        <th style="border:none" width="150px">Category<sup class="text-danger">*</sup></th>
                                        <th style="border:none" width="150px">Product<sup class="text-danger">*</sup></th>
                                        <th style="border:none" width="150px">Color<sup class="text-danger">*</sup></th>
                                        <th id="blank_thead" style="display: none;"></th>

                                        <th style="border:none"></th>
                                        <th style="border:none"></th>
                                        <th style="border:none"></th>
                                        <th style="display: none;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="product">
                                        <td style="border:none" valign="top">
                                            <?=Form::select('brand_id',$brand,old('brand_id'), ['class' => 'form-control null_value_class brand_id','id' => 'brand_id','placeholder'=>'Select Brand','style'=>'width:150px']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.brand_id') ?></span>
                                        </td>
                                        <td style="border:none" valign="top">
                                            <?=Form::select('category_id',$category,old('category_id'), ['class' => 'form-control null_value_class category_id','id' => 'category_id','placeholder'=>'Select Category','style'=>'width:150px']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.category_id') ?></span>
                                        </td>
                                        <td style="border:none" valign="top">
                                            <?=Form::select('product_id',$product,old('product_id'), ['class' => 'form-control null_value_class product_id','id' => 'product_id','placeholder'=>'Select Product','style'=>'width:150px']);?>
                                            <span id="product_id__error" class="help-inline text-danger"><?= $errors->first('product.product.product_id') ?></span>                     
                                        </td>
                                        <input type="hidden" class="without_set" name="without_set" id="without_set" value="">
                                            <input type="hidden" class="price" name="price" id="price" value="">
                                            <input type="hidden" class="percentage" name="percentage" id="percentage" value="">
                                            <input type="hidden" class="tax" name="tax" id="tax" value="">
                                        <td style="border:none" valign="top" id="tmp_color_div">
                                            <?=Form::select('color_id',$color,old('color_id'), ['class' => 'form-control null_value_class category_id','id' => 'color_id','placeholder'=>'Select Color','style'=>'width:150px']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.color_id') ?></span>
                                        </td>        
                                        <td id="blank" style="display: none;"></td>
                                        <td style="border:none" valign="top">
                                            <?=Form::hidden('row_price','', ['class' => 'form-control null_value_class row_price','id' => 'row_price','placeholder'=>'Select Product','style'=>'width:150px']);?>                    
                                        </td>
                                        <td style="border:none" valign="top">
                                            <?=Form::hidden('tax_price','', ['class' => 'form-control null_value_class tax_price','id' => 'tax_price','placeholder'=>'Select Product']);?>                     
                                        </td>
                                        <td style="border:none;width:60px" valign="top">
                                            <a id="product_remove" onmouseup="btnRemove();" class="pt-10 pull-left btn-remove" style="width:20px"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                            <a id="product_add" onmouseup="btnAdd();" class="pt-10 pull-left btn-add" style="width:20px"><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-md-2" style="float:right">
                                <div class="form-group @if($errors->has('gstno')) {{ 'has-error' }} @endif">
                                    <label>Grand Total</label>
                                    <?= Form::text('grand_total', null, ['id' => 'grand_total','class' => 'form-control null_value_class', 'placeholder' => 'Grand Total','readOnly'=>true]); ?>
                                </div>
                            </div>
                            <div class="col-md-2" style="float:right">
                                <div class="form-group @if($errors->has('tax')) {{ 'has-error' }} @endif">
                                    <label>Tax</label>
                                    <?=Form::text('tax',old('tax'), ['class' => 'form-control null_value_class tax','id' => 'main_tax','readOnly'=>true,'placeholder'=>'Tax']);?>
                                    <span id="tax_error" class="help-inline text-danger"><?= $errors->first('tax') ?></span>
                                </div>
                            </div>
                            <div class="col-md-2" style="float:right">
                                <div class="form-group @if($errors->has('order_price')) {{ 'has-error' }} @endif">
                                    <label>Order Price</label>
                                    <?= Form::text('order_price', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Order Price','data-price'=>'@i.Price','id'=>'order_price','readOnly'=>true]); ?>
                                    <!-- <span id="order_price"></span> -->
                                    <span id="order_price_error" class="help-inline text-danger"><?= $errors->first('order_price') ?></span>
                                    <input type="hidden" name="oprice" value="" id="oprice">
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Billing Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_address')) {{ 'has-error' }} @endif">
                                    <label>Address</label>
                                    <?= Form::textarea('billing_address', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Address','rows'=>2,'id'=>'billing_address','readOnly'=>true]); ?>
                                    <span id="billing_address_error" class="help-inline text-danger"><?= $errors->first('billing_address') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_area')) {{ 'has-error' }} @endif">
                                    <label>Area</label>
                                    <?= Form::text('billing_area', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Area','id'=>'billing_area','readOnly'=>true]); ?>
                                    <span id="billing_area_error" class="help-inline text-danger"><?= $errors->first('billing_area') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"> 
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_state')) {{ 'has-error' }} @endif">
                                    <label>State</label>
                                     <?=Form::text('billing_state',old('billing_state'), ['class' => 'form-control status null_value_class','id' => 'billing_state','placeholder'=>'Select State','id'=>'billing_state','readOnly'=>true]);?>
                                    <span id="billing_state_error" class="help-inline text-danger"><?= $errors->first('billing_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_city')) {{ 'has-error' }} @endif">
                                    <label>City</label>
                                     <?=Form::text('billing_city',old('billing_city'), ['class' => 'form-control status null_value_class','id' => 'billing_city','placeholder'=>'Select City','id'=>'billing_city','readOnly'=>true]);?>
                                    <span id="billing_city_error" class="help-inline text-danger"><?= $errors->first('billing_city') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode</label>
                                    <?= Form::text('billing_pincode', null, ['class' => 'form-control null_value_class number_only', 'placeholder' => 'Pincode','maxlength'=>6,'id'=>'billing_pincode','readOnly'=>true]); ?>
                                    <span id="billing_pincode_error" class="help-inline text-danger"><?= $errors->first('billing_pincode') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Shipping Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_address')) {{ 'has-error' }} @endif">
                                    <label>Address<sup class="text-danger">*</sup></label>
                                    <?= Form::textarea('shipping_address', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Address','rows'=>2]); ?>
                                    <span id="shipping_address_error" class="help-inline text-danger"><?= $errors->first('shipping_address') ?></span>
                                </div>
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_area')) {{ 'has-error' }} @endif">
                                    <label>Area<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_area', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Area']); ?>
                                    <span id="shipping_area_error" class="help-inline text-danger"><?= $errors->first('shipping_area') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_state')) {{ 'has-error' }} @endif">
                                    <label>State<sup class="text-danger">*</sup></label>
                                     <?=Form::select('shipping_state',$state,old('shipping_state'), ['class' => 'form-control select2_2 status null_value_class','id' => 'shipping_state','placeholder'=>'Select State']);?>
                                    <span id="shipping_state_error" class="help-inline text-danger"><?= $errors->first('shipping_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_city')) {{ 'has-error' }} @endif">
                                    <label>City<sup class="text-danger">*</sup></label>
                                     <?=Form::select('shipping_city',$shipping_city,old('shipping_city'), ['class' => 'form-control select2_2 status null_value_class','id' => 'shipping_city','placeholder'=>'Select City']);?>
                                    <span id="shipping_city_error" class="help-inline text-danger"><?= $errors->first('shipping_city') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_pincode', null, ['class' => 'form-control number_only null_value_class', 'placeholder' => 'Pincode','maxlength'=>6]); ?>
                                    <span id="shipping_pincode_error" class="help-inline text-danger"><?= $errors->first('shipping_pincode') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and new">Save & New</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?=Html::script('backend/js/jquery.fileuploader.min.js')?>
    <?=Html::script('backend/js/bootstrap-datepicker.js')?> 
    <?= Html::script('backend/js/jquery.form.min.js') ?>
    <?= Html::script('backend/js/dynamicform.js') ?>  
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>

    <script type="text/javascript">
        var tag1 = $('#user_id').select2({
            placeholder : "Select User"
    
        });
        var tag2 = $('#payment_type').select2({
            placeholder : "Select Payment Type"
    
        });
        var tag3 = $('#shipping_state').select2({
            placeholder : "Select State"
    
        });
        var tag4 = $('#shipping_city').select2({
            placeholder : "Select City"
    
        });

    </script> 

    <!-- DYNAMIC FORM -->
    <script type="text/javascript">
        var product_detail =  $("#product").dynamicForm("#product_add", "#product_remove", {
            limit: 10,
            normalizeFullForm : false,
        });

        function btnAdd(){
            $(this).delay(100).queue(function() {
                $(this).hide();
                
                get_logic_data();

                logic_order_calculation();

                // product_change();
                
                // calculation();
                $(this).dequeue();

                var select_list = $('tbody tr');

            }); 
        }
        function btnRemove(){
            $(this).delay(100).queue(function() {
                $(this).hide();
                
                logic_order_calculation_remove();

                $(this).dequeue();
            });
            // get_logic_data();
        }
        old_data = <?= json_encode(old('product.product')) ?>;
        // 
        product_detail.inject(old_data);

        var select_list = $('tbody tr');

        @if($errors)
            var detail_Errors = <?= json_encode($errors->toArray()) ?>;        
            $.each(detail_Errors, function(id,msg){
                var id_arr = id.split('.');
                if (id_arr[3] == 'brand_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'category_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'product_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'color_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'row_price') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'tax_price') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
            });
        @endif        
    </script> 

    <!-- BRANDWISE SIZE --> 
   <script type="text/javascript">
        $(document).ready(function(){
           $('body').on('change','.brand_id',function (){
                var id = $(this).attr('id');
                var b_id = id.split('_');
                var brand_id = b_id[1].substr(2,1);
               //console.log(brand_id);
                var a = $(this).parent().parent().attr('id');
               loadSize($(this).val(),a,brand_id);
            });

            function loadSize(val,a,brand_id)
            {
               // alert('hi');
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('orders.getsize')?>",
                    type       : 'post',
                    data       : { "brand_id": val , '_token' : token,'b_id':brand_id },
                    dataType   : 'html',
                    success    : function(get_sizeIn) {
                        var data = JSON.parse(get_sizeIn);
                        //console.log(data['result']);
                        $('#'+a).find('#blank').show();
                        $('#'+a).find('#blank').empty();
                        $('#'+a).find('#blank').append(data['result']);
                        // console.log($('#'+a).parent().parent().children().find('#blank_thead'));
                        $('#'+a).parent().parent().children().find('#blank_thead').show();
                        $('#'+a).parent().parent().children().find('#blank_thead').empty();
                        $('#'+a).parent().parent().children().find('#blank_thead').append(data['title']);           
                    }
                });
            }
        });
    </script>

    <!-- DATEPICKER AND NUMBERONLY JS -->
    <script type="text/javascript">
        $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&& 32!=h.which&&(h.which<48||h.which>57))return!1});

        var currentDate = new Date();  
            $('#delivery_date').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true,
                todayHighlight: true,
                // endDate:currentDate,
            });
    </script>

    <!-- STATEWISE CITY IN BILLING --> 
    <script type="text/javascript">
        $(document).ready(function(){
            $("#billing_state").on('change',function() {
               loadCity($(this).val());
            });

            function loadCity(val)
            {
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('orders.getcity')?>",
                    type       : 'post',
                    data       : { "billing_state": val , '_token' : token },
                    dataType   : 'html',
                    success    : function(cities) {
                        var city_options = $(cities).html();
                        $('#billing_city').val('').trigger('change');
                        $("#billing_city").html(city_options).val("{{ old('billing_city') }}");
                    }
                });
            }
            // STATEWISE CITY IN SHIPPING
            $("#shipping_state").on('change',function() {
                // alert('hi'); 
               loadShippingCity($(this).val());
            });

            function loadShippingCity(val)
            {
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('orders.shippingcity')?>",
                    type       : 'post',
                    data       : { "shipping_state": val , '_token' : token },
                    dataType   : 'html',
                    success    : function(shippingcities) {
                        var shipping_city_options = $(shippingcities).html();
                        //
                        $('#shipping_city').val('').trigger('change');
                        $("#shipping_city").html(shipping_city_options).val("{{ old('shipping_city') }}");
                    }
                });
            }

        });
    </script>
    
     <script type="text/javascript">
        var o_price = 0;
        var o_per_price = 0;
        var t_price = 0;
        function logic_order_calculation(){
            var select_list = $('tbody tr');
            $.each(select_list,function(k,v){
               
                $('#brand_id'+k).change(function(key,value){
                    var id = $(this).attr('id');
                    var b_id = id.split('_');
                    var brand_id = b_id[1].substr(2,1);
                    
                    setTimeout(function(){
                        var qty = [];
                        var o_val = [];
                        var total_qty_id = [];
                        $.each($("input[name='qty_"+brand_id +"[]']"),function(q_k,q_v){
                            var qty_id = $(q_v).attr('id');
                            total_qty_id.push(qty_id);
                            
                        });
                        $('.qty_'+brand_id).keyup(function(e){
                            var code = e.keyCode || e.which;
                                if (code == '9') {
                                    return false;
                                }
                            var without_set_value = $(v).find('.without_set').val();
                            
                            if(without_set_value == '1'){
                                var remove_qty_price = $('#row_price'+k).val();
                                var remove_tax_price = $('#tax_price'+k).val();
                                var qty_value = $(this).val();
                                var qty_length = total_qty_id.length;
                                
                                $(v).find('.qty_'+brand_id).val(qty_value);
                                
                                var sum_qty = 0;
                                for(i=0;i<qty_length;i++){
                                    sum_qty = sum_qty + parseInt(qty_value);
                                }

                                var total_row_price = sum_qty * ($(v).find('.price').val());
                                    if(isNaN(total_row_price)){

                                        total_row_price = 0;
                                    }
    
                                var total_row_tax_price = ($(v).find('.tax').val() * total_row_price) /100; 
                                var total_price = total_row_price + total_row_tax_price;
    
                                $(v).find('.row_price').val(total_row_price);  
                                $(v).find('.tax_price').val(total_row_tax_price); 

                                var order_price = $(v).find('.row_price');
                                $.each(order_price,function(o_k,o_v){

                                    o_price = o_price + parseInt($(o_v).val()) - remove_qty_price;

                                    if(isNaN(o_price)){

                                        o_price = 0;
                                    }
                                     
                                });
                                $('#order_price').val(o_price);
                                $('#oprice').val(o_price);

                                var tax_price = $(v).find('.tax_price');
                                $.each(tax_price,function(t_k,t_v){
                                
                                    t_price = t_price + parseInt($(t_v).val()) - remove_tax_price;
                                
                                    if(isNaN(t_price)){

                                        t_price = 0;
                                    }
                                });
                                $('#main_tax').val(t_price);
                                
                                var grand_total = parseInt($('#order_price').val()) + parseInt($('#main_tax').val());
                                $('#grand_total').val(grand_total);
                            }
                            else{
                                row_price_sum = 0;
                                total_row_without_tax_price = 0;
                                total_row_tax_price = 0;
                                order_price_arr= [];
                                qty = [];
                                total_row_price_arr = [];
                                qty_minus_arr = [];
                                total_row_without_tax_price_arr = [];
                                total_row_tax_price_arr = [];
                                var id_length = $(v).find('.qty_'+brand_id);
                                for (var i = 0; i < id_length.length; i++) {
                                    var q  = $('#qty_'+i+'_'+brand_id).val();
                                    qty.push(q);
                                    if(isNaN(q)){
                                        break;
                                    }
                                    var total_row_price = q * ($(v).find('.price').val());
                                     total_row_price_arr.push(total_row_price)

                                }
                                     console.log(total_row_price_arr);
                                for (var i = 0; i < qty.length; i++) {
                                        var min_qty = Math.min.apply(Math,qty);
                                        var qty_minus = parseInt( (qty[i]) - min_qty);
                                            if (isNaN(qty_minus)) {
                                                qty_minus = 0;
                                            }
                                            qty_minus_arr.push(qty_minus);
                                }
                                    console.log(qty_minus_arr);
                                for (i =0; i < qty_minus_arr.length; i++) {
                              
                                    var extra_percentage_qty_price = qty_minus_arr[i] * ($(v).find('.price').val());//unset total qty wdprice 3*1000=3000
                              
                                    if(isNaN(extra_percentage_qty_price)){
                                        var total_row_percentage_price = 0;
                                    }
                                    else{
                                        var total_row_percentage_price = ($(v).find('.percentage').val() * extra_percentage_qty_price) /100; //unset qty wd price 3000*10 /100 = 300
                                    }
                                    
                                    var total_row_without_tax_price_val = total_row_price_arr[i] +total_row_percentage_price;
                                    var total_row_tax_price_val = ($(v).find('.tax').val() * total_row_without_tax_price_val) /100; 
                                    var total_price = total_row_without_tax_price_val + total_row_tax_price_val;
                                    
                                    // all value push in array after sum of all order size value
                                    total_row_without_tax_price_arr.push(total_row_without_tax_price_val);
                                    total_row_tax_price_arr.push(total_row_tax_price_val);
                                }

                                // sum of all array value for total row price
                                for (var i = 0; i < total_row_price_arr.length; i++) {
                                  row_price_sum += total_row_price_arr[i]
                                }
                                
                                // sum of all array value for total row without tax price
                                for (var i = 0; i < total_row_without_tax_price_arr.length; i++) {
                                  total_row_without_tax_price += total_row_without_tax_price_arr[i]
                                }
                                // sum of all array value for total row tax price
                                for (var i = 0; i < total_row_tax_price_arr.length; i++) {
                                  total_row_tax_price += total_row_tax_price_arr[i]
                                }
                                
                                // minus old  qty value after change the row value
                                var remove_qty_price = $('#row_price'+k).val();
                                
                                // minus old tax value after change the row value
                                var remove_tax_price = $('#tax_price'+k).val();
                                
                                // sum of all row value  append in the hidden field
                                $(v).find('#row_price'+k).val(total_row_without_tax_price);  
                                $(v).find('#tax_price'+k).val(total_row_tax_price);  

                                // get order value from hidden field
                                var order_price = $(v).find('#row_price'+k).val();

                                // all order value push in the array
                                order_price_arr.push(order_price);
                                
                                // sum of all multiple row total value push in array
                                o_val.push(order_price_arr.join(""));
                                
                                // use for get first index of array
                                for (i =1; i <o_val.length ; i++) {
                                    o_val.splice(0, 1);
                                }

                                o_price = o_price + parseInt(o_val[0]) - remove_qty_price;
                                
                                $('#order_price').val(o_price);
                                $('#oprice').val(o_price);

                                var tax_price = $(v).find('#tax_price'+k).val();
                                t_price = t_price + parseInt(tax_price) - remove_tax_price;
                                $('#main_tax').val(t_price);

                                var grand_total = parseInt($('#order_price').val()) + parseInt($('#main_tax').val());
                                $('#grand_total').val(grand_total);
                            }
                        }); 
                    },300);
                });
            });
        }
        
        logic_order_calculation();  

    </script>
    <script type="text/javascript">
        function logic_order_calculation_remove(){
        var o_price = 0;
        var t_price = 0;
            var select_list = $('tbody tr');
            $.each(select_list,function(k,v){
                var order_price = $(v).find('.row_price');
                $.each(order_price,function(o_k,o_v){
                    o_price = o_price + parseInt($(o_v).val());
                });
                $('#order_price').val(o_price);
                $('#oprice').val(o_price);

                var tax_price = $(v).find('.tax_price');
                $.each(tax_price,function(t_k,t_v){
                    t_price = t_price + parseInt($(t_v).val());
                });
                $('#main_tax').val(t_price);

                var grand_total = parseInt($('#order_price').val()) + parseInt($('#main_tax').val());
                $('#grand_total').val(grand_total);
            });
        }
    </script>

     <!-- LOGIC FOR ORDER -->
    <script type="text/javascript">
        $(document).ready(function() {
            get_logic_data();
        });
        var logic_data = [];
        var small_array = [];
        var medium_array = [];
        var large_array = [];
        var extra_large_array = [];

        function get_logic_data(){
            var loop = [];
            $('.qty_s').keyup(function() {
                var this_s = $(this).parent('td').parent('tr');
                var resp = setDataLogic($(this).val(),this_s);
                $('#order_price').val(resp.total_price);
                $('#main_tax').val(resp.tax);
                $('#grand_total').val(resp.tax_total_price);
                return resp;
            });
            $('.qty_m').keyup(function() {
                var this_m = $(this).parent('td').parent('tr');
                var resp = setDataLogic($(this).val(),this_m);
                $('#order_price').val(resp.total_price);
                $('#main_tax').val(resp.tax);
                $('#grand_total').val(resp.tax_total_price);
                return resp;
            });
            $('.qty_l').keyup(function() {
                var this_l = $(this).parent('td').parent('tr');
                var resp = setDataLogic($(this).val(),this_l);
                $('#order_price').val(resp.total_price);
                $('#main_tax').val(resp.tax);
                $('#grand_total').val(resp.tax_total_price);
                return resp;
            });
            $('.qty_xl').keyup(function() {
                var this_xl = $(this).parent('td').parent('tr');
                var resp = setDataLogic($(this).val(),this_xl);
                $('#order_price').val(resp.total_price);
                $('#main_tax').val(resp.tax);
                $('#grand_total').val(resp.tax_total_price);
                return resp;
            });
            $(".product_id").on('change',function() {
                
                var token = "{{csrf_token()}}";
                var this_product = $(this);
                var a = $(this_product).attr('id');
                var id_raf = a.substr(a.length - 1);
                $('#qty_s'+id_raf).val(0);
                $('#qty_m'+id_raf).val(0);
                $('#qty_l'+id_raf).val(0);
                $('#qty_xl'+id_raf).val(0);
                
                $.ajax({
                    url        : "<?=URL::route('orders.getproductdata')?>",
                    type       : 'post',
                    data       : { "product_id": $(this).val() , '_token' : token },
                    dataType   : 'json',
                    success    : function(resp) {
                        var product_fields = resp.product_fields;
                        var percentage = product_fields.percentage;
                        var tax = product_fields.tax;
                        if(percentage == null){
                            percentage = 0.0;
                        }
                        $('#without_set'+id_raf).val(product_fields.without_set);
                        $('#percentage'+id_raf).val(percentage);
                        $('#price'+id_raf).val(product_fields.original_price);
                        $('#tax'+id_raf).val(tax);
                    }
                });
            });
        }

        function setDefaultLogic(){
            var temp_array = [];
            var select_list = $('tbody tr');
            $.each(select_list, function(k,v){
                // console.log(k);
                // console.log($(v).find('.qty_s').val());
                var temp = [];
                // temp['small'] = $(v).find('.qty_s').val();
                temp['small'] = ($(v).find('.qty_s').val() != 0) ? $(v).find('.qty_s').val() : 0;
                temp['medium'] = ($(v).find('.qty_m').val() != 0) ? $(v).find('.qty_m').val() : 0;
                temp['large'] = ($(v).find('.qty_l').val() != 0) ? $(v).find('.qty_l').val() : 0;
                temp['extra_large'] = ($(v).find('.qty_xl').val() != 0) ? $(v).find('.qty_xl').val() : 0;
                // temp['small'] = ($('#qty_s'+k).val() != 0) ? $('#qty_s'+k).val() : 0;
                // temp['medium'] = ($('#qty_m'+k).val() != 0) ? $('#qty_m'+k).val() : 0;
                // temp['large'] = ($('#qty_l'+k).val() != 0) ? $('#qty_l'+k).val() : 0;
                // temp['extra_large'] = ($('#qty_xl'+k).val() != 0) ? $('#qty_xl'+k).val() : 0;
                temp['without_set'] = $(v).find('.without_set').val();
                // temp['without_set'] = $('#without_set'+k).val();
                
                var val = temp['small'];
                if(temp['without_set'] == '1'){
                    temp['small'] = val;
                    temp['medium'] = val;
                    temp['large'] = val;
                    temp['extra_large'] = val;

                    $(v).find('.qty_s').val(val);
                    $(v).find('.qty_m').val(val);
                    $(v).find('.qty_l').val(val);
                    $(v).find('.qty_xl').val(val);

                    // $('#qty_s'+k).val(val);
                    // $('#qty_m'+k).val(val);
                    // $('#qty_l'+k).val(val);
                    // $('#qty_xl'+k).val(val);
                }
                temp['percentage'] = $(v).find('.percentage').val();
                temp['price'] = $(v).find('.price').val();
                temp['tax'] = $(v).find('.tax').val();
                //console.log(temp);
                // temp['percentage'] = $('#percentage'+k).val();
                // temp['price'] = $('#price'+k).val();
                // temp['tax'] = $('#tax'+k).val();
                temp_array[k] = temp;
            });
            logic_data = temp_array;
            var total_price = logic(logic_data);
            return total_price;
        }

        function setDataLogic(val,this_v){
            var temp_array = [];
            var select_list = $('tbody tr');
            $.each(select_list, function(k,v){
                var temp = [];
                // 
                // 
                temp['small'] = ($('#qty_s'+k).val() != 0) ? $('#qty_s'+k).val() : 0;
                temp['medium'] = ($('#qty_m'+k).val() != 0) ? $('#qty_m'+k).val() : 0;
                temp['large'] = ($('#qty_l'+k).val() != 0) ? $('#qty_l'+k).val() : 0;
                temp['extra_large'] = ($('#qty_xl'+k).val() != 0) ? $('#qty_xl'+k).val() : 0;
                temp['without_set'] = this_v.find('.without_set').val();
                
                if(temp['without_set'] == '1'){
                    console.log('done');
                    temp['small'] = val;
                    temp['medium'] = val;
                    temp['large'] = val;
                    temp['extra_large'] = val;
                    $(this_v.find('.qty_s')).val(val);
                    $(this_v.find('.qty_m')).val(val);
                    $(this_v.find('.qty_l')).val(val);
                    $(this_v.find('.qty_xl')).val(val);
                }
                temp['percentage'] = $('#percentage'+k).val();
                temp['price'] = $('#price'+k).val();
                temp['tax'] = $('#tax'+k).val();
                temp_array[k] = temp;
                // 
            });
            logic_data = temp_array;
            var total_price = logic(logic_data);
            return total_price;
        }

        function logic(logic_data){
            var logic_data = logic_data;
            var minValue = [];
            var total_price = 0;
            var add_tax_total_price = 0;
            var tax = 0;
            var response = [];
            $.each(logic_data, function(key,value){
                var smallest = value['small'];
                var check_small = [parseInt(value['small']),parseInt(value['medium']),parseInt(value['large']),parseInt(value['extra_large'])];
                minValue[key] = Math.min(...check_small);
                var small = parseInt(value['small']);
                var medium = parseInt(value['medium']);
                var large = parseInt(value['large']);
                var extra_large = parseInt(value['extra_large']);

                if(value['without_set'] == '1'){
                    // not check set or unset
                    var total_qty = small + medium + large + extra_large;

                    total_price += value['price'] * total_qty;

                }else{
                    // to check set or unset logic
                    var price = value['price'];
                    var minQty = minValue[key];
                    var extraQty = 0;
                    if(small != minQty){
                        extraQty += small - minQty;
                    }
                    if(medium != minQty){
                        extraQty += medium - minQty;
                    }
                    if(large != minQty){
                        extraQty += large - minQty;
                    }
                    if(extra_large != minQty){
                        extraQty += extra_large - minQty;
                    }
                    var total_qty = small + medium + large + extra_large;
                    total_price += (value['price'] * total_qty) + ((value['price'] * extraQty * value['percentage']) / 100);
                    
                    
                    // response.splice(response['tax'],1);
                    // response.splice(response['total_price'],1);
                    // response.splice(response['tax_total_price'],1);
                    // 
                    // 

                }

                tax = (total_price * value['tax']) / 100;
                tax_total_price = tax + total_price;
                response['tax'] = tax;
                response['total_price'] = total_price;
                response['tax_total_price'] = tax_total_price;
            });
            // console.log(response);
            return response;
        }

    </script>

    <!-- GET DEALER DATA -->
    <script type="text/javascript">
        $(document).ready(function(){
           $("#user_id").on('change',function() {
               loadDealer($(this).val());
                //console.log($(this).val());
            });

            function loadDealer(val)
            {
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('orders.getdealerdata')?>",
                    type       : 'post',
                    data       : { "user_id": val , '_token' : token },
                    dataType   : 'json',
                    success    : function(dealer_data) {
                        //var append_email = dealer_data.email;
                        //console.log(dealer_data.state);
                        $("#name").val(dealer_data.company_name);
                        $("#email").val(dealer_data.email);
                        $("#mobile").val(dealer_data.mobile);
                        $("#gstno").val(dealer_data.gstno);
                        $("#billing_address").val(dealer_data.address);
                        $("#billing_area").val(dealer_data.area);
                        $("#billing_state").val(dealer_data.state);
                        $("#billing_city").val(dealer_data.city);
                        $("#billing_pincode").val(dealer_data.pincode);
                        //$("#email").html(dealer_data.company_name).val();
                    }
                });
            } 
        });
    </script>

    <!-- AJAX SUBMIT -->
    <script type="text/javascript">
        $('.save_data').click(function(e,ele)
        {   

            var btn_val = $(this).val();

            var token = "<?=csrf_token()?>";
            $('#order_form_data').ajaxSubmit({
                url: "<?=route('orders.store')?>",
                type: 'post',
                data: { "_token" : token},
                dataType: 'json', 
                beforeSubmit : function()
                {
                   $("[id$='_error']").empty();
                   $("[id$='_div']").removeClass('has-error');
                },           
                success : function(resp)
                {     
                    toastr.success("Record added successfully."); 
                    $(".null_value_class").val("");
                    if(btn_val == 'save_new')
                    {
                        location.reload();
                    }
                    else
                    {
                        location.href = "<?= route('orders.index') ?>"
                    }
                },
                error : function(respObj){

                    toastr.error('There was some error');
                    $.each(respObj.responseJSON.errors, function(id,msg){
                        var id_arr = id.split('.');
                        // console.log(id_arr);
                        // product[product][0][M]
                        if (id_arr[3] == 'brand_id') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'category_id') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'product_id') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'color_id') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[4] == 'qty') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                            // $("input[name="+"'"+id_arr[0]+"["+id_arr[1]+"]"+"["+id_arr[3]+"]"+"["+id_arr[2]+"]"+"'").closest('td').find('span').text(msg[0]);
                        }
                    });
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                        $('#'+k+'_div').removeClass('has-error');
                        $('#'+k+'_div').addClass('has-error');
                    }); 
                }
            });
        });
    </script>
    @include('admin.layout.alert')
@stop