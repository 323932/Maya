<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
    table,td,th{
      border: 1px solid #333;
      border-collapse: collapse;
      text-align: center;
    }
    td,th{
      padding: 5px
    }
  </style>
  </head>
  <body style="font-family: 'Muli', sans-serif;">
    <div style="text-align: center;width: 100%;font-family: open sans;max-width: 580px;float: none;margin: 0 auto;border: 1px solid #686868 ;padding-top: 30px;">
      <div style="padding: 0px;text-align: left;display: inline-block;float: left;width: 100%;">
        <div style="margin-bottom: 20px;text-align: center;" ><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 80px"></div>
        <div>
          <div style="padding: 0 20px;">
            <div>
              <div style="font-size: 16px;margin-bottom: 10px;">
                ORDER NO – {{$data['order_no']}}<br/><br/>
                DEAR CUSTOMER,<br/><br/>

                YOUR ORDER WITH ORDER NUMBER {{$data['order_no']}} CONTAINING BELOW ITEM HAS BEEN CONFIRMED AND IS BEING PROCESSED.THE ITEM WILL BE SHIPPED SOON ONCE READY.
                <br/>
                <div style="padding: 20px;display: inline-block;width: calc(100% - 40px);text-align: center;">
                  <table style="float: none;display: inline-block;">
                    <tr>
                      <th>Product Name</th>
                      <th>Quantity</th>
                    </tr>
                    @foreach($data['order_item'] as $order_item)
                      <tr>
                        <td>{{$order_item['name']}}</td>
                        <td>{{$order_item['qty']}}</td>
                      </tr>
                    @endforeach 
                  </table>
                </div>  
                IN CASE OF ANY QUERY, PLEASE GO THROUGH THE FAQs SECTION OR CONTACT US.</div>
            </div>
          </div>
        </div>
        <div style="text-align: left">
          <div style="padding:  20px;">
            <div>
              <div style="font-size: 16px;margin-bottom: 10px;">
              <b>ORDER DETAILS-</b><br>
              ORDER NO {{$data['order_no']}}<br>
              PAYMENT MODE- {{strtoupper($data['payment_mode'])}}<br>
              EXPECTED DELIVERY DATE-{{strtoupper($data['delivery_date'])}}<br><br>

              <b>DELIVERY ADDRESS DETAILS</b><br/>
              {{strtoupper($data['shipping_address'])}},{{strtoupper($data['shipping_area'])}},<br>
              {{strtoupper($data['shipping_state'])}},{{strtoupper($data['shipping_city'])}},<br>
              {{strtoupper($data['shipping_pincode'])}},India<br>
                <br>
              </div>
              <div style="font-size: 20px;margin-bottom: 10px;">Thanks for dealing with us Team MAAYA
                <br>
                <div style="margin-bottom: 20px;"><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 40px"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div>
          <div style="background-color: #333;color: #fff;font-size: 20px;width: 100%;display: inline-block;box-sizing: border-box;">
            <div style="padding:10px  20px;">
              <div>Stay In Touch ! Find Us On </div>
              <div>
                <a href="www.facebook.com/maayaaclothingofficial"><img src="<?= IMAGE_PATH.'facebook-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.youtube.com/user/MrVipulchheda/channels"><img src="<?= IMAGE_PATH.'you-tube.png' ?>" style="margin:5px;"></a>
                <a href="www.instagram.com/maayaaclothingofficial/"><img src="<?= IMAGE_PATH.'instagram-icon.png' ?>" style="margin:5px;"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>