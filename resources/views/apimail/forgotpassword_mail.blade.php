<!DOCTYPE html>
<html>
  <head></head>
  <body style="font-family: 'Muli', sans-serif;">
    <div style="text-align: center;width: 100%;font-family: open sans;max-width: 580px;float: none;margin: 0 auto;border: 1px solid #686868 ;padding-top: 30px;">
      <div style="padding: 0px;text-align: center;display: inline-block;float: left;width: 100%;">
        <div style="margin-bottom: 20px;"><img src="<?= LOCAL_IMAGE_PATH.'maya-logo.png' ?>" alt="logo" style="height: 80px"></div>
        <div>
          <div style="padding: 0 20px;">
            <div>
              <div style="font-size: 20px;margin-bottom: 10px;">Hello!
You are receiving this email because we received a password reset request for your account.		</div>
			<a href="{{route('dealer.reset')}}" target="_blank"><button style="background-color:#0078bd;color: #ffffff;border: 1px solid #0078bd;padding: 10px;border-radius: 4px;width: 150px;text-align: center;font-size: 15px;font-weight: bold;margin-bottom: 10px">Reset Password</button></a>
            </div>
          </div>
        </div>
        <div style="text-align: center;">
          <div style="padding: 0 20px;">
            <div>
              <div style="font-size: 20px;margin-bottom: 20px;">If you did not request a password reset, no further action is required.
                <br>
                <br>
                <div style="margin-bottom: 20px;text-align: left"><img src="<?= LOCAL_IMAGE_PATH.'maya-logo.png' ?>" alt="logo" style="height: 40px"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div>
          <div style="background-color: #333;color: #fff;font-size: 20px;width: 100%;display: inline-block;box-sizing: border-box;">
            <div style="padding:10px  20px;">
              <div>Stay In Touch ! Find Us On </div>
              <div>
                <a href="www.facebook.com/maayaaclothingofficial"><img src="<?= LOCAL_IMAGE_PATH.'facebook-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.youtube.com/user/MrVipulchheda/channels"><img src="<?= LOCAL_IMAGE_PATH.'youtube-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.instagram.com/maayaaclothingofficial/"><img src="<?= LOCAL_IMAGE_PATH.'instagram-icon.png' ?>" style="margin:5px;"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>