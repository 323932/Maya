<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
    table,td,th{
      border: 1px solid #333;
      border-collapse: collapse;
      text-align: center;
    }
    td,th{
      padding: 5px
    }
  </style>
  </head>
  <body style="font-family: 'Muli', sans-serif;">
    <div style="text-align: center;width: 100%;font-family: open sans;max-width: 580px;float: none;margin: 0 auto;border: 1px solid #686868 ;padding-top: 30px;">
      <div style="padding: 0px;text-align: left;display: inline-block;float: left;width: 100%;">
        <div style="margin-bottom: 20px;text-align: center;" ><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 80px"></div>
        <div>
          <div style="padding: 0 20px;">
            <div>
              <div style="font-size: 16px;margin-bottom: 10px;">
                THANK YOU FOR CONNECTING WITH MAAYAA. YOU WILL BE SHORTLY INTIMATED WITH A CONFIRMATION MAIL.</div>
            </div>
          </div>
        </div>
        <div style="text-align: left">
          <div style="padding:  20px;">
            <div>
              <div style="font-size: 20px;margin-bottom: 10px;">Thanks for dealing with us Team MAAYA
                <br>
                <div style="margin-bottom: 20px;"><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 40px"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div>
          <div style="background-color: #333;color: #fff;font-size: 20px;width: 100%;display: inline-block;box-sizing: border-box;">
            <div style="padding:10px  20px;">
              <div>Stay In Touch ! Find Us On </div>
              <div>
                <a href="www.facebook.com/maayaaclothingofficial"><img src="<?= IMAGE_PATH.'facebook-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.youtube.com/user/MrVipulchheda/channels"><img src="<?= IMAGE_PATH.'you-tube.png' ?>" style="margin:5px;"></a>
                <a href="www.instagram.com/maayaaclothingofficial/"><img src="<?= IMAGE_PATH.'instagram-icon.png' ?>" style="margin:5px;"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>