<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <!-- title-->
    <title>MAAYAA</title>
    <link rel="icon" href="<?= LOCAL_IMAGE_PATH.'maya-logo.png'  ?>">
    {{Html::style('backend/css/main.css')}}
    <link rel="stylesheet" id="colorChange" type="text/css" href="">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')	
    -->
  </head>
  <body>
    <section class="notfound-page">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-3 justify-content-center text-center"><a class="logo" href=""><img class="img-fluid" src=" alt="Harekrishnalogo" style="width:200px;"></a></div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-6 center-section text-center">
            <div class="row">
              <div class="col-md-12 mt-50 text-center">
                <div class="text-h1">404</div>
              </div>
              <div class="col-md-12 mt-30">
                <h1>Oops!</h1>
                <h2>We can't seem to find the page you're looking for.</h2>
                <h6>Error code: 404</h6>
                <ul class="list-unstyled"> 
                  <li>Go to Back <a class="ml-10" href=""><span class="icon icon-House mr-5"></span>Home</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>