@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::open(['route'=>'color.store','role'=>'form','class'=>'m-0','files'=>true]) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and ad new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('color.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                                    <label>Name <sup class="text-danger">*</sup></label>
                                    <?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']); ?>
                                    <span id="Name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <label>Hex Code <sup class="text-danger">*</sup></label>
                            <?= Form::text('hexa_code', null, ['class' => 'form-control', 'placeholder' => 'Hexacode','maxlength'=>6,'id'=>'hexa_code']); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('hexa_code') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('color.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?= Html::script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js')?>
    <script type="text/javascript">
        $(function () {
          $('#hexa_code').colorpicker({   
          });
        });
    </script>
    @include('admin.layout.alert')
@stop