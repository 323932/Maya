@extends('admin.layout.layout')
@section('start_form')
    <?= Form::open(['role'=>'form','class'=>'m-0','files'=>true,'method'=>'POST','id'=>'product_form_data']) ?>
@stop
@section('top_fixed_content')
    <?=Html::style('backend/css/custome.css') ?>
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <?=Html::style('backend/css/fileinput.min.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and new">Save & New</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('products.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop 
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                                <label>Name <sup class="text-danger">*</sup></label>
                                <?= Form::text('name', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Name','maxlength'=>50]); ?>
                                <span id="name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('sku')) {{ 'has-error' }} @endif">
                                <label>SKU <sup class="text-danger">*</sup></label>
                                <?= Form::text('sku', null, ['class' => 'form-control null_value_class', 'placeholder' => 'SKU']); ?>
                                <span id="sku_error" class="help-inline text-danger"><?= $errors->first('sku') ?>&nbsp;</span>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('brand_id')) {{ 'has-error' }} @endif">
                                <label>Brand <sup class="text-danger">*</sup></label>
                                <?=Form::select('brand_id',$brand,old('brand_id'), ['class' => 'form-control null_value_class select2_2 brand_id','id' => 'brand_id','placeholder'=>'Select Brand']);?>
                                <span id="brand_id_error" class="help-inline text-danger"><?= $errors->first('brand_id') ?></span>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('category_id')) {{ 'has-error' }} @endif">
                                <label>Category <sup class="text-danger">*</sup></label>
                                <?=Form::select('category_id',$category,old('category_id'), ['class' => 'form-control select2_2 category_id null_value_class','id' => 'category_id']);?>
                                <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('category_id') ?></span>
                            </div>
                        </div> 
                    </div> 
                    <div class="col-md-6">
                            <div class="form-group @if($errors->has('category_id')) {{ 'has-error' }} @endif">
                                <label>Collection <sup class="text-danger">*</sup></label>
                                <?= Form::text('collection',old('collection'),array('class' => 'form-control collection','placeholder'=>'Collection','id'=>'collection_list')) ?>
                                <div id="collection_suggetion"></div>
                                <input type="hidden" id="hidden_id">
                                <span id="collection_error" class="help-inline text-danger"><?= $errors->first('collection') ?></span>
                            </div>
                        </div>
                    <div class="row">                   
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('original_price')) {{ 'has-error' }} @endif">
                                <label>Price <sup class="text-danger">*</sup></label>
                                <?= Form::text('original_price', null, ['class' => 'form-control number_only null_value_class', 'placeholder' => 'Price']); ?>
                                <span id="original_price_error" class="help-inline text-danger"><?= $errors->first('original_price') ?></span>
                            </div>
                        </div>    
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('tax')) {{ 'has-error' }} @endif">
                                <label>Tax<sup class="text-danger">*</sup></label>
                                <?=Form::select('tax',['5'=>'5%','12'=>'12%'],old('tax'), ['class' => 'form-control null_value_class select2_2','id' => 'tax','placeholder'=>'Select Tax']);?>
                                <span id="tax_error" class="help-inline text-danger"><?= $errors->first('tax') ?></span>
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('description')) {{ 'has-error' }} @endif">
                                <label>Description</label>
                                <?= Form::textarea('description', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Description','rows'=>"2"]); ?>
                                <span id="" class="help-inline text-danger"><?= $errors->first('description') ?></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label class="col-md-2">Status </label>
                                <div class="col-md-12" style="padding-top:10px;">
                                    <div class="animated-radio-button pull-left mr-10">
                                        <label for="status_true">
                                            <?=Form::radio('status', 1, true,['id' => 'status_true','class'=>'null_value_class'])?>
                                            <span class="label-text"></span> Enable
                                        </label>
                                    </div>
                                    <div class="animated-radio-button pull-left">
                                        <label for="status_false">
                                            <?=Form::radio('status', 0,null, ['id' => 'status_false','class'=>'null_value_class'])?>
                                            <span class="label-text"></span> Disable
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group row">
                                <label class="col-md-2">Availability Set </label>
                                <div class="col-md-12" style="padding-top:10px;">
                                    <div class="animated-radio-button pull-left mr-10">
                                        <label for="without_set_true">
                                            <?=Form::radio('without_set', 1, true,['id' => 'without_set_true','class'=>'without_set null_value_class'])?>
                                            <span class="label-text"></span> Yes
                                        </label>
                                    </div>
                                    <div class="animated-radio-button pull-left">
                                        <label for="without_set_false">
                                            <?=Form::radio('without_set', 0,null, ['id' => 'without_set_false','class'=>'without_set null_value_class'])?>
                                            <span class="label-text"></span> No
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="per">
                                <div class="form-group @if($errors->has('percentage')) {{ 'has-error' }} @endif">
                                    <label>Percentage</label>
                                    <?= Form::text('percentage', null, ['class' => 'form-control null_value_class number_only percentage', 'placeholder' => 'Percentage']); ?>
                                    <span id="percentage_error" class="help-inline text-danger"><?= $errors->first('percentage') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group @if($errors->has('image')) {{ 'has-error' }} @endif">
                                <label>Images <sup class="text-danger">*</sup></label>
                                <div class="file-loading">
                                    <input id="input-image-4" name="input-image[]" type="file" class="null_value_class" accept="image/*"  name="image" multiple>
                                    <span id="input-image_error" class="help-inline text-danger"><?= $errors->first('input-image') ?></span>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card" id="product_variant_detail">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Product Variant Detail</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="table-responsive" id="product_variant">
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Product Detail</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table m-0 v-top verticle-align">
                                <thead>
                                    <tr>
                                        <th style="border:none">Key<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Value<sup class="text-danger">*</sup></th>
                                        <th style="border:none"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="attribute">
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <?= Form::text('product_key',old('product_key'),['class' => 'form-control null_value_class product_key','id'=>'product_key','style'=>'width:250px','placeholder' => 'Key']); ?>
                                            <span id="product_key_error" class="help-inline text-danger"><?= $errors->first('attribute.attribute.product_key') ?></span>
                                        </td>
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <?= Form::text('product_value',old('product_value'),['class' => 'form-control null_value_class product_value','id'=>'product_value','style'=>'width:250px','placeholder' => 'Value']); ?>
                                            <span id="product_value_error" class="help-inline text-danger"><?= $errors->first('attribute.attribute.product_value') ?></span>
                                        </td>
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <a id="attribute_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                            <a id="attribute_add" class="pt-10 pull-left btn-add" ><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4> More Information</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table m-0 v-top verticle-align">
                                <thead>
                                    <tr>
                                        <th style="border:none">Key<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Value<sup class="text-danger">*</sup></th>
                                        <th style="border:none"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="information">
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <?= Form::text('information_key',old('information_key'),['class' => 'form-control null_value_class information_key','id'=>'information_key','style'=>'width:250px','placeholder' => 'Key']); ?>
                                            <span id="information_key_error" class="help-inline text-danger"><?= $errors->first('information.information.information_key') ?></span>
                                        </td>
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <?= Form::text('information_value',old('information_value'),['class' => 'form-control null_value_class information_value','id'=>'information_value','style'=>'width:250px','placeholder' => 'Value',]); ?>
                                            <span id="information_value_error" class="help-inline text-danger"><?= $errors->first('information.information.information_value') ?></span>
                                        </td>
                                        <td class="col-md-4" style="border:none" valign="top">
                                            <a id="information_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                            <a id="information_add" class="pt-10 pull-left btn-add" ><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title" style="margin-bottom: 20px">
                    <h4>User Plans <sup class="text-danger">*</sup></h4>
                    <span id="is_visible_error" class="help-inline text-danger"><?= $errors->first('is_visible') ?></span>
                </div>
                <input type="hidden" name="plans" value="<?= $user_flag ?>">
                <div class="col-md-12">
                    @foreach($user_plans as $key=>$value)
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('is_visible')) {{ 'has-error' }} @endif">
                                <div class="utility">
                                    <div class="animated-checkbox">
                                        <label class="semibold-text">
                                          <input name="is_visible[]" class="check_{{$key}} null_value_class" type="checkbox" value="{{$key}}"><span class="label-text"><?= $value ?></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                 </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and new">Save & New</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('products.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
    <?=Html::script('backend/js/jquery.fileuploader.min.js')?>
    <?= Html::script('backend/js/dynamicform.js') ?>
    <?= Html::script('backend/js/piexif.min.js') ?>
    <?= Html::script('backend/js/fileinput.min.js') ?>    
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
    <script type="text/javascript">

        $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&& 32!=h.which&&(h.which<48||h.which>57))return!1});

        var tag1 = $('#brand_id').select2({
            placeholder : "Select Brand"
    
        });
        var tag2 = $('#category_id').select2({
            placeholder : "Select Category"
    
        });
        var tag3 = $('#tax').select2({
            placeholder : "Select Tax"
    
        });
    </script>

    <!-- COLLECTION SUGGETION -->
    <script type="text/javascript">
        $('.collection').keyup(function(){
            //alert($(this).val());
            var collection_value = $(this).val();
            //var id = $("#hidden_id").val();
            // console.log(id);
            var token = "{{csrf_token()}}";
            var url = "{{route('products.collection.suggetion')}}";
            var path = url;
            $.ajax({
                type : 'GET',
                url : path,
                data : {
                    'collection' : collection_value,
                    '_token' : token
                },
                dataType : 'html',
                success : function(data){
                    $('#collection_suggetion').show();
                    $('#collection_suggetion').html(data);
                }
            });
        });

        function selectCollection(collection_value){
            console.log(collection_value);
            $('.collection').val(collection_value);
            $('#collection_suggetion').hide();
        }
    </script>

    <!-- PRESERVE CHECKBOX -->
    <script type="text/javascript">
        var old_tag = {!! json_encode(old('is_visible')) !!};
        //console.log(old_tag);
       
        $.each(old_tag,function(k,v){
           $(".check_"+v).attr('checked',true);
        });
    </script>
    
    <!-- IMAGE GALLERY -->
    <script type="text/javascript">

        var url = "<?= route('upload.url')?>";
        $("#input-image-4").fileinput({
            theme: 'fa',
        uploadUrl: url,
        uploadAsync: false,
        });
    </script>

    <!-- DYNAMIC FORM -->
    <script type="text/javascript">
        var product_detail =  $("#attribute").dynamicForm("#attribute_add", "#attribute_remove", {
            limit: 10,
            normalizeFullForm : false,
        });

        old_data = <?= json_encode(old('attribute.attribute')) ?>;
        // console.log(old_data);
        product_detail.inject(old_data);

        var select_list = $('tbody tr');
        
        @if($errors)
            var detail_Errors = <?= json_encode($errors->toArray()) ?>;        
            $.each(detail_Errors, function(id,msg){
                var id_arr = id.split('.');
                if (id_arr[3] == 'product_key') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'product_value') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
            });
        @endif

        var information_detail =  $("#information").dynamicForm("#information_add", "#information_remove", {
            limit: 10,
            normalizeFullForm : false,
        });

        old_data = <?= json_encode(old('information.information')) ?>;
        // console.log(old_data);
        information_detail.inject(old_data);

        var select_list = $('tbody tr');

        @if($errors)
            var detail_Errors = <?= json_encode($errors->toArray()) ?>;        
            $.each(detail_Errors, function(id,msg){
                var id_arr = id.split('.');
                if (id_arr[3] == 'information_key') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'information_value') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
            });
        @endif
    </script>

    <!-- BRANDWISE CATEGORY --> 
    <script type="text/javascript">
        $(document).ready(function(){
           $("#brand_id").on('change',function() {
               loadCategory($(this).val());
                //console.log($(this).val());
            });

            function loadCategory(val)
            {
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('products.getcategory')?>",
                    type       : 'post',
                    data       : { "brand_id": val , '_token' : token },
                    dataType   : 'html',
                    success    : function(category_list) {
                        var options = $(category_list).html();
                        $('#category_id').val('').trigger('change');
                        $("#category_id").html(options).val({{ old('category_id') }});
                    }
                });
            } 
        });
    </script>

    <!-- BRANDWISE SIZE --> 
    <script type="text/javascript">
        $('#product_variant_detail').hide();
        $(document).ready(function(){
           $("#brand_id").on('change',function() {
               loadSize($(this).val());
            });

            function loadSize(val)
            {
                $('#product_variant_detail').show();
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?=URL::route('products.getsize')?>",
                    type       : 'post',
                    data       : { "brand_id": val , '_token' : token },
                    dataType   : 'html',
                    success    : function(result) {
                        console.log(result);
                        $('#product_variant').html(result);
                        var product_detail =  $("#product").dynamicForm("#product_add", "#product_remove", {
                            limit: 10,
                            normalizeFullForm : false,
                        });
                        old_data = <?= json_encode(old('product.product')) ?>;
                        // console.log(old_data);
                        product_detail.inject(old_data);
                        var select_list = $('tbody tr');                        
                    }
                });
            }
        });
    </script>    

    <!-- HIDE SHOW PERCENTAGE FIELD -->
    <script type="text/javascript">        
        $('.without_set').change( function(){
            var status = $(this).val();
            if(status == 0){
                $('.per').css('display','block');    
            }else{
                $('.per').css('display','none');
            }
        });
    </script>
    <script type="text/javascript">  
        var old_status = {!! json_encode(old('without_set')) !!};
       
        if(old_status == 0){

            $('.per').css('display','block');    
        }else{
           
            $('.per').css('display','none');
        }
    </script> 

    <!-- AJAX SUBMIT  -->
    <script type="text/javascript">
        $('.save_data').click(function(e,ele)
        {   

            var btn_val = $(this).val();

            var token = "<?=csrf_token()?>";
            $('#product_form_data').ajaxSubmit({
                url: "<?=route('products.store')?>",
                type: 'post',
                data: { "_token" : token},
                dataType: 'json', 
                beforeSubmit : function()
                {
                   $("[id$='_error']").empty();
                   $("[id$='_div']").removeClass('has-error');
                },           
                success : function(resp)
                {     
                    toastr.success("Record added successfully."); 
                    $(".null_value_class").val("");
                    if(btn_val == 'save_new')
                    {
                        location.reload();
                    }
                    else
                    {
                        location.href = "<?= route('products.index') ?>"
                    }
                },
                error : function(respObj){

                    toastr.error('There was some error');
                    $.each(respObj.responseJSON.errors, function(id,msg){
                        var id_arr = id.split('.');
                        console.log(id_arr);
                        // product[product][0][M]
                        if (id_arr[3] == 'color_id') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[4] == 'qty') {
                            $("input[name="+"'"+id_arr[0]+"["+id_arr[1]+"]"+"["+id_arr[3]+"]"+"["+id_arr[2]+"]"+"'").closest('td').find('span').text(msg[0]);
                            // $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        // if (id_arr[3] == 'qty') {
                        //     $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        // }
                        if (id_arr[3] == 'product_key') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'product_value') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'information_key') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                        if (id_arr[3] == 'information_value') {
                            $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                        }
                    });
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                        $('#'+k+'_div').removeClass('has-error');
                        $('#'+k+'_div').addClass('has-error');
                    }); 
                }
            });
        });
    </script>
    @include('admin.layout.alert')
@stop