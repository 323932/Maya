 <table class="table m-0 v-top verticle-align">
    <thead>
        <tr>
            <th style="border:none" width="150px">Color <sup class="text-danger">*</sup></th>
            @foreach($data as $key=>$value)
                @if($count == 1)
                	<th class="variant-detail-th" style="border:none">{{$value['name']}}<sup class="text-danger">*</sup></th>
                @else
                    <th style="border:none">{{$value['name']}}<sup class="text-danger">*</sup></th>
                @endif
            @endforeach
            <th style="border:none"></th>
        </tr>
    </thead>
    <tbody>
        <tr id="product">
            <td style="border:none" valign="top">
                <?=Form::select('color_id',$color,old('color_id'), ['class' => 'form-control null_value_class color_id','id' => 'color_id','placeholder'=>'Select Color','style'=>'width:150px']);?>
                <span id="product.product.color_id_error" class="help-inline text-danger"><?= $errors->first('product.product.color_id') ?></span>
            </td>
            @foreach($data as $key=>$value)
                <?php $size = $value['name']?>
	            <td style="border:none" valign="top">
                    @if($count == 1)
                        <?=Form::text($size,old('size'), ['class' => 'form-control number_only null_value_class  '.$size,'id'=>'qty','placeholder'=>'Qty of '. $size.' size']);?>
                    @else
	                   <?=Form::text($size,old('size'), ['class' => 'form-control number_only null_value_class  '.$size,'id'=>'qty','placeholder'=>'Qty of '. $size.' size']);?>
                    @endif
	                <span id="qty_error" class="help-inline text-danger"><?= $errors->first('product.product.qty') ?></span>
	            </td>
            @endforeach
            <td style="border:none" valign="top">
                <a id="product_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                <a id="product_add" class="pt-10 pull-left btn-add" ><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
            </td>
        </tr>
    </tbody>
</table>