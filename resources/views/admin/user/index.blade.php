@extends('admin.layout.layout')
@section('top_fixed_content')
<?=Html::style('backend/css/custome.css')?>
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Dealer Master</h4>
    </div>
    <div class="top_filter"></div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="supplier_table" class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>GST</th>
                            <th>Approve/DisApprove</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>
<div id="modal-consultant-profile" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body box no-border">
                <div id="modal-consultant-profile-info"></div>
                <!-- <div class="overlay" id="modal-consultant-profile-loader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div> -->
            </div>
        </div>
    </div>
@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>

<script type="text/javascript">
    $('#supplier_table').DataTable({
            "bProcessing": false,
            "bServerSide": true,
            "autoWidth": false,
            lengthMenu: [
                [ 10, 25, 50, 100,200,500],
                [ '10', '25', '50','100','200','500']
            ],
            "sAjaxSource": "<?= route('dealers.index')?>",
            "aaSorting": [ 1,"desc"],
            "aoColumns": [
            { mData:"updated_at",bSortable : false,sClass : 'text-center',sWidth:"16%",bVisible : false},
            { mData:"name",bSortable : true,sClass : 'text-center',sWidth:"16%"},
            { mData:"email",bSortable : true,sClass : 'text-center',sWidth:"16%"},
            { mData:"mobile",bSortable : true,sClass : 'text-center',sWidth:"16%"},
            { mData:"gstno",bSortable : true,sClass : 'text-center',sWidth:"16%"},
            { mData:"status",bSortable : false,sClass : 'text-center',sWidth:"15%",
                mRender: function(v, t, o) {

                            var editurl = "";
                            editurl = editurl.replace(':id',o['id']);

                            if(o['status'] == '0')
                            {
                                var act_html = "<div class='btn-group'>"
                                +"<label class='switch'><input type='checkbox' onClick='approve("+o['id']+","+o['status']+")' data-attr="+o['status']+" id='status' name='status' ><span class='slider round'></span></label>"
                                +"</div>"
                            }else{
                                var act_html = "<div class='btn-group'>"
                                +"<label class='switch'><input type='checkbox' onClick='approve("+o['id']+","+o['status']+")'  id='status' name='status' checked><span class='slider round'></span></label>"
                                +"</div>"
                            }
                            return act_html;
                        }
                    },
            {
            mData     : "id",
            sWidth    : "100px",
            sClass    : 'text-center',
            bVisible  : true,
            bSortable : false,
            mRender   : function (v, t, o) {

                var editurl = "";
                editurl = editurl.replace(':id',o['id']);
                    var act_html = "<div class='btn-group'>"

                    +"<button type='button' class='btn btn-default btn-xs' onClick='view("+o['id']+")'><i class='fa fa-search-plus' title='Edit Record'></i></button>"    
                    +"</div>";
                return act_html;
              }
                },
                    
            ],
            fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
            fnDrawCallback : function (oSettings) {
                $("div.overlay").hide();
            },
        });
</script>

<script type="text/javascript">

    function approve(id,status)
    {
        var new_id = status +'_'+ id;
        if(status == '0'){
            $('#modal-consultant-profile-loader').show();
            $('#modal-consultant-profile').modal('show');

            var viewurl = "{{ route('dealers.edit',':id')}}";
            viewurl = viewurl.replace(':id',new_id);

            $.get(viewurl, function(resp) {
                $('#modal-consultant-profile-loader').hide();
                $('#modal-consultant-profile-info').html(resp);
            });
        }else{
            var viewurl = "{{ route('dealers.deactive',':id')}}";
            viewurl = viewurl.replace(':id',new_id);
            $.ajax({
                url : viewurl,
                type : 'POST',
                data : {'_token':"{{csrf_token()}}"},
                success : function(response, textStatus, jqXhr) {
                    console.log("Venue Successfully Patched!");
                    location.reload();
                }
            });
        }
        
    }

    function view(id)
    {
        $('#modal-consultant-profile-loader').show();
            $('#modal-consultant-profile').modal('show');

            var viewurl = "{{ route('dealers.view',':id')}}";
            viewurl = viewurl.replace(':id',id);
            $.get(viewurl, function(resp) {
                $('#modal-consultant-profile-loader').hide();
                $('#modal-consultant-profile-info').html(resp);
        });
    }
</script>
@include('admin.layout.alert')
@stop