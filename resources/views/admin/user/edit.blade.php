<?= Form::open(['route'=>['dealers.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH','id'=>'form_data']) ?>
<input type="hidden" name="status" value="{{$status}}">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('type')) {{ 'has-error' }} @endif">
                                    <label>Dealer Type <sup class="text-danger">*</sup></label>
                                    <?=Form::select('type',['seller'=>'Sales Person','Company Name'=>'Company Name'],old('type'), ['class' => 'form-control type null_value_class','id' => 'type','placeholder'=>'Select Dealer Type']);?> 
                                    <span id="type_error" class="help-inline text-danger"><?= $errors->first('type') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('user_plan_id')) {{ 'has-error' }} @endif">
                            <label>Dealer Plan <sup class="text-danger">*</sup></label>
                            <?=Form::select('user_plan_id',$user_plan,old('user_plan_id'), ['class' => 'form-control null_value_class user_plan_id','id' => 'user_plan_id','placeholder'=>'Select User Plan']);?>
                            <span id="user_plan_id_error" class="help-inline text-danger"><?= $errors->first('user_plan_id') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save">Save</button>
            <a href="<?= route('dealers.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>

<script src="<?= asset('backend/js/jquery.min.js') ?>"></script>
<script src="<?= asset('backend/js/jquery.form.min.js') ?>"></script>
<script type="text/javascript">  
    $('.save_data').click(function(e,ele){
        var token = "<?=csrf_token()?>";
        var id = "<?= $id?>";
        var viewurl = "{{ route('dealers.update',':id')}}";
        url = viewurl.replace(':id',id);
        $('#form_data').ajaxSubmit({
            url: url,
            type: 'POST',
            data: { "_token" : token},
            dataType: 'json',  
            beforeSubmit : function()
            {
                //$("[id$='_error']").empty();
            },          
            success : function(resp)
            { 
                $(".null_value_class").val("");
                toastr.success('Record added Successfully'); 
                location.href = "<?= route('dealers.index') ?>"
            },
            error : function(respObj){ 
                $.each(respObj.responseJSON, function(k,v){
                    $('#'+k+'_error').text(v);
                }); 
                $('#type_error').text(respObj.responseJSON.errors.type);
                $('#user_plan_id_error').text(respObj.responseJSON.errors.user_plan_id);
                //toastr.error('There was some error');
            }
        });
        return false;
    });
</script>