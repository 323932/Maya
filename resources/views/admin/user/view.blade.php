
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header" style="border-bottom: medium none;margin-bottom: 10px;">
            <i class=""></i>Dealer Details
          </h2>
        </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <div class="table-responsive">
            <table class="table">
              <tbody>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 64%;text-align: right;margin-right: 24px;float: right;">
                          Company Name:
                        </td>
                        <td style="width: 50%;">
                          <?= $user_data['company_name'] ?>
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                           Dealer Type:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['type'])}}
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          FirstName:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['firstname'])}}
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 61%;text-align: right;margin-right: 24px;float: right;">
                          LastName:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['lastname'])}}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                         GSTNo:
                        </td>
                        <td style="width: 50%;">
                          {{$user_data['gstno']}}
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          Mobile No:
                        </td>
                        <td style="width: 50%;">
                          {{$user_data['mobile']}}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          Email:
                        </td>
                        <td style="width: 50%;">
                          {{$user_data['email']}}
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          Address:
                        </td>
                        <td style="width: 50%;">
                          {{$user_data['address']}}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          Area:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['area'])}}
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          State:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['state'])}}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <th style="width:50%">
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          City:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['city'])}}
                        </td>
                      </tr>
                    </table>
                  </th>
                  <td>
                    <table style="width: 100%">
                      <tr>
                        <th style="width: 50%;text-align: right;margin-right: 24px;float: right;">
                          Pincode:
                        </td>
                        <td style="width: 50%;">
                          {{ucwords($user_data['pincode'])}}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>                
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
    </section>    