@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::open(['route'=>'category.store','role'=>'form','class'=>'m-0','files'=>true]) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('category.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('file')) {{ 'has-error' }} @endif">
                                    <div class="col-md-12"><label>Image <sup class="text-danger">*</sup></label></div>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;">
                                                <img src="<?=asset('backend/images/no_image.png', IS_SECURE)?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <label class="btn btn-file">
                                                    <?=Form::file('image', ['class' => 'form-control'])?>
                                                    <span  class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span  class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                </label>
                                                <a href="" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                            </div>
                                            <span id="image_error" class="help-inline text-danger"><?=$errors->first('image')?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                                    <label>Name <sup class="text-danger">*</sup></label>
                                    <?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name','maxlenght' => 100]); ?>
                                    <span id="Name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6"> 
                                <div class="form-group  @if($errors->has('brand_id')) {{ 'has-error' }} @endif">
                                    <label>Brand<sup class="text-danger">*</sup></label>
                                    <?=Form::select('brand_id',$brand,old('brand_id'), ['class' => 'form-control select2_2 brand_id','id' => 'brand_id','placeholder'=>'Select Brand']);?> 
                                    <span id="brand_id_error" class="help-inline text-danger"><?= $errors->first('brand_id') ?></span>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-md-2">Status</label>
                                    <div class="col-md-12" style="padding-top:10px;">
                                        <div class="animated-radio-button pull-left mr-10">
                                            <label for="is_active_true">
                                                <?=Form::radio('is_active', 1, true,['id' => 'is_active_true'])?>
                                                <span class="label-text"></span> Active
                                            </label>
                                        </div>
                                        <div class="animated-radio-button pull-left">
                                            <label for="is_active_false">
                                                <?=Form::radio('is_active', 0,null, ['id' => 'is_active_false'])?>
                                                <span class="label-text"></span> InActive
                                            </label>
                                            <div>
                                                <span id="is_active_error" class="help-inline text-danger"><?= $errors->first('is_active') ?></span>
                                            </div>
                                        </div>                                
                                    </div>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('category.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?= Html::script('backend/js/jquery-migrate-1.2.1.min.js') ?>
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
    <script type="text/javascript">
        var tag1 = $('#brand_id').select2({
            placeholder : "Select Brand"
    
        });
    </script>
    @include('admin.layout.alert')
@stop