@extends('admin.layout.layout')
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::open(['route'=>'size.store','role'=>'form','class'=>'m-0','files'=>true]) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('size.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('name')) {{ 'has-error' }} @endif">
                                    <label>Name <sup class="text-danger">*</sup></label>
                                    <?= Form::text('name', null, ['class' => 'form-control char_only', 'placeholder' => 'Name','maxlength'=>5]); ?>
                                    <span id="Name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-md-2">Status</label>
                        <div class="col-md-12" style="padding-top:10px;">
                            <div class="animated-radio-button pull-left mr-10">
                                <label for="is_active_true">
                                    <?=Form::radio('is_active', 1, true,['id' => 'is_active_true'])?>
                                    <span class="label-text"></span> Active
                                </label>
                            </div>
                            <div class="animated-radio-button pull-left">
                                <label for="is_active_false">
                                    <?=Form::radio('is_active', 0,null, ['id' => 'is_active_false'])?>
                                    <span class="label-text"></span> InActive
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('size.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <script type="text/javascript">
        $(".char_only").keypress(function (e) {
                 if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 97 || e.which > 122)&& (e.which < 65 || e.which > 90))return !1           
               });
    </script>
    @include('admin.layout.alert')
@stop