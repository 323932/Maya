<?=Html::script('backend/js/jquery.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/jquery.slimscroll.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/toastr-master/toastr.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/pace.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/main.js', [], IS_SECURE)?>
@yield('script')
