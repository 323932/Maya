<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <div class="search-panel">            
        </div>
        <!-- Sidebar Menu-->
         <div class="sidebar-content">
            <ul class="sidebar-menu" id="nav-accordion">
                <ul class="sidebar-menu" id="nav-accordion">
                    <li class=" @if(Request::segment(2) == 'dashboard') active @endif">
                        <a href="/admin/dashboard">
                        <i class="fas fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="sub-menu @if(Request::segment(2) == 'brands'|| Request::segment(2) == 'category' || Request::segment(2) == 'color' || Request::segment(2) == 'products' || Request::segment(2) == 'user-plan' || Request::segment(2) == 'dealers' || Request::segment(2) == 'sales-person') active @endif"">
                        <a href="">
                            <i class="fas fa-book-open"></i>
                            <span>Catalog</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class=" @if(Request::segment(2) == 'brands') active @endif">
                                <a href="<?= route('brands.index') ?>">
                                <i class="fas fa-tags"></i>
                                <span>Brand</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'category') active @endif">
                                <a href="<?= route('category.index') ?>">
                                <i class="fas fa-list-alt"></i>
                                <span>Category</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'color') active @endif">
                                <a href="<?= route('color.index')?> ">
                                <i class="fas fa-palette"></i>
                                <span>Color</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'size') active @endif">
                                <a href="<?= route('size.index')?> ">
                                <i class="fas fa-tshirt"></i>
                                <span>Size</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'products') active @endif">
                                <a href="<?= route('products.index') ?>">
                                <i class="fas fa-boxes"></i>
                                <span>Product</span>
                                </a>
                            </li>                            
                            <li class=" @if(Request::segment(2) == 'user-plan') active @endif">
                                <a href="<?= route('user-plan.index') ?>">
                                <i class="fas fa-user"></i>
                                <span>UserPlan</span>
                                </a>
                            </li>                        
                            <li class=" @if(Request::segment(2) == 'dealers') active @endif">
                                <a href="<?= route('dealers.index') ?>">
                                <i class="fas fa-users"></i>
                                <span>Dealer</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'sales-person') active @endif">
                                <a href="<?= route('sales-person.index') ?>">
                                <i class="fas fa-people-carry"></i>
                                <span>Sales Persons</span>
                                </a>
                            </li>                    
                        </ul>
                    </li>
                    <li class="sub-menu @if(Request::segment(2) == 'coupons' || Request::segment(2) == 'notifications' || Request::segment(2) == 'banners') active @endif"">
                        <a href="">
                            <i class="fas fa-bullhorn"></i>
                            <span>Promotions</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">                            
                            <li class=" @if(Request::segment(2) == 'coupons') active @endif">
                                <a href="<?= route('coupons.index') ?>">
                                <i class="fas fa-barcode"></i>
                                <span>Coupon Code</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'notifications') active @endif">
                                <a href="<?= route('notifications.index') ?>">
                                <i class="fas fa-bell"></i>
                                <span>Notification</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'banners') active @endif">
                                <a href="<?= route('banners.index') ?>">
                                <i class="fas fa-image"></i>
                                <span>Banner</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class=" @if(Request::segment(2) == 'orders') active @endif">
                        <a href="<?= route('orders.index') ?>">
                        <i class="fas fa-cart-arrow-down"></i>
                        <span>Orders</span>
                        </a>
                    </li>                    
                    <li class=" @if(Request::segment(2) == 'pages') active @endif">
                        <a href="<?= route('pages.index') ?>">
                        <i class="fas fa-file"></i>
                        <span>Pages</span>
                        </a>
                    </li>
                    <li class=" @if(Request::segment(2) == 'reports') active @endif">
                        <a href="<?= route('reports.index') ?>">
                        <i class="fas fa-chart-bar"></i>
                        <span>Reports</span>
                        </a>
                    </li>
                </ul>
                <li class="spacer"></li>
                <li class="logout">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span>{{Auth::guard('admin')->user()->name}}</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="logout-modal">
                        <li>
                            <a href="<?=route('admin.logout')?>">Logout</a>
                        </li>
                    </ul>
                </li>                
            </ul>
        </div>
    </section>
</aside>