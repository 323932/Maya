<aside class="main-sidebar hidden-print">
    <section class="sidebar">
        <div class="search-panel">
            <div class="search-box">
              <div class="icon pull-left"><i class="fa fa-search"></i></div>
              <input class="text" type="text" placeholder="Special Search if require">
              <div class="clearfix"></div>
            </div>
        </div>
        <!-- Sidebar Menu-->
         <div class="sidebar-content">
            <ul class="sidebar-menu" id="nav-accordion">
                <ul class="sidebar-menu" id="nav-accordion">
                    <li class=" @if(Request::segment(2) == 'dashboard') active @endif">
                        <a href="/admin/dashboard">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="sub-menu @if(Request::segment(2) == 'brands'|| Request::segment(2) == 'category' || Request::segment(2) == 'color' || Request::segment(2) == 'products' || Request::segment(2) == 'user-plan' || Request::segment(2) == 'users' || Request::segment(2) == 'sales-person') active @endif"">
                        <a href="">
                            <i class="fa fa-dashboard"></i>
                            <span>Catalog</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class=" @if(Request::segment(2) == 'brands') active @endif">
                                <a href="<?= route('brands.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Brand</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'category') active @endif">
                                <a href="<?= route('category.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Category</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'color') active @endif">
                                <a href="<?= route('color.index')?> ">
                                <i class="fa fa-dashboard"></i>
                                <span>Color</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'products') active @endif">
                                <a href="<?= route('products.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Product</span>
                                </a>
                            </li>                            
                            <li class=" @if(Request::segment(2) == 'user-plan') active @endif">
                                <a href="<?= route('user-plan.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>UserPlan</span>
                                </a>
                            </li>                        
                            <li class=" @if(Request::segment(2) == 'users') active @endif">
                                <a href="<?= route('users.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Dealer</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'sales-person') active @endif">
                                <a href="<?= route('sales-person.index') ?>">
                                <i class="fa fa-list"></i>
                                <span>Sales Persons</span>
                                </a>
                            </li>                    
                        </ul>
                    </li>
                    <li class="sub-menu @if(Request::segment(2) == 'coupans' || Request::segment(2) == 'notifications') active @endif"">
                        <a href="">
                            <i class="fa fa-dashboard"></i>
                            <span>Promotions</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">                            
                            <li class=" @if(Request::segment(2) == 'coupans') active @endif">
                                <a href="<?= route('coupans.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Coupon Code</span>
                                </a>
                            </li>
                            <li class=" @if(Request::segment(2) == 'notifications') active @endif">
                                <a href="<?= route('notifications.index') ?>">
                                <i class="fa fa-dashboard"></i>
                                <span>Notification</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class=" @if(Request::segment(2) == 'orders') active @endif">
                        <a href="<?= route('orders.index') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Orders</span>
                        </a>
                    </li>
                    <li class=" @if(Request::segment(2) == 'pages') active @endif">
                        <a href="<?= route('pages.index') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Pages</span>
                        </a>
                    </li>
                    <li class=" @if(Request::segment(2) == 'reports') active @endif">
                        <a href="<?= route('reports.index') ?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Reports</span>
                        </a>
                    </li>
                    <!-- <li class="sub-menu @if(Request::segment(2) == 'reports') active @endif"">
                        <a href="<?= route('pages.index') ?>">
                            <i class="fa fa-dashboard"></i>
                            <span>Reports</span>
                            <i class="fa fa-angle-right"></i>
                        </a>
                        <ul class="treeview-menu">
                        <li class="@if(Request::segment(2) == 'designation') active @endif">
                            <a href="">
                            <i class="fa fa-pencil-square-o"></i>
                            <span>Product</span>
                            </a>
                        </li>
                    </ul>
                    </li> -->
                </ul>
                <li class="spacer"></li>
                <li class="logout"><a href="javascript:;"><i class="fa fa-user"></i>
                <span>{{Auth::guard('admin')->user()->name}}</span>
                <i class="fa fa-angle-right"></i></a>
                    <ul class="logout-modal">
                        <li><a href="/admin/change-password">Change Password</a></li>
                        <li><a href="<?= route('admin.logout') ?>"> Logout</a></li>
                    </ul>
                </li>                
            </ul>
        </div>
    </section>
</aside>