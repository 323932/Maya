<thead>
    <tr>
        <th align="center">Sr No</th>
        <th align="center">Product Name</th>
        <th align="center">Brand</th>
        <th align="center">Category</th>
        <th align="center">SKU</th>
        <th align="center">Price</th>
        <th align="center">Tax</th>
        <th align="center">Quantity of small size</th>
        <th align="center">Quantity of medium size</th>
        <th align="center">Quantity of large size</th>
        <th align="center">Quantity of extra large size</th>
    </tr>
</thead>
<tbody id="append_body">
    @if(!empty($product_data))
        @foreach($product_data as $key=>$value)
        <tr>
            <td>{{ $key+1 }}</td>
            <td align="center">{{$value['name']}}</td>
            <td align="center">{{$value['sku']}}</td>
            <td align="center">{{$value['brand']}}</td>
            <td align="center">{{$value['category']}}</td>
            <td align="center">{{$value['original_price']}}</td>
            <td align="center">{{$value['tax']}}</td>
            <td align="center">{{$value['qty_s']}}</td>
            <td align="center">{{$value['qty_m']}}</td>
            <td align="center">{{$value['qty_l']}}</td>
            <td align="center">{{$value['qty_xl']}}</td>
        @endforeach
    @else
        <tr>
            <td colspan="6" align="center">No data available.</td>    
        </tr>
    @endif
</tbody>