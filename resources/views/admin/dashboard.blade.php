@extends('admin.layout.layout')
@section('style')
	<?= Html::style('backend/css/dataranger.css',[],IS_SECURE) ?>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
    <div class="title">
        <h4><i class="fa fa-dashboard"></i> Dashboard</h4>
    </div>    
</nav>
@stop
@section('script')
	<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
	<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?>
@include('admin.layout.alert')
@stop