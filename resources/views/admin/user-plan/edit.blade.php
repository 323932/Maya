@extends('admin.layout.layout')
@section('start_form')
    <?= Form::model($userPlan_data,['role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH','id'=>'user_form_data']) ?>
@stop
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save">Save</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('user-plan.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group  @if($errors->has('user_plan')) {{ 'has-error' }} @endif">
                                <label>Name <sup class="text-danger">*</sup></label>
                                <?=Form::text('user_plan',old('user_plan'), ['class' => 'form-control user_plan','id' => 'color','placeholder'=>'User Plan Name','maxlength'=>100]);?>
                                <span id="user_plan_error" class="help-inline text-danger"><?= $errors->first('user_plan') ?>&nbsp;</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('brand_id')) {{ 'has-error' }} @endif">
                                <label>Brand <sup class="text-danger">*</sup></label>
                                <?=Form::select('brand_id',$brand,old('brand_id'), ['class' => 'form-control select2_2 brand_id','id' => 'brand_id','placeholder'=>'Select Brand']);?>
                                <span id="brand_id_error" class="help-inline text-danger"><?= $errors->first('brand_id') ?>&nbsp;</span>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('category_id')) {{ 'has-error' }} @endif">
                                <label>Category <sup class="text-danger">*</sup></label>
                                <?=Form::select('category_id',$category,old('category_id'), ['class' => 'form-control select2_2 category_id','id' => 'category_id']);?>
                                <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('category_id') ?>&nbsp;</span>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group @if($errors->has('discount')) {{ 'has-error' }} @endif">
                                <label>Discount <sup class="text-danger">*</sup></label>
                                <?= Form::number('discount', old('discount'), ['class' => 'form-control number_only', 'placeholder' => 'Discount']); ?>
                                <span id="discount_error" class="help-inline text-danger"><?= $errors->first('discount') ?>&nbsp;</span>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-12"  id="product_chk">
                    <label>Products<sup class="text-danger">*</sup></label>
                    <span id="is_visible_error" class="help-inline text-danger"><?= $errors->first('is_visible') ?></span>
                    <input type="hidden" name="product_plans" value="<?= $product_flag ?>">
                    <div class="row">
                        <div class="col-md-12">
                            @foreach($products as $key => $value)
                                <div class="col-md-6">
                                    <div class="form-group @if($errors->has('is_visible')) {{ 'has-error' }} @endif">    
                                        <div class="utility">
                                            <div class="animated-checkbox">
                                                <label class="semibold-text">
                                                    <input name="is_visible[]" type="checkbox" value="{{$key}}" @if(array_key_exists($key,$old_product)) checked @endif><span class="label-text"><?= $value ?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach 
                        </div>
                    </div>
                </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="button" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn save_data" title="Save">Save</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_data" title="Save and exit">Save & exit </button>
            <a href="<?= route('user-plan.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?=Html::script('backend/js/jquery.fileuploader.min.js')?>
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
    <?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>

    <script type="text/javascript">

        $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&& 32!=h.which&&(h.which<48||h.which>57))return!1});

        var tag1 = $('#brand_id').select2({
            placeholder : "Select Brand"
    
        });

        var tag2 = $('#category_id').select2({
            placeholder : "Select Category"
    
        });
    </script>

    <!-- BRANDWISE CATEGORY -->
    <script type="text/javascript">
        $(document).ready(function() {

            var category_id = $('#brand_id');
            //console.log(state_id);
            $("#brand_id").on('change',function() {
                loadCategory($(this).val());
            });

            function loadCategory(val)
            {
                var token = "{{csrf_token()}}";

                $.ajax({
                    url        : "<?= route('user-plan.getcategory')?>",
                    type       : 'post',
                    data       : { "brand_id": val , '_token' : token },
                    success    : function(category_list) {
                        var city_options = $(category_list).html();
                        //console.log(city_options);
                        $('#category_id').val('').trigger('change');
                        $("#category_id").html(city_options);
                    }
                });
            }
            $("#category_id").on('change',function() {
                $('#product_chk').html('');
                if($(this).val() != ''){
                    loadProduct($(this).val());
                }
            });

            function loadProduct(val)
            {
                // console.log(val);
                var token = "{{csrf_token()}}";
                // console.log($('#brand_id').val());
                $.ajax({
                    url        : "<?=URL::route('user-plan.getproduct')?>",
                    type       : 'post',
                    data       : { "category_id": val , '_token' : token,"brand_id":$('#brand_id').val() },
                    dataType   : 'html',
                    success : function(data){
                        console.log(data);
                        $('#product_chk').html(data);
                    }
                })
            } 
        });
    </script>

    <script type="text/javascript">
        $('.save_data').click(function(e,ele)
        {
            var btn_val = $(this).val();
            var id = "<?=$userPlan_data['id']?>";
            var url = "<?=route('user-plan.update',':id')?>";
            url = url.replace(':id',id);
            var token = "<?=csrf_token()?>";
            $('#user_form_data').ajaxSubmit({
                url: url,
                type: 'post',
                data: { "_token" : token},
                dataType: 'json', 
                beforeSubmit : function()
                {
                   $("[id$='_error']").empty();
                   $("[id$='_div']").removeClass('has-error');
                },           
                success : function(resp)
                {     
                    
                    toastr.success("Record updated successfully."); 
                    $(".null_value_class").val("");
                    if(btn_val == 'save_new')
                    {
                        location.reload();
                    }
                    else
                    {
                        location.href = "<?= route('user-plan.index') ?>"
                    }
                },
                error : function(respObj){
                    toastr.error('There was some error');
                    $.each(respObj.responseJSON.errors, function(k,v){
                        $('#'+k+'_error').text(v);
                        $('#'+k+'_div').removeClass('has-error');
                        $('#'+k+'_div').addClass('has-error');
                    }); 
                }
            });
        });
    </script>

    @include('admin.layout.alert')
@stop