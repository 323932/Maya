
<label>Products<sup class="text-danger">*</sup></label>
<span id="is_visible_error" class="help-inline text-danger"><?= $errors->first('is_visible') ?></span>
<div class="row">    
    <div class="col-md-12" id="product_chk">
        @foreach($products as $key=>$product)
            <div class="col-md-6">
                <div class="form-group @if($errors->has('is_visible')) {{ 'has-error' }} @endif">
                    <div class="utility">
                        <div class="animated-checkbox">
                            <label class="semibold-text">
                              <input name="is_visible[]" type="checkbox" value="{{$key}}" class=" null_value_class check_{{$key}}"><span class="label-text">{{$product}}</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach   
    </div>
</div>
    <script type="text/javascript">
        var old_tag = {!! json_encode(old('is_visible')) !!};
        console.log(old_tag);
       
        $.each(old_tag,function(k,v){
           $(".check_"+v).attr('checked',true);
        });
    </script>