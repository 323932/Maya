@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>    
    <?= Html::style('backend/css/datepicker.css') ?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::open(['route'=>'coupons.store','role'=>'form','class'=>'m-0','files'=>true]) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('coupons.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('user_id')) {{ 'has-error' }} @endif">
                                    <label>User <sup class="text-danger">*</sup></label>
                                    <?=Form::select('user_id[]',$user,old('user_id[]'), ['class' => 'form-control select2_2 user_id ','id' => 'user_id','data-placeholder'=>'Select User','multiple'=>true]);?>
                                    <span id="code_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('code')) {{ 'has-error' }} @endif">
                            <label>Coupon Code <sup class="text-danger">*</sup></label>
                            <?= Form::text('code', null, ['class' => 'form-control', 'placeholder' => 'Coupon Code']); ?>
                            <span id="code_error" class="help-inline text-danger"><?= $errors->first('code') ?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('discount')) {{ 'has-error' }} @endif">
                            <label>Discount<sup class="text-danger">*</sup></label>
                            <?=Form::select('discount',['Flat'=>'Flat','Percentage'=>'Percentage'],old('discount'), ['class' => 'form-control select2_2 discount','id' => 'discount','placeholder'=>'Select Discount']);?>
                            <span id="discount_error" class="help-inline text-danger"><?= $errors->first('discount') ?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('percentage')) {{ 'has-error' }} @endif">
                            <div class="select_per">
                                <label>Percentage</label>
                                <?= Form::text('percentage', null, ['class' => 'form-control number_only', 'placeholder' => 'Percentage']); ?>
                                <span id="percentage_error" class="help-inline text-danger"><?= $errors->first('percentage') ?></span>
                            </div>
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('time')) {{ 'has-error' }} @endif">
                            <label> Coupon Code Time <sup class="text-danger">*</sup></label>
                            <?= Form::text('time', null, ['class' => 'form-control number_only', 'placeholder' => 'Coupon Code Time','maxlength' => 4]); ?>
                            <span id="time_error" class="help-inline text-danger"><?= $errors->first('time') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('from_date')) {{ 'has-error' }} @endif">
                            <label> From Date<sup class="text-danger">*</sup></label>
                            <?= Form::text('from_date', null, ['class' => 'form-control date', 'placeholder' => 'dd-mm-yyyy']); ?>
                            <span id="from_date_error" class="help-inline text-danger"><?= $errors->first('from_date') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('to_date')) {{ 'has-error' }} @endif">
                            <label> To Date<sup class="text-danger">*</sup></label>
                            <?= Form::text('to_date', null, ['class' => 'form-control date', 'placeholder' => 'dd-mm-yyyy']); ?>
                            <span id="to_date_error" class="help-inline text-danger"><?= $errors->first('to_date') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('coupons.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>    
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>    
    <?=Html::script('backend/js/bootstrap-datepicker.js')?> 
    <?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>
    <script type="text/javascript">

        $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&& 32!=h.which&&(h.which<48||h.which>57))return!1});

        var tag1 = $('#discount').select2({
            placeholder : "Select Discount"
    
        });
    </script>

    <!-- MULTISELECT -->
    <script type="text/javascript">
        var tag1 = $('#user_id').select2({
            placeholder : "Select Dealer"
    
        });
        @if(old('user_id'))
    
        var old_tag1 = {!! json_encode(old('user_id')) !!};        
        $('.user_id').val(old_tag1).trigger('change');         
        $.each(old_tag1,function(k,v){        
            $("#user_id").append($('<option>', {value: v, text: v}));    
        });     
        //tag1.val(old_tag1).trigger('change'); 
    @endif
    </script>
    <?=Html::script('backend/js/form.demo.min.js', [], IS_SECURE)?>

    <!-- DATEPICKER -->
    <script type="text/javascript">

        $('input[name="to_date"]').datepicker({ format : 'dd-mm-yyyy'});
        $('input[name="from_date"]').datepicker({ format : 'dd-mm-yyyy'}).on('changeDate', function(e){
            $('input[name="to_date"]').datepicker('setStartDate', e.date);
        });
    </script>

    <!-- HIDE SHOW DISCOUNT -->
   <!--  <script type="text/javascript">        
        $('#discount').change( function(){
            var discount = $('#discount').val();
                if(discount == "Percentage"){
                    $('.select_per').css('display','block');    
                }else{
                    $('.select_per').css('display','none');
                }
            });
    </script> -->
    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.select_per').css('display','none');
            var discounts = $('#discount').val();
            // console.log(discount);
            if(discounts == "Percentage"){
                console.log('hi');
                $('.select_per').show();    
            }
        });
    </script> -->

    <!-- <script type="text/javascript">        
        $('#discount').change( function(){
            var discount = $('#discount').val();
                if(discount == "Flat"){
                    $('.select_per').css('display','block');    
                }else{
                    $('.select_per').css('display','none');
                }
            });
    </script> -->
    <!-- <script type="text/javascript">
        $(document).ready(function(){
            $('.select_per').css('display','none');
            var discounts = $('#discount').val();
            // console.log(discount);
            if(discounts == "Flat"){
                console.log('hi');
                $('.select_per').show();    
            }
        });
    </script> -->

    @include('admin.layout.alert')
@stop