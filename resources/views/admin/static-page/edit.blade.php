@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::model($page_data,['route'=>['pages.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('pages.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('title')) {{ 'has-error' }} @endif">
                                    <label>Title <sup class="text-danger">*</sup></label>
                                    <?= Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']); ?>
                                    <span id="title_error" class="help-inline text-danger"><?= $errors->first('title') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group  @if($errors->has('description')) {{ 'has-error' }} @endif">
                            <label>Description<sup class="text-danger">*</sup></label>
                            <?= Form::textarea('description', null, ['class' => 'form-control','id'=>'editor1']); ?>
                            <span id="description_error" class="help-inline text-danger"><?= $errors->first('description') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('pages.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?= Html::script("https://cdn.ckeditor.com/4.7.1/standard-all/ckeditor.js") ?>
<script type="text/javascript">

    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.replace( 'editor1', {
        height: 300,

        // Configure your file manager integration. This example uses CKFinder 3 for PHP.
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}',


        toolbar: [
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
        { name: 'paragraph',  items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        // { name: 'alignment', items : [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
        { name: 'clipboard',items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo']},
        '/',
        { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        { name : 'links',items : ['Link','Unlink','Anchor']},
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'others', items: [ '-' ] },
        ],
        removeButtons: '',
        image_previewText: ' '
    });
</script>
    @include('admin.layout.alert')
@stop