@extends('admin.layout.layout')
@section('start_form')
    <?= Form::model($banner_data,['route'=>['banners.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
@stop
@section('top_fixed_content')
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & Exit </button>
            <a href="<?= route('banners.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop 
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('title')) {{ 'has-error' }} @endif">
                            <label>Title<sup class="text-danger">*</sup></label>
                            <?= Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title','id'=>'title','maxlength'=>100]); ?>
                            <span id="order_price_error" class="help-inline text-danger"><?= $errors->first('title') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('description')) {{ 'has-error' }} @endif">
                            <label>Description</label>
                            <?= Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','id'=>'description','rows'=>2]); ?>
                            <span id="description_error" class="help-inline text-danger"><?= $errors->first('description') ?>&nbsp;</span>
                        </div>
                    </div>                      
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('type')) {{ 'has-error' }} @endif">
                            <label>Type <sup class="text-danger">*</sup></label>
                            <?=Form::select('type',['product'=>'Product','category'=>'Category','brand'=>'Brand'],old('type'), ['class' => 'form-control select2_2 type','id' => 'type','placeholder'=>'Select Type']);?>
                            <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('type') ?></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('action_id')) {{ 'has-error' }} @endif">
                            <label>Name<sup class="text-danger">*</sup></label>
                            <?=Form::select('action_id',$banner,old('action_id'), ['class' => 'form-control select2_2','id' => 'action_id','placeholder'=>'Select Name']);?>
                            <div id="name_box"></div>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('action_id') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('status')) {{ 'has-error' }} @endif">
                            <label>Status <sup class="text-danger">*</sup></label>
                            <?=Form::select('status',[1=>'Enable',0=>'Disable'],old('status'), ['class' => 'form-control select2_2 status','id' => 'status','placeholder'=>'Select Type']);?>
                            <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('status') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('image')) {{ 'has-error' }} @endif">
                            <label>Image <sup class="text-danger">*</sup></label>
                            <div class="inputfile-box" id="image_error_div">
                                <input class="inputfile image" id="file" type="file" name="image">
                                <label for="file"><span class="file-box" id="file-name"></span><span class="file-button">Upload</span></label>
                                <a href='<?= $banner_data['image'] ?>' target="_blank">View</a>
                                <span id="image_error" class="help-inline text-danger"><?= $errors->first('image') ?></span>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>    
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & Exit </button>
            <a href="<?= route('banners.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/jquery.form.min.js') ?> 
    <?= Html::script('backend/js/select2.min.js') ?>

    <script type="text/javascript">
        $(document).ready(function(){

            var filename = "<?= $banner_image[5] ?>";
                $('#file-name').text(filename);

            $('#file').change(function(){
                $('#file-name').text($(this)[0].files[0].name);
            });
        });
    </script>


    <script type="text/javascript">
        var tag1 = $('#type').select2({
            placeholder : "Select Type"
    
        });
        var tag2 = $('#action_id').select2({
            placeholder : "Select Name"
    
        });
        var tag3 = $('#status').select2({
            placeholder : "Select Status"
    
        });
    </script> 
    
    <script type="text/javascript">
        $(document).ready(function(){

            $("#type").on('change',function() {
               loadName($(this).val());
            });

            function loadName(val)
            {
                var token = "{{csrf_token()}}";
                $.ajax({
                    url        : "<?=URL::route('get.name.banner')?>",
                    type       : 'post',
                    data       : { "type": val , '_token' : token },
                    dataType   : 'html',
                    success    : function(names_list) {
                        var options = $(names_list).html();
                        //console.log($names_list);
                        $('#action_id').val('').trigger('change');
                        $("#action_id").html(options).val({{ old('action_id') }});
                    }
                });
            }
        });
    </script>
    @include('admin.layout.alert')
@stop