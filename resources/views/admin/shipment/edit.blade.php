@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::model($shipment_data,['route'=>['shipment.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and add new">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('shipment.index') ?>" class="btn btn-default btn-sm" title="Back to users Page">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if($errors->has('file')) {{ 'has-error' }} @endif">
                                <label>Contact Name <sup class="text-danger">*</sup></label>
                            <?= Form::text('contact_name', null, ['class' => 'form-control', 'placeholder' => 'Enter contact Name']); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('contact_name') ?></span>
                                </div>
                                <div class="form-group @if($errors->has('file')) {{ 'has-error' }} @endif">
                                <label>Contact Email <sup class="text-danger">*</sup></label>
                            <?= Form::text('contact_email', null, ['class' => 'form-control', 'placeholder' => 'Enter Contact Email']); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('contact_email') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <label>City <sup class="text-danger">*</sup></label>
                            <?= Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'city']); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('city') ?></span>
                        </div>
                        <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <label>Mobile<sup class="text-danger">*</sup></label>
                            <?= Form::text('mobile', null, ['class' => 'form-control', 'placeholder' => 'mobile']); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('mobile') ?></span>
                            
                        </div>
                    </div>
                   
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and add new">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('shipment.index') ?>" class="btn btn-default btn-sm" title="Back to users Page">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    @include('admin.layout.alert')
@stop