<div class="row">
    <div class="card col-md-12 mb-30">
        <div class="bg-white p-5 row m-0 box-shadow">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group row">
                        <label class="control-label col-md-3">FirstName<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('firstname')) {{ 'has-error' }} @endif">
                            <?= Form::text('firstname',old('firstname'),['class' => 'form-control','placeholder'=>'First Name','id'=>'firstname']); ?>
                            <span id="firstname_error" class="help-inline text-danger error_msg"><?= $errors->first('firstname') ?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">LastName<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('lastname')) {{ 'has-error' }} @endif">
                            <?= Form::text('lastname',old('lastname'),['class' => 'form-control','placeholder'=>'Last Name','id'=>'lastname']); ?>
                            <span id="lastname_error" class="help-inline text-danger error_msg"><?= $errors->first('lastname') ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <div class="form-group row">
                        <label class="control-label col-md-3">Email<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <?= Form::text('email',old('email'),['class' => 'form-control','placeholder'=>'Email','id'=>'email']); ?>
                            <span id="email_error" class="help-inline text-danger error_msg"><?= $errors->first('email') ?></span>
                        </div>
                    </div>
                    @if(Request::segment(1) == 'admin' && Request::segment(3) == 'create')
                    <div class="form-group row">
                        <label class="control-label col-md-3">Password<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('password')) {{ 'has-error' }} @endif">
                            <?= Form::password('password',['class' => 'form-control mb-0','placeholder'=>'Password','id'=>'password']); ?>
                            <span id="password_error" class="help-inline text-danger error_msg"><?= $errors->first('password') ?></span>
                        </div>
                    </div> 
                </div>
                @endif
            </div>
        </div>
    </div>
   
</div>
