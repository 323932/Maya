@extends('admin.layout.layout')
@section('start_form')
    <?=Form::open(['route'=>'sales-person.store','role'=>'form','method' => 'POST', 'class' => 'm-0 form-horizontal', 'id'=>'status_form_data'])?>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4></h4>
    </div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
        <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
        <a href="<?= route('sales-person.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
</nav>
@stop
@section('content')
    <input type="hidden" name="id" id="id" value="">
    <div class="row">
    <div class="card col-md-12 mb-30">
        <div class="bg-white p-5 row m-0 box-shadow">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-group row">
                        <label class="control-label col-md-3">FirstName<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('firstname')) {{ 'has-error' }} @endif">
                            <?= Form::text('firstname',old('firstname'),['class' => 'form-control char_only','placeholder'=>'First Name','id'=>'firstname']); ?>
                            <span id="firstname_error" class="help-inline text-danger error_msg"><?= $errors->first('firstname') ?></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-3">LastName<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('lastname')) {{ 'has-error' }} @endif">
                            <?= Form::text('lastname',old('lastname'),['class' => 'form-control char_only','placeholder'=>'Last Name','id'=>'lastname']); ?>
                            <span id="lastname_error" class="help-inline text-danger error_msg"><?= $errors->first('lastname') ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <div class="form-group row">
                        <label class="control-label col-md-3">Email<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <?= Form::text('email',old('email'),['class' => 'form-control','placeholder'=>'Email','id'=>'email']); ?>
                            <span id="email_error" class="help-inline text-danger error_msg"><?= $errors->first('email') ?></span>
                        </div>
                    </div><div class="form-group row">
                        <label class="control-label col-md-3">Password<sup class="text-danger">*</sup></label>
                        <div class="col-md-9 @if($errors->has('password')) {{ 'has-error' }} @endif">
                            <?= Form::password('password',['class' => 'form-control mb-0','placeholder'=>'Password','id'=>'password']); ?>
                            <span id="password_error" class="help-inline text-danger error_msg"><?= $errors->first('password') ?></span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>  
</div>
<div class="text-right">
    <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
    <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
    <a href="<?= route('sales-person.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
</div>
@stop
<?=Form::close()?>

@section('script')
    <script type="text/javascript">
        $(".char_only").keypress(function (e) {
             if (e.which != 8 && e.which != 0 && e.which != 32 && (e.which < 97 || e.which > 122)&& (e.which < 65 || e.which > 90))return !1           
           });
    </script>
@stop


