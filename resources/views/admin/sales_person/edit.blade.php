@extends('admin.layout.layout')
@section('start_form')
    <?=Form::model($sales_person_data,['route'=>['sales-person.update',$id],'role'=>'form','method' => 'PATCH', 'class' => 'm-0 form-horizontal', 'id'=>'status_form_data'])?>
@stop
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4></h4>
    </div>
    <div class="pl-10">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
        <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
        <a href="<?= route('sales-person.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
</nav>
@stop
@section('content')
    <input type="hidden" name="id" id="id" value="">
@include('admin.sales_person.form')
<div class="text-right">
    <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
    <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
    <a href="<?= route('sales-person.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
</div>
@stop
<?=Form::close()?>


