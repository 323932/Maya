@extends('admin.layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
    <div class="title">
        <h4><i class="fa fa-list"></i>Sales Person</h4>
    </div>
    <div class="pl-10">
        <a href="<?=route('sales-person.create')?>" class="btn btn-primary btn-sm" title="Add New"> Add New</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table id="salesperson_table" class="table table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>
@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>

<script type="text/javascript">

$(function(){
    $('#salesperson_table').DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": true,
        lengthMenu: [
                [ 10, 25, 50, 100,200,500],
                [ '10', '25', '50','100','200','500']
            ],
        "sAjaxSource": "<?= route('sales-person.index')?>",
        "aaSorting": [ 0,"desc"],
        "aoColumns": [
            { mData:"updated_at",bSortable : false,sClass : 'text-center',sWidth:"16%",bVisible : false},
            { mData:"firstname",sWidth:"30%",bSortable : true},
            { mData:"lastname",sWidth:"20%",bSortable : true},
            { mData:"email",bSortable : true ,sWidth:"30%"},
        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
});
</script>
@include('admin.layout.alert')
@stop