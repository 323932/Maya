<div class="card product_detail">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <table style="width: 100%" class="table table-bordered">
                        <tr>
                            <td rowspan="2"> Product Name </td>
                            <td> Quantity </td>
                            <td rowspan="2"> Total </td>
                        </tr>
                        <tr>
                            <td style="padding: 0">
                                <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                    <tr>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px  ">S</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px  ">M</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px  ">L</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 75px  ">XL</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                            <tr>
                                <td></td>
                                <td style="padding: 0">
                                    <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                        <tr>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"><?= $order_data_info[] ?></td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"></td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"></td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 75px"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                            </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>




<div class="card product_detail">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Product Detail</h4>
                </div>
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <table class="table m-0 v-top verticle-align">
                                <thead>
                                    <tr>
                                        <th style="border:none">Brand<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Category<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Product<sup class="text-danger">*</sup></th>
                                        <th style="border:none">Quantity of small size <sup class="text-danger">*</sup></th>
                                        <th style="border:none">Quantity of medium size <sup class="text-danger">*</sup></th>
                                        <th style="border:none">Quantity of large size <sup class="text-danger">*</sup></th>
                                        <th style="border:none">Quantity of extra large size <sup class="text-danger">*</sup></th>
                                        <th style="border:none">Total<sup class="text-danger">*</sup></th>
                                        <th style="border:none"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr id="product">
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?=Form::select('brand_id',$brand,old('brand_id'), ['class' => 'form-control brand_id','id' => 'brand_id','placeholder'=>'Select Brand']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.brand_id') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?=Form::select('category_id',$category,old('category_id'), ['class' => 'form-control category_id','id' => 'category_id','placeholder'=>'Select Category']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.category_id') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?=Form::select('product_id',$product,old('product_id'), ['class' => 'form-control product_id null_value_class','id' => 'multi_product_id','placeholder'=>'Select Product']);?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.product_id') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?=Form::text('qty_s',old('qty_s'), ['class' => 'form-control qty_s null_value_class','id'=>'qty_s','placeholder'=>'Quantity of small size']);?>
                                            <span id="qty_s_error" class="help-inline text-danger"><?= $errors->first('product.product.qty_s') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?= Form::text('qty_m', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Quantity of medium size','id'=>'qty_m']); ?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.qty_m') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?= Form::text('qty_l', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Quantity of large size','id'=>'qty_l']); ?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.qty_l') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?= Form::text('qty_xl', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Total','id'=>'qty_xl']); ?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.qty_xl') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <?= Form::text('total', null, ['class' => 'form-control null_value_class', 'placeholder' => 'Total','id'=>'total','readOnly'=>true]); ?>
                                            <span id="product_error" class="help-inline text-danger"><?= $errors->first('product.product.total') ?></span>
                                        </td>
                                        <td class="col-md-1" style="border:none" valign="top">
                                            <a id="product_remove" class="pt-10 pull-left btn-remove"><i class="fa fa-minus-circle fa-small pull-left"></i></a>
                                            <a id="product_add" class="pt-10 pull-left btn-add" ><i class="fa fa-plus-circle fa-small pull-left" ></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>


    <!-- DYNAMIC FORM -->
    <script type="text/javascript">
        var product_detail =  $("#product").dynamicForm("#product_add", "#product_remove", {
            limit: 10,
            normalizeFullForm : false,
        });

        old_data = <?= json_encode(old('product.product',$order_data_info)) ?>;
        // console.log(old_data);
        product_detail.inject(old_data);

        var select_list = $('tbody tr');

        @if($errors)
            var detail_Errors = <?= json_encode($errors->toArray()) ?>;        
            $.each(detail_Errors, function(id,msg){
                var id_arr = id.split('.');
                if (id_arr[3] == 'brand_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'category_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'product_id') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'qty_s') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'qty_m') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'qty_l') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'qty_xl') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
                if (id_arr[3] == 'total') {
                    $('#'+id_arr[3]+id_arr[id_arr.length-2]).closest('td').find('span').text(msg[0]);
                }
            });
        @endif        
    </script>  