@extends('admin.layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Order Master</h4>
    </div>
    <div class="top_filter"></div>
    <div class="pl-10">
        <a href="<?= route('orders.create') ?>" class="btn btn-primary btn-sm" title="Add New">Add New</a>
    </div>
</nav>
@stop
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown">
                                <a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected record</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="p-20">
                    <table id="supplier_table" class="table table-hover">
                        <thead>
                            <tr>
                                <th class="select-all no-sort">
                                    <div class="animated-checkbox">
                                        <label class="m-0">
                                            <input type="checkbox" id="checkAll" />
                                            <span class="label-text"></span>
                                        </label>
                                    </div>
                                </th>
                                <th>Order No</th>
                                <th>User</th>
                                <th>Order Price</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>
<div id="modal-consultant-profile" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body box no-border">
                <div id="modal-consultant-profile-info"></div>
                <!-- <div class="overlay" id="modal-consultant-profile-loader">
                    <i class="fa fa-spin fa-spinner"></i>
                </div> -->
            </div>
        </div>
    </div>
@stop
<!-- for error list show in model -->
@section('script')

<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>

<script type="text/javascript">
var table = "supplier_table";
var title = "Are you sure to delete this Order?";
var text = "You will not be able to recover this record";
var type = "warning";
var delete_path = "<?= route('orders.delete') ?>";
var token = "<?=csrf_token()?>";

$(function(){
    $('#delete').click(function(){
        var delete_id = $('#'+table+' tbody .checkbox:checked');
        checkLength(delete_id);
    });

    $('#supplier_table').DataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": false,
        lengthMenu: [
                [ 10, 25, 50, 100,200,500],
                [ '10', '25', '50','100','200','500']
            ],
        "sAjaxSource": "<?= route('orders.index')?>",
        "aaSorting": [ 1,"desc"],
        "aoColumns": [
            {   
                mData: "id",
                bSortable:false,
                sWidth:"2%",
                sClass:"text-center",
                mRender: function (v, t, o) {
                    return '<div class="animated-checkbox"><label class="m-0"><input class="checkbox" type="checkbox" id="chk_'+v+'" name="special_id['+v+']" value="'+v+'"/><span class="label-text"></span></label></div>';
                },
            },
            {   mData:"order_no",sWidth:"20%",bSortable : true},
        { mData:"user",bSortable : true,sClass : 'text-center',sWidth:"20%"},
        { mData:"order_price",bSortable : true,sClass : 'text-center',sWidth:"20%"},
        { mData:"created_at",bSortable : true,sClass : 'text-center',sWidth:"20%"},
        {
            mData: null,
            bSortable: false,
            sWidth: "12%",
            sClass: "text-center",
            mRender: function(v, t, o) {

                var editurl = "{{ route('orders.edit',':id')}}";
                editurl = editurl.replace(':id',o['id']);


                var act_html = "<div class='btn-group'>"
                +"<a href='"+editurl+"'class='btn btn-default btn-sm'><i class='fas fa-pen' title='Edit Record'></i></a>"
                +"</div>"
                return act_html;
            }
        },

        ],
        fnPreDrawCallback : function() { $("div.overlay").css('display','flex'); },
        fnDrawCallback : function (oSettings) {
            $("div.overlay").hide();
        },
    });
});
</script>

<!-- <script type="text/javascript">
    function Order(id)
    {
        console.log(id);
        $('#modal-consultant-profile-loader').show();
        $('#modal-consultant-profile').modal('show');

        var viewurl = "{{ route('orders.edit',':id')}}";
        viewurl = viewurl.replace(':id',id);

        $.get(viewurl, function(resp) {
            $('#modal-consultant-profile-loader').hide();
            $('#modal-consultant-profile-info').html(resp);
        });
    }
</script> -->
@include('admin.layout.alert')
@stop