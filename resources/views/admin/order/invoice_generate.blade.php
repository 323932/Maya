<!DOCTYPE html>
<html>
  <head>
  	<style type="text/css">
  		table{
			border: 1px solid #333;
			border-collapse: collapse;
			vertical-align: top
		}
		td,th{
			padding: 3px 5px
		}
    table.invoice-table{
      width: 100%;
    }
    table.invoice-table,table.invoice-table td,table.invoice-table th{
      border: 1px solid #333;
      border-collapse: collapse;
      vertical-align: top;
    }
  	</style>
  </head>
  <body style="font-family: 'Muli', sans-serif;">
    <div style="text-align: left;width: 100%;font-family: open sans;max-width: 580px;float: none;margin: 0 auto;border: 1px solid #686868 ;padding-top: 30px;">
      <div style="padding: 0px;text-align: left;display: inline-block;float: left;width: 100%;">
        <div style="margin-bottom: 20px;text-align: center;" ><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 80px"></div>
        <div>
          <div style="padding: 0 20px;">
            <div>
              <div style="font-size: 16px;margin-bottom: 10px;">
                DEAR CUSTOMER,<br/><br/>
        				REACHING YOU SOONER!<br/><br/>
        				AS A PART OF OUR ENDEAVOUR TO PROVIDE THE BEST SHOPPING EXPERIENCE TO OUR MOST VALUABLE CUSTOMER, YOUR PRODUCT WILL BE DELIVERED TO YOU AS SOON AS POSSIBLE.<br/><br/>
        				YOUR ORDER NO {{$data['order_no']}} HAVE BEEN SHIPPED.<br/><br/>
              </div>
      				<div style="display: flex;">
      					<table style="width: 48%;display: inline-block;margin: 0 3px;">
      						<tr >
      							<th width="50%">PACKAGE DETAILS  </th>
      						</tr>
      						<tr>
      							<td>
      								TRANSPORT-{{strtoupper($data['transport'])}}
      								<br><br>
      								PAYMENT MODE- {{strtoupper($data['payment_mod'])}}
      							</td>	
      						</tr>
      					</table>
      					<table style="width: 48%;display: inline-block;margin: 0 3px;">
      						<tr>
      							<th width="50%">DELIVERY ADDRESS DETAILS-</th>
      						</tr>
      						<tr>
      							<td>
      								OORJAA
      								<br><br>
      								{{strtoupper($data['shipping_address'])}}
      							</td>
      						</tr>
      					</table>
              </div>
            </div>
          </div>
        </div>
        <div style="padding: 0 20px;margin-top: 20px;">
          <table class="invoice-table">
            <tr>
              <th>SR no.</th>
              <th>Product</th>
              <th>QTY S</th>
              <th>QTY M</th>
              <th>QTY L</th>
              <th>QTY XL</th>
              <th>Price</th>
            </tr>
            @foreach($data['brand_arr'] as $brand_key=>$brand_value)
              <tr>
                <?php $brand_name = explode('_',$brand_key);?>
                <td colspan="7"><b> Brand: {{$brand_name[1]}}</b></td>
              </tr>
              @foreach($brand_value as $product_key=>$product_value)
              <tr>
                <td>{{$product_key + 1}}</td>            
                <td>{{$product_value['product_name']}}</td>
                <td>{{$product_value['qty_s']}}</td>
                <td>{{$product_value['qty_m']}}</td>
                <td>{{$product_value['qty_l']}}</td>
                <td>{{$product_value['qty_xl']}}</td>
                <td>{{$product_value['price']}}</td>
              </tr>
              @endforeach
            @endforeach
          </table>
        </div>
        <div style="text-align: left">
          <div style="padding:  20px;">
            <div>
              <div style="font-size: 16px;margin-bottom: 10px;">IF YOU HAVE ANY QUERY OR NEED FURTHER ASSISTANCE PLEASE CONTACT OUR HELP/SUPPORT SECTION.<br/><br/>
              HAPPY SHOPPING!<br/>TEAM MAAYAA CLOTHING.<br/>
                <br>
                <div style="margin-bottom: 20px;"><img src="<?= IMAGE_PATH.'images/maya-logo.png'?>" alt="logo" style="height: 40px"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div>
          <div style="background-color: #d6b146;color: #fff;font-size: 20px;padding:10px  20px;width: 100%;display: inline-block;box-sizing: border-box;text-align: center;">
            <div>Stay In Touch ! Find Us On </div>
            <div>
                <a href="www.facebook.com/maayaaclothingofficial"><img src="<?= IMAGE_PATH.'images/facebook-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.youtube.com/user/MrVipulchheda/channels"><img src="<?= IMAGE_PATH.'images/youtube-icon.png' ?>" style="margin:5px;"></a>
                <a href="www.instagram.com/maayaaclothingofficial/"><img src="<?= IMAGE_PATH.'images/instagram-icon.png' ?>" style="margin:5px;"></a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>