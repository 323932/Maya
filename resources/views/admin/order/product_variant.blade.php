<?php 

$brand_id=$b_id;
foreach($data as $key=>$value){
	$size = $value['name'];
?>
    <td class="col-md-1" style="border:none" valign="top">
        <?=Form::text('qty['.$brand_id.']['.$key.']',old('qty'), ['class' => 'form-control number_only qty','id'=>'qty_'.$key.'_'.$brand_id,'placeholder' => $size.' size','style'=>'width:60px']);?>
        <span id="qty[<?=$brand_id?>][]_error" class="help-inline text-danger qty"><?= $errors->first('qty['.$brand_id.'][]') ?></span>
        <input type="hidden" name="size[]" value="<?= $size?>">
    </td>
<?php
}
?>
<?=Form::hidden('q_id['.$brand_id.'][]',null, ['class' => 'form-control number_only qty','id'=>'qty','placeholder' => '0','style'=>'width:60px']);?>