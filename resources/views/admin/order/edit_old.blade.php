@extends('admin.layout.layout')
@section('start_form')
    <?= Form::model($order_data,['route'=>['orders.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
@stop
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <?= Html::style('backend/css/datepicker.css') ?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="button" name="save_button" class="btn btn-primary btn-sm disabled-btn" id="shipment" title="Add shipment">Add Shipment</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Add invoice">Add Invoice</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save">Save</button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Cancel">Back</a>
        </div>
    </nav>
@stop 
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('user_id')) {{ 'has-error' }} @endif">
                            <label>Order Date <sup class="text-danger">*</sup></label>
                            <?=Form::text('created_at',old('created_at'), ['class' => 'form-control created_at','id' => 'created_at','placeholder'=>'Order Date','readOnly'=>true]);?>
                            <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('payment_type')) {{ 'has-error' }} @endif">
                            <label>Order Status<sup class="text-danger">*</sup></label>
                            <?=Form::text('status',old('status'), ['class' => 'form-control status','id' => 'status','readOnly'=>true]);?>
                            <span id="sku_error" class="help-inline text-danger"><?= $errors->first('payment_type') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('order_price')) {{ 'has-error' }} @endif">
                            <label>Dealer Name<sup class="text-danger">*</sup></label>
                            <?= Form::text('user_id', null, ['class' => 'form-control','id'=>'user_id','readOnly'=>true]); ?>
                            <span id="order_price_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <label>Email<sup class="text-danger">*</sup></label>
                            <?=Form::text('email',old('email'), ['class' => 'form-control email','id' => 'email','readOnly'=>true]);?>
                            <span id="tax_error" class="help-inline text-danger"><?= $errors->first('email') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('category')) {{ 'has-error' }} @endif">
                            <label>Category<sup class="text-danger">*</sup></label>
                            <?= Form::text('category', null, ['class' => 'form-control','readOnly'=>true]); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('category') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('payment_type')) {{ 'has-error' }} @endif">
                            <label>Payment Type<sup class="text-danger">*</sup></label>
                            <?= Form::text('payment_type', null, ['class' => 'form-control','readOnly'=>true]); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('payment_type') ?></span>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>    
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Billing Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('billing_address')) {{ 'has-error' }} @endif">
                            <label>Address<sup class="text-danger">*</sup></label>
                            <?= Form::textarea('billing_address', null, ['class' => 'form-control', 'placeholder' => 'Address','rows'=>6,'readOnly'=>true]); ?>
                            <span id="billing_address_error" class="help-inline text-danger"><?= $errors->first('billing_address') ?></span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_area')) {{ 'has-error' }} @endif">
                                    <label>Area<sup class="text-danger">*</sup></label>
                                    <?= Form::text('billing_area', null, ['class' => 'form-control', 'placeholder' => 'Area','readOnly'=>true]); ?>
                                    <span id="billing_area_error" class="help-inline text-danger"><?= $errors->first('billing_area') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_state')) {{ 'has-error' }} @endif">
                                    <label>State<sup class="text-danger">*</sup></label>
                                     <?=Form::text('billing_state',old('billing_state'), ['class' => 'form-control status','id' => 'billing_state','placeholder'=>'Select State','readOnly'=>true]);?>
                                    <span id="billing_state_error" class="help-inline text-danger"><?= $errors->first('billing_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_city')) {{ 'has-error' }} @endif">
                                    <label>City<sup class="text-danger">*</sup></label>
                                     <?=Form::text('billing_city',old('billing_city'), ['class' => 'form-control status','id' => 'billing_city','placeholder'=>'Select City','readOnly'=>true]);?>
                                    <span id="billing_city_error" class="help-inline text-danger"><?= $errors->first('billing_city') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode<sup class="text-danger">*</sup></label>
                                    <?= Form::text('billing_pincode', null, ['class' => 'form-control number_only', 'placeholder' => 'Pincode','maxlength'=>6,'readOnly'=>true]); ?>
                                    <span id="billing_pincode_error" class="help-inline text-danger"><?= $errors->first('billing_pincode') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Shipping Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('billind_address')) {{ 'has-error' }} @endif">
                            <label>Address<sup class="text-danger">*</sup></label>
                            <?= Form::textarea('shipping_address', null, ['class' => 'form-control', 'placeholder' => 'Address','rows'=>5,'readOnly'=>true]); ?>
                            <span id="billind_address_error" class="help-inline text-danger"><?= $errors->first('billind_address') ?></span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_area')) {{ 'has-error' }} @endif">
                                    <label>Area<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_area', null, ['class' => 'form-control', 'placeholder' => 'Area','readOnly'=>true]); ?>
                                    <span id="shipping_area_error" class="help-inline text-danger"><?= $errors->first('shipping_area') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_state')) {{ 'has-error' }} @endif">
                                    <label>State<sup class="text-danger">*</sup></label>
                                     <?=Form::text('shipping_state',old('shipping_state'), ['class' => 'form-control status','id' => 'shipping_state','placeholder'=>'Select State','readOnly'=>true]);?>
                                    <span id="shipping_state_error" class="help-inline text-danger"><?= $errors->first('shipping_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_city')) {{ 'has-error' }} @endif">
                                    <label>City<sup class="text-danger">*</sup></label>
                                     <?=Form::text('shipping_city',old('shipping_city'), ['class' => 'form-control status','id' => 'shipping_city','placeholder'=>'Select City','readOnly'=>true]);?>
                                    <span id="shipping_city_error" class="help-inline text-danger"><?= $errors->first('shipping_city') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_pincode', null, ['class' => 'form-control number_only', 'placeholder' => 'Pincode','maxlength'=>6,'readOnly'=>true]); ?>
                                    <span id="shipping_pincode_error" class="help-inline text-danger"><?= $errors->first('shipping_pincode') ?></span>
                                </div>
                            </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card ">
        <div class="card-body">
            <div class="row">
                <h4>Line Items</h4>
                <div class="col-md-12">
                    <table style="width: 100%" class="table table-bordered">
                        <tr>
                            <td rowspan="2"> Product Name </td>
                            <td rowspan="2"> SKU </td>
                            <td> Quantity </td>
                            <td rowspan="2">Qty to Ship</td>
                        </tr>
                        <tr>
                            <td style="padding: 0">
                                <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                    <tr>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px ">S</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px  ">M</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px  ">L</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 75px  ">XL</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td></td>
                            <td style="padding: 0">
                                <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                    <tr>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"></td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"></td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 75px"></td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 75px"></td>
                                    </tr>
                                </table>
                            </td>
                            <td><?=Form::number('total',null,['size'=>2])?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card shipment_detail">
        <div class="card-body">
            <div class="row">
                <h4>Shipping Information</h4>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('shipping_title')) {{ 'has-error' }} @endif">
                            <label>Title<sup class="text-danger">*</sup></label>
                            <?=Form::text('shipping_title',old('shipping_title'), ['class' => 'form-control shipping_title','id' => 'shipping_title','placeholder'=>'Title']);?>
                            <span id="shipping_title_error" class="help-inline text-danger"><?= $errors->first('shipping_title') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('shipping_number')) {{ 'has-error' }} @endif">
                            <label>Number<sup class="text-danger">*</sup></label>
                            <?=Form::text('shipping_number',old('shipping_number'), ['class' => 'form-control shipping_number','id' => 'shipping_number']);?>
                            <span id="shipping_number_error" class="help-inline text-danger"><?= $errors->first('shipping_number') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('shipping_charge')) {{ 'has-error' }} @endif">
                            <label>Charge<sup class="text-danger">*</sup></label>
                            <?= Form::text('shipping_charge', null, ['class' => 'form-control','id'=>'shipping_charge']); ?>
                            <span id="shipping_charge_error" class="help-inline text-danger"><?= $errors->first('shipping_charge') ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script') 
    <script type="text/javascript">
        $('.shipment_detail').css('display','none');
        $('#shipment').click( function(){
            $('.shipment_detail').css('display','block');
        });
    </script> 
    @include('admin.layout.alert')
@stop