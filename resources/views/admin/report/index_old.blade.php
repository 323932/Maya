@extends('admin.layout.layout')
@section('top_fixed_content')
    <?= Html::style('backend/css/dataranger.css',[],IS_SECURE) ?>
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Report Master</h4>
    </div>
    <div class="top_filter"></div>
</nav>

@stop
@section('content')
<div class="row">
    <?=Form::open(['route'=>'reports.store','role'=>'form','class'=>'m-0','method'=>'POST'])?>
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                    <div class="col-sm-3 col-md-3 mb-10" style="float: right">
                        <a href="<?= route('reports.index')?>" type="submit" class="btn btn-primary btn-sm" title="Export to CSV" style="float: right">Refresh</a>
                        <button type="submit" class="btn btn-primary btn-sm mr-10" title="Export to CSV" style="float: right" id="btn-display">Export</a>
                    <div class="col-sm-3 col-md-3 mb-10" style="float: right">
                    </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 col-md-3">
                        <div>
                        <?=Form::select('product_type',$product_data, "", array('class' => 'form-control select type_wise', 'id' => 'product_type','data-attr'=>'product'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div >
                        <?=Form::select('dealer_type',$dealer_data, "", array('class' => 'form-control select type_wise', 'id' => 'dealer_type','data-attr'=>'dealer'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div >
                        <?=Form::select('sales_person_data',$sales_person_data, "", array('class' => 'form-control select type_wise', 'id' => 'sales_person_data','data-attr'=>'sale'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div >
                        <?=Form::select('state_data',$state_data, "", array('class' => 'form-control select type_wise', 'id' => 'state_data','data-attr'=>'state'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div >
                        <?=Form::select('brand_data',$brand_data, "", array('class' => 'form-control select type_wise', 'id' => 'brand_data','data-attr'=>'brand'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div>
                        <?=Form::select('category_data',$category_data, "", array('class' => 'form-control select type_wise', 'id' => 'category_data','data-attr'=>'category'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <div >
                        <?=Form::select('price_data',[''=>'Select Price wise'] + config('Constant.price_value'), "", array('class' => 'form-control select type_wise', 'id' => 'price_data','data-attr'=>'price'))?>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mb-10">
                        <form id="frm_filter" name ="frm_filter">
                            <div class="form-group" style="margin-bottom: 10px">
                                <div class="input-group date_range">
                                    <div class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></div>
                                    <input type="text" name="yoy[]" class="form-control pull-right" id="date_range_1">
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                    
            </div>
        </div>
    <?= Form::close()?>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body table-responsive">
                <div class="text-right">
                    <div class="number-delete">
                        <ul>
                            <li>
                                <p class="mb-0"><span class="num_item"></span>Item Selected.</p>
                            </li>
                            <li class="bulk-dropdown">
                                <a href="javascript:;">Bulk actions<span class="caret"></span></a>
                                <div class="bulk-box">
                                    <div class="bulk-tooltip"></div>
                                    <ul class="bulk-list">
                                        <li><a href="javascript:void(0);" id="delete" class="delete-btn">Delete selected record</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <table id="supplier_table" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Order No</th>
                            <th>Qty(M) size</th>
                            <th>Qty(L) size</th>
                            <th>Qty(XL) size</th>
                            <th>Qty(XXL) size</th>
                            <th>Product Name</th>
                            <th>Category Name</th>
                            <th>Brand Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($order_products_data as $key=>$value)
                            <tr>
                                <td>{{$value['order_no']}}</td>
                                <td>{{$value['qty_s']}}</td>
                                <td>{{$value['qty_m']}}</td>
                                <td>{{$value['qty_l']}}</td>
                                <td>{{$value['qty_xl']}}</td>
                                <td>{{$value['product_name']}}</td>
                                <td>{{$value['category_name']}}</td>
                                <td>{{$value['brand_name']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @include('admin.layout.overlay')
        </div>
    </div>
</div>
@stop
<!-- for error list show in model -->
@section('script')

<?= Html::script('backend/js/jquery.form.min.js',[],IS_SECURE) ?>
<?=Html::script('backend/plugins/datatable/jquery.dataTables.min.js', [], IS_SECURE)?>
<?=Html::script('backend/plugins/datatable/dataTables.bootstrap.min.js', [], IS_SECURE)?>
<?= Html::script('backend/js/moment.min.js',[],IS_SECURE) ?>
<?= Html::script('backend/js/dateranger.js',[],IS_SECURE) ?> 
<?=Html::script('backend/plugins/sweetalert/sweetalert.min.js', [], IS_SECURE)?>
<?=Html::script('backend/js/fnstandrow.js', [], IS_SECURE)?>
<?=Html::script('backend/js/delete_script.js', [], IS_SECURE)?>
<?= Html::script('backend/js/select2.min.js',[],IS_SECURE) ?>

<script type="text/javascript">
    var table = $('#supplier_table').DataTable({
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10', '25', '50', 'Show all' ]
        ],
        "info" : false,
        "searching" : false
    });
    $('#btn-display').hide();
    if($('#show_all_type').is(':checked')){
        console.log('hi');
        $('.type_wise').prop('disabled',false);
    }
    $('.type_wise').change(function(){
        $('#btn-display').show();
    });
    $('#product_type').select2();
    $('#dealer_type').select2();
    $('#sales_person_data').select2();
    $('#state_data').select2();
    $('#brand_data').select2();
    $('#category_data').select2();
    $('#price_data').select2();
    var table = "supplier_table";
    var title = "Are you sure to delete this User?";
    var text = "You will not be able to recover this record";
    var type = "warning";
    var delete_path = "<?= route('products.delete') ?>";
    var token = "<?=csrf_token()?>";
    $('#product_type').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#dealer_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#dealer_type').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#sales_person_data').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#dealer_type').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#state_data').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#dealer_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#brand_data').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#dealer_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#category_data').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#dealer_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#price_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    $('#price_data').change(function(){
        var product_type_val = $(this).val();
        var attr = $(this).attr('data-attr');
        $('#product_type').prop('disabled',true);
        $('#dealer_type').prop('disabled',true);
        $('#sales_person_data').prop('disabled',true);
        $('#state_data').prop('disabled',true);
        $('#brand_data').prop('disabled',true);
        $('#category_data').prop('disabled',true);
        ajax_call_type(product_type_val,attr);
    });
    function ajax_call_type(product_type_val,attr){
        var product_type_val = product_type_val;
        var attr = attr;
        var url = "<?= route('reports.index')?>";
        var token = "{{csrf_token()}}";
        var date = $('#date_range_1').val();
        var date_split = date.split('-');
        var fromdate = date_split[0].split("/").reverse().join("-").replace(/\s+/g, "");
        var todate = date_split[1].split("/").reverse().join("-").replace(/\s+/g, "");
        $.ajax({
            type : 'GET',
            url : url,
            data : {
                'type_value' : product_type_val,
                '_token' : token,
                'attr' : attr,
                'fromdate' : fromdate,
                'todate' : todate
            },
            success : function(data){
                $('#supplier_table').DataTable().destroy();
                var table = $('#supplier_table').DataTable({
                    lengthMenu: [
                        [ -1 ],
                        [ 'Show all' ]
                    ],
                    "info" : false,
                    "searching" : false
                });
                $('#supplier_table').html('');
                $('#supplier_table').append(data.response_html);
            }
        });
    }
    $('#date_range_1').click(function(){
        $(this).change(function(){
            var product_type_val = '';
            var attr = '';
            var url = "<?= route('reports.index')?>";
            var date = $('#date_range_1').val();
            var date_split = date.split('-');
            var fromdate = date_split[0].split("/").reverse().join("-").replace(/\s+/g, "");
            var todate = date_split[1].split("/").reverse().join("-").replace(/\s+/g, "");
            if($('#product_type').val() != ""){
                product_type_val = $('#product_type').val();
                attr = $('#product_type').attr('data-attr');
            }
            if($('#dealer_type').val() != ""){
                product_type_val = $('#dealer_type').val();
                attr = $('#dealer_type').attr('data-attr');
            }
            if($('#sales_person_data').val() != ""){
                product_type_val = $('#sales_person_data').val();
                attr = $('#sales_person_data').attr('data-attr');
            }
            if($('#state_data').val() != ""){
                product_type_val = $('#state_data').val();
                attr = $('#state_data').attr('data-attr');
            }
            if($('#brand_data').val() != ""){
                product_type_val = $('#brand_data').val();
                attr = $('#brand_data').attr('data-attr');
            }
            if($('#category_data').val() != ""){
                product_type_val = $('#category_data').val();
                attr = $('#category_data').attr('data-attr');
            }
            if($('#price_data').val() != ""){
                product_type_val = $('#price_data').val();
                attr = $('#price_data').attr('data-attr');
            }
            $.ajax({
                type : 'GET',
                url : url,
                data : {
                    'type_value' : product_type_val,
                    '_token' : token,
                    'attr' : attr,
                    'fromdate' : fromdate,
                    'todate' : todate,
                },
                success : function(data){
                    $('#supplier_table').html('');
                    $('#supplier_table').html(data.response_html);
                }
            });
        });
    });
    $('#date_range_1').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'This Year': [moment().startOf('year').startOf('days'),moment()],
            'Last Year': [moment().subtract('month',12).startOf('days'), moment().subtract(moment(),12).endOf('days')]
        },
        opens : "right",
        startDate: moment().startOf('year').startOf('days'),
        endDate: moment()
    });
</script>    
@include('admin.layout.alert')
@stop