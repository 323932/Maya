 <thead>
    <tr>
        <th>Order No</th>
        <th>Qty Small</th>
        <th>Qty Medium</th>
        <th>Qty Large</th>
        <th>Qty Xtra Large</th>
        <th>Product Name</th>
        <th>Categoy Name</th>
        <th>Brand Name</th>
    </tr>
</thead>

@foreach($order_data as $key=>$value)
	<tr>
        <td>{{$value['order_no']}}</td>
        <td>{{$value['qty_s']}}</td>
        <td>{{$value['qty_m']}}</td>
        <td>{{$value['qty_l']}}</td>
        <td>{{$value['qty_xl']}}</td>
        <th>{{$value['product_name']}}</th>
        <th>{{$value['category_name']}}</th>
        <th>{{$value['brand_name']}}</th>
	</tr>
@endforeach