
@if($attr == null)
<thead>
    <tr>
        <th>Order No</th>
        @foreach($qty_name as $data)
            @if($data['name'] != null)
                <th>{{$data['name']}}</th>
            @endif
        @endforeach
        <th>Product Name</th>
        <th>Category Name</th>
        <th>Brand Name</th>
    </tr>
</thead>

@foreach($order_products_data as $key=>$value)
    <tr>
        <th>{{$value['order_no']}}</th>
        @foreach($qty_name as $data)
            @if($data['name'] != null)
                <th>{{$data['qty']}}</th>
            @endif
        @endforeach
        <th>{{$value['product_name']}}</th>
        <th>{{$value['category_name']}}</th>
        <th>{{$value['brand_name']}}</th>
    </tr>
@endforeach
@else
     <thead>
    <tr>
        <th>Party</th>
        @if($attr == 'product')
            <th>SKU</th>
        @endif
        @if(!empty($qty_name))
            @foreach($qty_name as $data)
                @if($data['name'] != null)
                    <th>{{$data['name']}}</th>
                @endif
            @endforeach
        @endif
        <th>Rate</th>
        @if($attr == 'state')
        <th>State Name</th>
        @endif
        @if($attr == 'category')
        <th>Categoy Name</th>
        @endif
    </tr>
</thead>

@foreach($product_data as $key=>$value)
    <tr>
        <td>{{$value['firstname']}}</td>
        @if($attr == 'product')
            <td>{{$value['sku']}}</td>
        @endif
        @if(!empty($qty_name))
            @foreach($qty_name as $data)
                @if($data['name'] != null)
                    <th>{{$data['qty']}}</th>
                @endif
            @endforeach
        @endif
        <td>{{$value['order_price']}}</td>
        @if($attr == 'state')
            <td>{{$value['billing_state']}}</td>
        @endif
        @if($attr == 'category')
        <th>{{$value['name']}}</th>
        @endif
    </tr>
@endforeach

@endif
