@extends('admin.layout.layout')
@section('top_fixed_content')
<nav class="navbar navbar-static-top">
    <div class="title">
        <h4><i class="fa fa-list"></i>Manage Brands</h4>
    </div>
    <div class="text-right">
        <?= Form::label('','',['class'=>'col-sm-2 control-label'])?>
        <a href="<?= route('brands.index') ?>" class="btn btn-default btn-sm" title="Back">Back</a>
    </div>
</nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div id="sortable">
                            @foreach($brand_data as $data)
                                <div class="col-md-3" id="{{$data->id}}">
                                     <div class="box blue-border">
                                        <div class="box-header">
                                            <h5 class="box-title" style="height: 50px">{{$data->name}}</h6>
                                        </div>
                                        <div class="box-body no-padding">
                                            <img class="img-responsive" src="<?=asset(LOCAL_IMAGE_PATH.'brands/'.$data->logo_image, IS_SECURE)?>" style="width:100%">
                                        </div>  
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</section>
@stop


@section('script')

{{ Html::script('backend/js/jquery.min.js') }}
{{ Html::script('backend/js/draggable/jquery.mCustomScrollbar.concat.min.js') }}
{{ Html::script('backend/js/draggable/nprogress.js') }}
{{ Html::script('backend/js/draggable/jquery-ui.min.js') }}
{{ Html::script('backend/js/draggable/main.min.js') }}
{{ Html::script('backend/js/jquery.slimscroll.min.js')}}
{{ Html::script('backend/js/sweetalert.min.js') }}
    <script type="text/javascript">
    var token = "<?=csrf_token()?>";
    $(document).ready(function(){
        $( "#sortable" ).sortable({
            update: function(){ 
                var order = $(this).sortable('toArray',{attribute:'id' });
                console.log(order);
                $.ajax({
                    url: "{{ URL::route('brand.order') }}",
                    type: 'post',
                    data: { order: order,_token:token },
                    complete: function(){
                        toastr.success('Brand has been ordered successfully.');
                    }
                });
            }
        });
        $( "#sortable" ).disableSelection();
    }); 
    </script>
@stop