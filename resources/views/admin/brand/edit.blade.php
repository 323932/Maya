@extends('admin.layout.layout')
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>
        <?= Form::model($brand_data,['route'=>['brands.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
        <div class="pl-10">
            <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('brands.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
        </div>
    </nav>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group  @if($errors->has('name')) {{ 'has-error' }} @endif">
                            <label>Name <sup class="text-danger">*</sup></label>
                            <?= Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name','maxlength'=>50]); ?>
                            <span id="Name_error" class="help-inline text-danger"><?= $errors->first('name') ?></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12"><label>Image <sup class="text-danger">*</sup></label></div>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;">
                                                @if(empty($brand_data['image']))
                                                    <img src="<?=asset('backend/images/no_image.png', IS_SECURE)?>" alt="" />
                                                @else
                                                    <img src="<?=asset(LOCAL_IMAGE_PATH.'/brands/'.$brand_data['image'], IS_SECURE)?>" alt="" />
                                                @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <label class="btn btn-file">
                                                    <input type="file" name="image" class="form-control image_div_error">
                                                    <span  class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span  class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                </label>
                                                <a href="" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                            </div>
                                            <span id="image_error" class="help-inline text-danger"><?=$errors->first('image')?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-md-12"><label>Logo <sup class="text-danger">*</sup></label></div>
                                    <div class="col-md-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;">
                                                @if(empty($brand_data['logo_image']))
                                                    <img src="<?=asset('backend/images/no_image.png', IS_SECURE)?>" alt="" />
                                                @else
                                                    <img src="<?=asset(LOCAL_IMAGE_PATH.'/brands/'.$brand_data['logo_image'], IS_SECURE)?>" alt="" />
                                                @endif
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 190px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <label class="btn btn-file">
                                                    <?=Form::file('logo_image', ['class' => 'form-control image_div_error'])?>
                                                    <span  class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                    <span  class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                </label>
                                                <a href="" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                            </div>
                                            <span id="image_error" class="help-inline text-danger"><?=$errors->first('logo_image')?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @if($errors->has('description')) {{ 'has-error' }} @endif">
                            <label>Description</label>
                            <?= Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','rows'=>"2",'maxlength'=>300]); ?>
                            <span id="description_error" class="help-inline text-danger"><?= $errors->first('description') ?></span>
                        </div>
                        <div class="row"> 
                            <div class="col-md-12"> 
                                <div class="form-group @if($errors->has('size_id')) {{ 'has-error' }} @endif">
                                    <label>Size <sup class="text-danger">*</sup></label>
                                    <?=Form::select('size_id[]',$size,old('size_id[]'), ['class' => 'form-control select2_2 size_id ','id' => 'size_id','data-placeholder'=>'Select Size','multiple'=>true]);?>
                                    <span id="size_id_error" class="help-inline text-danger"><?= $errors->first('size_id') ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="form-group  @if($errors->has('hex_code')) {{ 'has-error' }} @endif">
                                    <label>Hex Code <sup class="text-danger">*</sup></label>
                                    <?= Form::text('hex_code', null, ['class' => 'form-control', 'placeholder' => 'Hex Code','maxlength' => 6]); ?>
                                    <span id="hex_code_error" class="help-inline text-danger"><?= $errors->first('hex_code') ?></span>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="form-group  @if($errors->has('title_color')) {{ 'has-error' }} @endif">
                                    <label>Title Color <sup class="text-danger">*</sup></label>
                                    <?= Form::text('title_color', null, ['class' => 'form-control', 'placeholder' => 'Title Color(Start with #)','maxlength' => 7]); ?>
                                    <span id="title_color_error" class="help-inline text-danger"><?= $errors->first('title_color') ?></span>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-md-2">Status</label>
                                    <div class="col-md-12" style="padding-top:10px;">
                                        <div class="animated-radio-button pull-left mr-10">
                                            <label for="is_active_true">
                                                <?=Form::radio('is_active', 1, true,['id' => 'is_active_true'])?>
                                                <span class="label-text"></span> Active
                                            </label>
                                        </div>
                                        <div class="animated-radio-button pull-left">
                                            <label for="is_active_false">
                                                <?=Form::radio('is_active', 0,null, ['id' => 'is_active_false'])?>
                                                <span class="label-text"></span> InActive
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and new">Save</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('brands.index') ?>" class="btn btn-default btn-sm" title="Cancel">Cancel</a>
    </div>
<?= Form::close() ?>
@stop

@section('script')
    <?= Html::script('backend/js/select2.min.js') ?>    
    <?= Html::script('backend/js/bootstrap-fileupload.js') ?>
    <?= Html::script('backend/js/jquery-migrate-1.2.1.min.js') ?>

    <!-- MULTISELECT -->
    <script type="text/javascript">
        var tag2 = $('#size_id').select2({
                placeholder : "Select User"
            
            });
            @if(old('size_id',$size_id))    
                var old_tag2 = {!! json_encode(old('size_id',$size_id)) !!};        
                $('.size_id').val(old_tag2).trigger('change');         
                $.each(old_tag2,function(k,v){        
                    $("#size_id").append($('<option>', {value: v, text: v}));    
                });     
                // tag2.val(old_tag2).trigger('change'); 
            @endif
    </script>
    @include('admin.layout.alert')
@stop