<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/testmail',function(){
	$email = 'karishma.thinktanker@gmail.com';
	$subject = 'as';

	\Mail::send('apimail.signup_mail',[], function ($message) use ($email,$subject) {
	        $message->to($email);
	        $message->subject($subject);
	        $message->from('noreply@thinktanker.in');
	});
	return 'success';
});
Route::post('register', 'Api\UserController@register');
Route::post('login','Api\UserController@login');
Route::post('checkuserstatus','Api\UserController@checkUserStatus');
Route::post('forgotpassword','Api\UserController@forgotPassword');
Route::post('getuseraddresses','Api\UserController@getUserAddresses');
Route::post('changepassword','Api\UserController@changePassword');
Route::post('editprofile','Api\UserController@editProfile');
Route::get('getshopowner','Api\UserController@getShopOwner');

Route::post('placeorder','Api\OrderController@placeOrder');
Route::post('getmyorders','Api\OrderController@getMyOrders');
Route::post('getorderdetail','Api\OrderController@getOrderDetail');

Route::post('productdetail','Api\ProductController@productDetail');
Route::post('productdetailnew','Api\ProductController@productDetailNew');
Route::post('getproduct','Api\ProductController@getProduct');
Route::post('getallproduct','Api\ProductController@getAllProduct');
Route::post('category_allproduct','Api\ProductController@categorywiseProduct');
Route::post('filter_product','Api\ProductController@FilterProduct');
Route::post('brand_category_allproduct','Api\CategoryController@brandwiseCategory');

Route::get('getallcategory','Api\CategoryController@getAllCategories');
Route::post('allcategory_brand','Api\CategoryController@getCategoryBrand');

Route::get('getallbanners','Api\HomeScreenController@getAllBanners');
Route::get('filteralldata','Api\FilterController@FilterList');

Route::get('getallbrands','Api\BrandController@getAllBrands');
Route::post('getbrandscategory','Api\BrandController@getBrandsCategory');
Route::post('getcollections','Api\BrandController@getAllCollections');

Route::get('getstates','Api\SearchController@getStates');
Route::post('getcities','Api\SearchController@getCities');
Route::post('getcouponcode','Api\SearchController@getCouponCode');
Route::get('getnotification','Api\SearchController@getNotification');
Route::post('search','Api\SearchController@search');

Route::group(array('middleware' => 'auth:api'),function() {
});


Route::post('/brands','Api\BrandController@store');
Route::get('/brands','Api\BrandController@index');
Route::get('/brands/{id}','Api\BrandController@show');
Route::post('/brands/update/{id}','Api\BrandController@update');
Route::get('/brands/delete/{id}','Api\BrandController@destroy');
