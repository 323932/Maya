<?php


Route::group(array('namespace'=>'Admin','prefix' => 'admin'), function () {

	Route::get('/admin', function(){
		return redirect()->route('admin.login.get');
	});

	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email.admin');

	Route::get('reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset.admin');

	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.reset');

	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login.get');

	Route::post('/login-post', 'Auth\AdminLoginController@login')->name('admin.login.post');

	Route::group(['middleware' => ['admin']], function () {

		//route for dashboard
		Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
		//route for logout
		Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
		//route for change-password
	});

});
