<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('admin.login.get');
});

// Route::get('admin/change-password',function(){
// 	return view('admin.changepassword');
// });
Route::get('admin/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login');

Route::post('admin/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email.admin');
Route::get('admin/password/reset/{token?}', 'Admin\Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('admin/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');

Route::post('admin/password/reset-password', 'Admin\Auth\ResetPasswordController@reset')->name('password.reset.post.password');

Route::get('/products/export','Admin\ProductController@exceltoexport')->name('products.export');
Route::post('notifications/names','Admin\NotificationController@getName')->name('get.name.notification');
Route::post('banners/names','Admin\BannerController@getName')->name('get.name.banner');
Route::get('admin/category/categoryorder','Admin\CategoryController@CategoryOrder')->name('category.order.get');
Route::get('admin/brand/brandorder','Admin\BrandController@BrandOrder')->name('brand.order.get');
Route::get('admin/banner/bannerorder','Admin\BannerController@BannerOrder')->name('banner.order.get');
Route::get('admin/products/suggetion','Admin\ProductController@CollectionSuggetion')->name('products.collection.suggetion');
Route::get('admin/product/suggetion/{id}','Admin\ProductController@CollectionSuggetionEdit')->name('products.suggetion.update');

Route::get('dealer/reset','Admin\UserController@ResetApiViewFile')->name('dealer.reset');
Route::post('api/reset/password','Admin\UserController@ApiResetPassword')->name('dealer.reset.password');

Route::group(['namespace'=>'Admin','prefix'=>'admin','middleware'=>'admin'],function(){
Route::post('dealers/deactive/{id}','UserController@deActive')->name('dealers.deactive');
	Route::resource('/brands','BrandController');
	Route::post('brands/delete','BrandController@delete')->name('brands.delete');
	Route::post('brand/order','BrandController@Order')->name('brand.order');	

	Route::resource('/category','CategoryController');
	Route::post('category/delete','CategoryController@delete')->name('category.delete');
	Route::post('category/order','CategoryController@Order')->name('category.order');

	Route::resource('/products','ProductController',['except'=>'update']);
	Route::post('/products/delete','ProductController@delete')->name('products.delete');
	Route::post('/products/update/{id}','ProductController@update')->name('products.update');
	Route::post('/products/category','ProductController@getCategory')->name('products.getcategory');
	Route::post('/products/image','ProductController@deleteImage')->name('delete.image');
	Route::post('/products/multipleimage','ProductController@UploadImage')->name('upload.url');
	Route::post('/products/size','ProductController@getSize')->name('products.getsize');	

	Route::post('/orders/{id}','OrderController@update')->name('orders.update');
	Route::post('/orders/store/invoice','OrderController@invoiceStore')->name('orders.store.invoice');
	Route::get('/orders/cancel','OrderController@OrderCancel')->name('orders.cancel');

	Route::resource('/orders','OrderController',['except'=>'update']);
	Route::post('/orders/delete','OrderController@delete')->name('orders.delete');
	Route::post('/orders/store/city','OrderController@getCity')->name('orders.getcity');
	Route::post('/orders/store/shippingcity','OrderController@getShippingCity')->name('orders.shippingcity');
	Route::post('/orders/store/getproductdata', 'OrderController@getProductData')->name('orders.getproductdata');
	Route::post('/orders/store/getdealerdata', 'OrderController@getDealerData')->name('orders.getdealerdata');
	Route::post('/orders/store/size','OrderController@getSize')->name('orders.getsize');

	Route::post('/user-plan/store','UserPlanController@store')->name('user-plan.store');
	Route::post('/user-plan/update/{id}','UserPlanController@update')->name('user-plan.update');
	Route::resource('/user-plan','UserPlanController',['except'=>'store','update']);
	Route::post('/user-plan/delete','UserPlanController@delete')->name('user-plan.delete');
	Route::post('/user-plan/category','UserPlanController@getCategory')->name('user-plan.getcategory');
	Route::post('/user-plan/product','UserPlanController@getProduct')->name('user-plan.getproduct');

	Route::resource('/coupons','CoupanCodeController');
	Route::post('/coupons/delete','CoupanCodeController@delete')->name('coupons.delete');
	// Route::post('/coupans/category','CoupanCode@getCategory')->name('coupans.getcategory');

	Route::resource('/color','ColorController');
	Route::post('color/delete','ColorController@delete')->name('color.delete');

	Route::resource('/size','SizeController');
	Route::post('size/delete','SizeController@delete')->name('size.delete');

	Route::resource('/pages','StaticPageController');
	Route::post('pages/delete','StaticPageController@delete')->name('pages.delete');


	Route::resource('/shipment','ShipmentController');
	Route::post('shipment/delete','ShipmentController@delete')->name('shipment.delete');

	Route::resource('/notifications','NotificationController');
	Route::post('notifications/delete','NotificationController@delete')->name('notifications.delete');

	Route::resource('/banners','BannerController');
	Route::post('banner/order','BannerController@Order')->name('banner.order');	
	Route::post('banners/delete','BannerController@delete')->name('banners.delete');
	

	Route::get('/dealers/view/{id}','UserController@view')->name('dealers.view');
	Route::resource('/dealers','UserController',['except'=>'show']);

	Route::get('/pdf','PdfController@downloadpdf')->name('pdf.get');

	Route::resource('/sales-person','SalesPersonController');
	Route::post('/sales-person/delete','SalesPersonController@delete')->name('sales-person.delete');
	Route::resource('/reports','ReportController');

	Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth:admin']], function () {

			Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
			Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
	});
});
