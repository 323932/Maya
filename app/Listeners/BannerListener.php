<?php

namespace App\Listeners;

use App\Events\BannerEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\BannerJob;

class BannerListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BannerEvent  $event
     * @return void
     */
    public function handle(BannerEvent $event)
    {
        $banner_data = $event->banner_data;
        $banner_data = $this->Dispatch(new BannerJob($banner_data));
    }
}
