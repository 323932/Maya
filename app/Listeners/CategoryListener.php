<?php

namespace App\Listeners;

use App\Events\CategoryEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CategoryJob;

class CategoryListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CategoryEvent  $event
     * @return void
     */
    public function handle(CategoryEvent $event)
    {
        $category_data = $event->category_data;
        $category_data = $this->Dispatch(new CategoryJob($category_data));
    }
}
