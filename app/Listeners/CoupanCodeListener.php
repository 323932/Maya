<?php

namespace App\Listeners;

use App\Events\CoupanCodeEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\CoupanCodeJob;

class CoupanCodeListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CoupanCodeEvent  $event
     * @return void
     */
    public function handle(CoupanCodeEvent $event)
    {
        $coupan_data = $event->coupan_data;
        $coupan_data = $this->Dispatch(new CoupanCodeJob($coupan_data));
    }
}
