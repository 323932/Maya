<?php

namespace App\Listeners;

use App\Events\InvoiceEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\InvoiceJob;
use App\Jobs\SendInvoiceMailJob;

class InvoiceListener
{
    use DispatchesJobs;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceEvent  $event
     * @return void
     */
    public function handle(InvoiceEvent $event)
    {
        $invoice_data = $event->invoice_data;
        
        $data = $this->Dispatch(new InvoiceJob($invoice_data));

        $view = 'admin.order.invoice_generate';
        $subject = 'invoice';
        $emails = $data['order_data']['email'];

        $data = $data['order_data'];
        
        $invoice = $this->Dispatch(new SendInvoiceMailJob($view, $emails, $subject, $data,$data['filename']));

    }
}
