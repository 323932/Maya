<?php

namespace App\Listeners;

use App\Events\ColorEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\ColorJob;

class ColorListener
{
     use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ColorEvent  $event
     * @return void
     */
    public function handle(ColorEvent $event)
    {
         $color_data = $event->color_data;
        $color_data = $this->Dispatch(new ColorJob($color_data));
    }
}
