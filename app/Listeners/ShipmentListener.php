<?php

namespace App\Listeners;

use App\Events\ShipmentEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\ShipmentJob;

class ShipmentListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShipmentEvent  $event
     * @return void
     */
    public function handle(ShipmentEvent $event)
    {
        $shipment_data = $event->shipment_data;
        $shipment_data = $this->Dispatch(new ShipmentJob($shipment_data));
    }
}