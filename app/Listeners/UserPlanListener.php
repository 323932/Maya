<?php

namespace App\Listeners;

use App\Events\UserPlanEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\UserPlanJob;

class UserPlanListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserPlanEvent  $event
     * @return void
     */
    public function handle(UserPlanEvent $event)
    {
        $userPlan_data = $event->userPlan_data;
        $userPlan_data = $this->Dispatch(new UserPlanJob($userPlan_data));
    }
}
