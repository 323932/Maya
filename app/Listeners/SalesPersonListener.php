<?php

namespace App\Listeners;

use App\Events\SalesPersonEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\SalesPersonJob;

class SalesPersonListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SalesPersonEvent  $event
     * @return void
     */
    public function handle(SalesPersonEvent $event)
    {
        $sales_person_data = $event->sales_person_data ;
        $sales_person_data = $this->Dispatch(new SalesPersonJob($sales_person_data));
    }
}
