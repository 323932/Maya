<?php

namespace App\Listeners;

use App\Events\UpdateOrderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\UpdateOrderJob;

class UpdateOrderListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateOrderEvent  $event
     * @return void
     */
    public function handle(UpdateOrderEvent $event)
    {
        $update_order_data = $event->update_order_data;
        $update_order_data = $this->Dispatch(new UpdateOrderJob($update_order_data));
    }
}
