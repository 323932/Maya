<?php

namespace App\Listeners;

use App\Events\BrandEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\BrandJob;

class BrandListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BrandEvent  $event
     * @return void
     */
    public function handle(BrandEvent $event)
    {
        $brand_data = $event->brand_data;
        $brand_data = $this->Dispatch(new BrandJob($brand_data));
    }
}
