<?php

namespace App\Listeners;

use App\Events\OrderEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\OrderJob;

class OrderListener
{
    use DispatchesJobs;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderListener  $event
     * @return void
     */
    public function handle(OrderEvent $event)
    {
        $order_data = $event->order_data;
        $order_data = $this->Dispatch(new OrderJob($order_data));
    }
}
