<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
        'App\Events\BrandEvent' => [
            'App\Listeners\BrandListener',
        ],
        'App\Events\CategoryEvent' => [
            'App\Listeners\CategoryListener',
        ],
        'App\Events\ColorEvent' => [
            'App\Listeners\ColorListener',
        ],
        'App\Events\ShipmentEvent' => [
            'App\Listeners\ShipmentListener',
        ],
        'App\Events\ProductEvent' => [
            'App\Listeners\ProductListener',
        ],
        'App\Events\OrderEvent' => [
            'App\Listeners\OrderListener',
        ],
        'App\Events\UpdateOrderEvent' => [
            'App\Listeners\UpdateOrderListener',
        ],
        'App\Events\CoupanCodeEvent' => [
            'App\Listeners\CoupanCodeListener',
        ],
        'App\Events\UserPlanEvent' => [
            'App\Listeners\UserPlanListener',
        ],
        'App\Events\NotificationEvent' => [
            'App\Listeners\NotificationListener',
        ],
        'App\Events\BannerEvent' => [
            'App\Listeners\BannerListener',
        ],
        'App\Events\SalesPersonEvent' => [
            'App\Listeners\SalesPersonListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
