<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;

class UpdateOrderJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($update_order_data)
    {
        $this->update_order_data = $update_order_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $update_order_data= $this->update_order_data;
        $id = $update_order_data['id'];
    
        $save_detail = new OrderProduct();
        $save_detail->fill($update_order_data);
        if(is_exist($save_detail['id']))
        {
            
        }
        $save_detail->id = $save_detail['id']+1;
        // echo "<pre>";
        // print_r($save_detail[]);
        // exit();

        // $get_id = Order::where('id',$save_detail->id)->get();
        // $get_id->fill($order_data);
        // $get_id->save();
        // //dd($save_detail->id);
        $save_detail->save();

        $order_delete = OrderProduct::where('order_id',$save_detail->id)->delete();
        $order_details = $request->input('product.product');


        foreach($order_details as $key=>$details)
        { 
            $save_details = new OrderProduct();
            $save_details->order_id = $save_detail->id;
            //dd($save_details);
            $save_details->product_id = $details['product_id'];
            $save_details->brand_id = $details['brand_id'];
            $save_details->category_id = $details['category_id'];
            $save_details->qty_s = $details['qty_s'];
            $save_details->qty_m = $details['qty_m'];
            $save_details->qty_l = $details['qty_l'];
            $save_details->qty_xl = $details['qty_xl'];
            //dd($save_details);
            $save_details->save(); 
        }
    }
}
