<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Size;
use App\Models\ProductImage;
use App\Models\ProductSize;
use App\Models\ProductVariant;
use App\Models\PlanProductDetail;

class ProductJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($product_data)
    {
        $this->product_data = $product_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $product_data = $this->product_data;
        // dd($product_data);
        $id= $request->id;

        if(isset($product_data['id']))
        {
            $id = $product_data['id'];  
        }       
        $product_detail = json_encode($product_data['attribute']);
        $information_detail = json_encode($product_data['information']);
        $product_variant_detail = $product_data['product']['product'];
        // echo "<pre>";
        // print_r($product_variant_detail);
        // exit();
        $save_detail =Product::firstOrNew(['id' => $id]);               
        $save_detail->fill($product_data);   
        $save_detail->attribute = $product_detail;
        $save_detail->information = $information_detail;
        $save_detail->without_set  = $product_data['without_set'];
        //dd($save_detail);
        //dd($product_data['percentage']);
        if($product_data['without_set']== 1)
        {
             $save_detail->percentage=0;
        }
        $save_detail->save();  

        $delete_product_variant = ProductVariant::where('product_id',$save_detail->id)->delete();
        
        foreach ($product_variant_detail as $variant_key => $variant_value) {
            $product_variant_save = new ProductVariant();
            $product_variant_save->product_id = $save_detail->id;
            $product_variant_save->color_id = $variant_value['color_id'];
            $product_variant_save->save();

            $size = array_except($variant_value, ['color_id']);
            // dd($size);
            foreach ($size as $key => $value) {
                $result = Size::select('id')->where('name',$key)->get()->toArray();
                $final_result = implode(',',$result[0]);
                $product_size_save = new ProductSize();
                $product_size_save->product_id = $save_detail->id;
                $product_size_save->brand_id = $save_detail['brand_id'];
                $product_size_save->product_variant_id = $product_variant_save->id;
                $product_size_save->size_id = $final_result;
                 $product_size_save->qty = $value;
                $product_size_save->save();
                
            }

        }

        //IMAGE STORE IN PRODUCT IMAGE TABLE
        $old_image = ProductImage::where('product_id',$id)->value('image');
        if($request->hasfile('input-image'))
        {
            foreach($product_data['input-image'] as $k=>$v)
            {
                $file = $product_data['input-image'][$k];
                $filename = time()."_".$file->getClientOriginalName();

                $path = public_path().'/upload/products/';
                $file->move($path,$filename);

                $save_image = new ProductImage();
                //dd($save_detail->id);
                $save_image->image = $filename;
                $save_image->product_id = $save_detail->id;
               $save_image->save();
            }  
        }
        //STORE USER PLANS IN PLAN PRODUCT DETAIL
        $delete = PlanProductDetail::where('product_id',$save_detail->id)->delete();
        if(isset($product_data['is_visible'])){
        $product_visible = $product_data['is_visible'];
            foreach($product_visible as $key=>$details)
            {   
                $save_details = new PlanProductDetail();
                $save_details->product_id = $save_detail->id;
                $save_details->user_plan_id = $details;
                //dd($save_details);
                $save_details->save();
            }
        }
        //dd($save_details);

        // $delete_product_size = ProductSize::where('product_id',$save_detail->id)->delete();
        // //$result = [];
        // foreach ($product_variant_detail as $size_key => $size_value) {
        //     $size = array_except($size_value, ['color_id']);
        //     foreach ($size as $key => $value) {
        //         $result = Size::select('id')->where('name',$key)->get()->toArray();
        //     // print_r($result);
        //     // exit();
        //        //print_r($result);
        //         $final_result = implode(',',$result[0]);
        //         //print_r($final_result);
        //         $product_size_save = new ProductSize();
        //         $product_size_save->product_id = $save_detail->id;
        //         $product_size_save->brand_id = $save_detail['brand_id'];
        //         $product_size_save->product_variant_id = $product_variant_save->id;
        //         $product_size_save->size_id = $final_result;
        //          $product_size_save->qty = $value;
        //        // print_r($product_size_save);
        //         $product_size_save->save();

        //     }

        // }
        //dd('hi');
    }
}