<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\User;
use App\Helpers\FileHelp;

class BannerJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($banner_data)
    {
        $this->banner_data = $banner_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $banner_data = $this->banner_data;
        //  dd($banner_data);
        $id= $request->id;

        if(isset($banner_data['id']))
        {
            $id = $banner_data['id'];
        }        
        $save_detail = Banner::firstOrNew(['id' => $id]);
        $save_detail->fill($banner_data);
        $save_detail->action_id = $banner_data['action_id'];
        $old_image = $save_detail['image'];    
        $path = public_path().'/upload/banners/';
      
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {
                FileHelp::UnlinkImage($path, $old_image);
            }
            $file = $request->file('image');
            $filename = FileHelp::getfilename($file);
            $request->file('image')->move($path, $filename);
            $save_detail->image = $filename;

        }    
        $save_detail->save();
    }
}
