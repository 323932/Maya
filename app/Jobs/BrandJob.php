<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\BrandSize;
use App\Helpers\FileHelp;

class BrandJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($brand_data)
    {
        $this->brand_data = $brand_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $brand_data = $this->brand_data;
        //dd($brand_data);
        $id= $request->id;
        if(isset($brand_data['id']))
        {
            $id = $brand_data['id'];  
        }
        //dd($brand_data);
        $save_detail =Brand::firstOrNew(['id' => $id]);               
        $save_detail->fill($brand_data);

        if(!empty($save_detail['size_id'])){
            $save_detail->size_id = implode(',',$save_detail['size_id']);
        }
        $logo_image = $save_detail['logo_image'];
        //$save_detail->logo_image = $logo_image;
        //dd($save_detail);
        $old_image = $save_detail['image'];        
        $path = public_path().'/upload/brands/';
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {
                FileHelp::UnlinkImage($path, $old_image);
            }
            $file = $request->file('image');
            $filename = FileHelp::getfilename($file);
            //dd($filename);
            $request->file('image')->move($path, $filename);
            $save_detail->image = $filename;

        }
        if ($request->hasfile('logo_image')) 
        {
            if ($logo_image != null) {
                FileHelp::UnlinkImage($path, $logo_image);
            }
            $file = $request->file('logo_image');
            $filename = FileHelp::getfilename($file);
            $request->file('logo_image')->move($path, $filename);
            $save_detail->logo_image = $filename;
        }
        $save_detail->save();  

        $delete_brand_size = BrandSize::where('brand_id',$save_detail->id)->delete();
        $brand_size_detail = $brand_data['size_id'];
        //dd($brand_size_detail);
        foreach ($brand_size_detail as $key => $value) {
            $brand_size_save = new BrandSize();
            $brand_size_save->brand_id = $save_detail->id;
            $brand_size_save->size_id = $value;
            $brand_size_save->save();

        }
        $product_size_delete = BrandSize::select('size_id')->where('brand_id',$save_detail->id)->get()->toArray();
        //dd($brand_data['size_id']);
        $new_arr = [];
        foreach ($product_size_delete as $key => $value) {
            $new_arr[] = $value['size_id'];
        }

        // dd($new_arr);
        //dd(array_diff($brand_data['size_id'],$new_arr));

    }
}
