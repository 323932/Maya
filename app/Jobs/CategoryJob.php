<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;
use App\Models\Category;

class CategoryJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($category_data)
    {
        $this->category_data = $category_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $category_data = $this->category_data;
        $id= $request->id;
        if(isset($category_data['id']))
        {
            $id = $category_data['id'];  
        }

        $save_detail =Category::firstOrNew(['id' => $id]);               
        $save_detail->fill($category_data);
        $old_image = $save_detail['image'];        
        $path = public_path().'/upload/categories/';
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {
                FileHelp::UnlinkImage($path, $old_image);
            }
            $file = $request->file('image');
            $filename = FileHelp::getfilename($file);
            //dd($filename);
            $request->file('image')->move($path, $filename);
            $save_detail->image = $filename;

        }        
        $save_detail->save(); 
    }
}
