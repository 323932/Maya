<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\SalesPerson;

use App\Models\User;
class SalesPersonJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($sales_person_data)
    {
        $this->sales_person_data = $sales_person_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $sales_person_data = $this->sales_person_data;
        $id = $sales_person_data['id'];    
        $sales_person_save = User::firstOrNew(['id' => $id]);
        if($id == null)
        {
            $password = $sales_person_data['password'];
            $generate_password = \Hash::make($password);
        }
        else
        {
            $generate_password = $sales_person_save['password'];
        }
        $sales_person_save->fill($sales_person_data);

        $sales_person_save->password = $generate_password;
        $status = '1';
        $type = 'seller';
        $sales_person_save->status = $status;
        $sales_person_save->type = $type;
        //dd($sales_person_save);
        $sales_person_save->save();

        return; 
    }
}
