<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\UserPlan;
use App\Models\PlanProductDetail;

class UserPlanJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userPlan_data)
    {
        $this->userPlan_data = $userPlan_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $userPlan_data = $this->userPlan_data;
        $id= $request->id;
        if(isset($userPlan_data['id']))
        {
            $id = $userPlan_data['id'];  
        }

        $save_detail =UserPlan::firstOrNew(['id' => $id]);               
        $save_detail->fill($userPlan_data);
        $save_detail->save();
        
        $delete = PlanProductDetail::where('user_plan_id',$save_detail->id)->delete();
        if(isset($product_data['is_visible']))
        {
            $user_visible = $userPlan_data['is_visible'];
            foreach($user_visible as $key=>$details)
            {   
                $save_details = new PlanProductDetail();
                $save_details->user_plan_id = $save_detail->id;
                $save_details->product_id = $details;
                //dd($save_details);
                $save_details->save();
            }
        }
    }  
}
