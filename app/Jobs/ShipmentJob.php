<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Helpers\FileHelp;
use App\Models\Shipment;
use App\Models\Order;
use App\Models\ShipmentProduct;

class ShipmentJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shipment_data)
    {
        $this->shipment_data = $shipment_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $shipment_data = $this->shipment_data;
        // dd($shipment_data);
        $id= \Request::segment(3);

        if($shipment_data['is_shipment'] == 1){

            foreach ($shipment_data['product_id'] as $key => $value) {
                $qty[$key]['id'] = $value;
            }
            foreach ($shipment_data['ship_qty_s'] as $key => $value) {
                if($value == null){
                    $value = 0;
                }
                $qty[$key]['ship_qty_s'] = $value;
            }
            foreach ($shipment_data['ship_qty_m'] as $key => $value) {
                if($value == null){
                    $value = 0;
                }
                $qty[$key]['ship_qty_m'] = $value;
            }
            foreach ($shipment_data['ship_qty_l'] as $key => $value) {
                if($value == null){
                    $value = 0;
                }
                $qty[$key]['ship_qty_l'] = $value;
            }
            foreach ($shipment_data['ship_qty_xl'] as $key => $value) {
                if($value == null){
                    $value = 0;
                }
                $qty[$key]['ship_qty_xl'] = $value;
            }
            foreach ($shipment_data['price'] as $key => $value) {
                 if($value == null){
                    $value = 0.00;
                }
                $qty[$key]['price'] = $value;
            }
            $save_detail =new Shipment();
            $save_detail->shipping_title = $shipment_data['shipping_title'];
            $save_detail->shipping_number = $shipment_data['shipping_number'];
            $save_detail->charge = $shipment_data['charge'];
            $save_detail->order_id = $id;
           
            $save_detail->save();

            $shipment_id = $save_detail['id'];

            $total_price = 0;

            $shipment_ids = [];
            
            foreach ($qty as $qty_key => $qty_value) {
               
                $shipment_detail = new ShipmentProduct();
                $shipment_detail->shipment_id = $shipment_id;             
                $shipment_detail->product_id = $qty_value['id'];             
                $shipment_detail->qty_s = $qty_value['ship_qty_s'];             
                $shipment_detail->qty_m = $qty_value['ship_qty_m'];             
                $shipment_detail->qty_l = $qty_value['ship_qty_l'];             
                $shipment_detail->qty_xl = $qty_value['ship_qty_xl']; 
                $shipment_detail->price = $qty_value['price']; 

                $total_price = $total_price + $qty_value['price'];

                $shipment_detail->save();   
                $shipment_ids[$qty_key] = $shipment_detail['id'];         
            } 

            $shipment = Shipment::find($save_detail['id']);
            $shipment->total_price = $total_price;
            $shipment->save(); 

            
            $shipment_count = Shipment::where('order_id',$id)->count();
            $order = Order::find($id);
            
            if($shipment_count > 0){
               $order_details = Order::select('order_products.unit_price as order_price','order_products.qty_s','order_products.qty_m','order_products.qty_l','order_products.qty_xl')->where('orders.id',$id)->leftjoin('order_products','order_products.order_id','orders.id')->get()->toArray();

                $order_prices = [];
                $total = 0;
                foreach ($order_details as $key => $value) {
                    $total = $value['qty_s'] + $value['qty_m'] + $value['qty_l'] + $value['qty_xl']; 
                    $order_prices[] = $value['order_price'] * $total; 
                }

                $total_order_price = 0;
                foreach ($order_prices as $key => $value) {
                    $total_order_price = $total_order_price + $value;
                }
                

                $shipment_detail = Shipment::where('order_id',$id)->get()->toArray();
                $shipment_prices = [];
                foreach ($shipment_detail as $key => $value) {
                    $shipment_prices[] = $value['total_price']; 
                }

                $total_shipment_price = 0;
                foreach ($shipment_prices as $key => $value) {
                    $total_shipment_price = $total_shipment_price + $value;
                }
                $grand_total = $total_order_price - $total_shipment_price;
                
                if($grand_total == '0.00'){
                    $order->status = config('Constant.status.complete');
                }else{
                    $order->status = config('Constant.status.partially shipped');
                }
                $order->save();  
            }else{
                $order = Order::find($id);
                $order->status = $shipment_data['order_status'];
                $order->save();
            }
            
        }
        else{
                $order = Order::find($id);
                $order->status = $shipment_data['order_status'];
                $order->save();
            }

    }
}