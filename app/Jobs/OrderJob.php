<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;  
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\OrderSize;
use App\Models\Size;

class OrderJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_data)
    {
        $this->order_data = $order_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $order_data = $this->order_data;
        // print_r($order_data);
        // exit();
        $id= $request->id;
        if(isset($order_data['id']))
        {
            $id = $order_data['id'];  
        }
        $save_detail =Order::firstOrNew(['id' => $id]);               
        $save_detail->fill($order_data);
        $last_order = Order::select('order_no')->orderBy('id','desc')->first();
        $order_no = 'MY'. str_pad((substr($last_order['order_no'],2,8))+1,7,'0',STR_PAD_LEFT);
        $save_detail->order_no = $order_no;   
        $save_detail->save(); 

        $order_delete = OrderProduct::where('order_id',$save_detail->id)->delete();
        $order_details[] = $request->input('product.product');
        // print_r($order_data['size']);
        // exit();
        $new_array = []; 
        foreach($order_details as $key=>$details)
        { 
            foreach ($details as $key1 => $value1) 
            {
                $qty_array = [];
                $set_qty_array = [];
                if($value1['percentage'] == 0.00)
                {
                    foreach ($order_data['qty'] as $key_q => $value_q) 
                    {
                        $sum_qty = 0;
                        foreach ($value_q as $single_key => $single_value) 
                        {
                         $sum_all_qty_single = $sum_qty += $single_value;
                        }
                        $sum_all_qty[] = $sum_all_qty_single; 
                    }                            
                    $total_price  = $sum_all_qty[$key1] * $value1['price'];
                }                            
                else
                {     
                    foreach ($order_data['qty'] as $key_qt => $value_qt) 
                    {
                        echo "<pre>";
                        print_r($key_qt);
                        $extra_qty_sum = 0;
                        $sum_qty = 0;
                        
                        foreach ($value_qt as $key_qty => $value_qty) 
                        {
                            $min_qty = min($value_qt);        
                            if($value_qty != $min_qty)
                            {
                                $total = $value_qty - $min_qty;
                                $extra_qty_sum = $extra_qty_sum + $total;
                                $extra_qty_sum_array[]=$extra_qty_sum;
                            }
                            $sum_all_qty_single = $sum_qty += $value_qty;
                        }
                        $sum_all_qty[] = $sum_all_qty_single;
                        $extra_tax = ($value1['percentage'] * $value1['price'])/100;
                        $extra_qty_tax = $extra_tax * $extra_qty_sum_array[$key1];
                        $total_price = ($sum_all_qty[$key1] * $value1['price']) + $extra_qty_tax;
                    }
                }  
                $save_details = new OrderProduct();
                $save_details->order_id = $save_detail->id;
                $save_details->product_id = $value1['product_id'];
                $save_details->brand_id = $value1['brand_id'];
                $save_details->category_id = $value1['category_id'];
                $save_details->tax = $value1['tax'];
                $save_details->percentage = $value1['percentage'];
                $save_details->unit_price = $total_price;
                $save_details->save(); 
            // dd('hi');                               
            }      
        }
        $size = [];
                $a = 0;
        foreach ($order_data['qty'] as $key => $value) {
            foreach ($value as $a_key => $a_value) {
                $size[$key][$a_key] = $order_data['size'][$a];
                $a++;
            }
        }
        $order_pro_data = OrderProduct::select('id','brand_id','product_id')->where('order_id',$save_detail->id)->get()->toArray(); 
        foreach ($size as $qty_key => $qty_value) 
        {
            foreach ($qty_value as $q_key => $q_value) {
                $order_size_save = new OrderSize();
                $size = Size::select('id')->where('name',$q_value)->first()->toArray();
                $order_size_save->size_id = $size['id'];
                $order_size_save->qty = $order_data['qty'][$qty_key][$q_key];
                $order_size_save->order_id = $save_detail->id;
                $order_size_save->product_id = $order_pro_data[$qty_key]['product_id'];
                $order_size_save->brand_id = $order_pro_data[$qty_key]['brand_id'];
                $order_size_save->order_product_id = $order_pro_data[$qty_key]['id'];
                $order_size_save->save();
                
            }           
        }
    }
}
