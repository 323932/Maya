<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\User;
use App\Helpers\FileHelp;


class NotificationJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($notification_data)
    {
        $this->notification_data = $notification_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $notification_data = $this->notification_data;
        
        $id= $request->id;

        if(isset($notification_data['id']))
        {
            $id = $notification_data['id'];
        }        
        $save_detail = Notification::firstOrNew(['id' => $id]);
        $save_detail->fill($notification_data);
        $save_detail->action_id = $notification_data['action_id'];
        $old_image = $save_detail['image'];    
        $path = public_path().'/upload/notification/';
      
        if ($request->hasfile('image')) 
        {
            if ($old_image != null) {
                FileHelp::UnlinkImage($path, $old_image);
            }
            $file = $request->file('image');
            $filename = FileHelp::getfilename($file);
            $request->file('image')->move($path, $filename);
            $save_detail->image = $filename;

        }    
        $save_detail->save();
        //dd($save_detail);
        // if($request->save_button == 'save_exit') 
        // {
        //     $delete = User::where('notification_id',$save_detail->id)->delete();
        //     $user_detail = User::select()->get();
        //     foreach ($user_detail as $key => $user) 
        //     {
               
        //         $details = new User();
        //         $details->notification_id = $save_detail->id;
        //         $details->save();
        //     }

        //     return back()->with('message','Record added  Successfully')
        //     ->with('message_type','success');
            
        // }
    }
}