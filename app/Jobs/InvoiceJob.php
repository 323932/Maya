<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Shipment;
use App\Models\ShipmentProduct;
use App\Helpers\FileHelp;

class InvoiceJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($invoice_data)
    {
        $this->invoice_data = $invoice_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $invoice_data = $this->invoice_data;

        $file = $invoice_data['file'];
        $shipment_id = $invoice_data['shipp_number'];
        $filename = '';

        if($invoice_data['file'] != '' && $invoice_data['shipp_number'] != '')
        {    
            $filename = FileHelp::getfilename($file);
            $destinationPath = LOCAL_UPLOAD_PATH.'/invoice';
            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0777);
            }
            $file->move($destinationPath, $filename);

            $shipment = Shipment::find($shipment_id);
            $shipment->invoice = $filename;
            $shipment->save();

            $order_data = Shipment::select('orders.order_no','orders.email as email','orders.shipping_address as shipping_address','orders.payment_type as payment_mod','shipments.shipping_title as transport')->where('shipments.id',$shipment_id)->leftjoin('orders','orders.id','shipments.order_id')->first()->toArray();
            // dd($shipment_id);
            $product_data = ShipmentProduct::select('shipment_products.qty_s as qty_s','shipment_products.qty_m as qty_m','shipment_products.qty_l as qty_l','shipment_products.qty_xl as qty_xl','order_products.unit_price as unit_price','shipment_products.price as price','order_products.brand_id as brand_id','brands.name as brand_name','order_products.product_name as product_name')->where('shipment_products.shipment_id',$shipment_id)->leftjoin('order_products','order_products.id','shipment_products.product_id')->leftjoin('brands','brands.id','order_products.brand_id')->get()->toArray();
            // dd($product_data);
            $brand_arr = [];

            foreach ($product_data as $key => $value) {
                $brand_arr[$value['brand_id'].'_'.$value['brand_name']][] = $value;
            }
        }
        $order_data['brand_arr'] = $brand_arr;
        $order_data['filename'] = LOCAL_IMAGE_PATH.'invoice/'.$filename;

        return ['order_data'=>$order_data,'filename'=>$filename];
    }
}