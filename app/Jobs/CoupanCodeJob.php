<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\CoupanCode;

class CoupanCodeJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($coupan_data)
    {
        $this->coupan_data = $coupan_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $coupan_data = $this->coupan_data;
        $id= $request->id;
        if(isset($coupan_data['id']))
        {
            $id = $coupan_data['id'];  
        }

        $save_detail =CoupanCode::firstOrNew(['id' => $id]);               
        $save_detail->fill($coupan_data);

        if(!empty($save_detail['user_id'])){
            $save_detail->user_id = implode(',',$save_detail['user_id']);
        }

        // if($coupan_data['discount']=="Flat")
        // {
        //     $save_detail->percentage=null;
        // }
        //dd($save_detail);
        $save_detail->save(); 
    }
}
