<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use App\Models\Color;

class ColorJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($color_data)
    {
         $this->color_data = $color_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Request $request)
    {
        $color_data = $this->color_data;
        $id= $request->id;
        if(isset($color_data['id']))
        {
            $id = $color_data['id'];
        }        
        $save_detail = Color::firstOrNew(['id' => $id]);
        $save_detail->fill($color_data);
        //dd($save_detail);
        $save_detail->save();
    }
}
