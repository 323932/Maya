<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       switch ($this->method()) {

            case 'POST':
            {
                return [
                'name' => 'required',
                'size_id' => 'required',
                'hex_code' => 'required',
                'image' => 'required|mimes:jpeg,png',
                'logo_image' =>'required|mimes:jpeg,png',
                'title_color' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'name' => 'required',
                'size_id' => 'required',
                'hex_code' => 'required',
                'image' => 'nullable|mimes:jpeg,png',
                'logo_image' =>'nullable|mimes:jpeg,png',
                'title_color' => 'required',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'name.required' => 'The name field is required.',
            'size_id.required' => 'The size field is required.',
            'image.required' => 'The image is required.',
            'image.mimes' => 'The image must be a file of type:jpeg,png.',
            'logo_image.required' => 'The logo is required.',
            'logo_image.mimes' => 'The logo must be a file of type:jpeg,png.',
            'hex_code' => 'The hexa code field is required',
            'title_color' => 'Te title color field is required',
        ];
    }
}
