<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SizeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'name' => 'required|unique:sizes,name',
                ]; 
            }
            case 'PATCH':
            {
                $id = \Request::segment(3);
                
                return [
                'name' => 'required|unique:sizes,name,'. $id . ',id',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'name.required' => 'The name field is required.',
        ];
    }
}
