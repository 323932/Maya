<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoupanCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                    'user_id' => 'required',
                    'code' => 'required',
                    'discount' => 'required',
                    'time' => 'required',
                    'percentage' => 'required|numeric|between:0,99.99',
                    'to_date' => 'required',
                    'from_date' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                    'user_id' => 'required',
                    'code' => 'required',
                    'discount' => 'required',
                    'time' => 'required',
                    'percentage' => 'required|numeric|between:0,99.99',
                    'to_date' => 'required',
                    'from_date' => 'required',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'user_id.required' => 'The user field is required.',
            'code.required' => 'The coupan code field is select.',
            'discount.required' => 'The discount field is required.',
            'time.required' => 'The coupon code time field is required.',
        ];
    }
}
