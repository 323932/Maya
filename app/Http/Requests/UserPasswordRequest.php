<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email',
            'password'=>'required',
            'password_confirmation'=>'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'email.required'=>'The email field is required.',
            'password.required'=>'The password field is required.',
            'password_confirmation.required'=>'The confirm password field is required.',
            'password_confirmation.same'=>'The confirm password is dismatched.',
        ];
    }
}
