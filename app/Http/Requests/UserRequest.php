<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required',
            'user_plan_id'=>'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required'=>'The user type field is required.',
            'user_plan_id.required'=>'The user plan field is required.',
        ];
    }
}
