<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get  validation rules that apply to  request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $input = $request->all();

        $rules= [
            'user_id' => 'required',
            'payment_type' => 'required', 
            'shipping_address' => 'required',
            'shipping_area' => 'required',
            'shipping_state' => 'required',
            'shipping_city' => 'required',
            'shipping_pincode' => 'required',
            'qty.*.*' => 'required|numeric',
        ]; 
        foreach(Input::get('product.product') as $key => $val)
        {
            $rules['product.product.'.$key.'.brand_id'] = 'required';
            $rules['product.product.'.$key.'.product_id'] = 'required';
            $rules['product.product.'.$key.'.category_id'] = 'required';
            $rules['product.product.'.$key.'.color_id'] = 'required';
            
        }
        // foreach ($input['q_id'] as $q_key => $q_value) {
        //     // print_r($input['qty_'.$q_key]);
        //     if(isset($input['qty_'.$q_key])){
        //         foreach ($input['qty_'.$q_key] as $qty_key => $qty_value) {
        //             // dd($qty_key);
        //             if($qty_value == null){
        //                 $rules['qty_'.$q_key] = 'required';
        //             }
        //             # code...
        //         }
        //     }
        // }
        return $rules;
    }
    public function messages()
    {
        $messages = [
            'user_id.required' => 'Dealer is required',
            'payment_type.required' => 'Payment type is required',
            'tax.required' => 'Tax is required',  
            'shipping_address.required'=> 'Address is required',
            'shipping_area.required'=> 'Area is required',
            'shipping_state.required'=> 'State is required',
            'shipping_city.required'=> 'City is required',
            'shipping_pincode.required'=> 'Pincode is required',
            'qty.*.*.required' => 'Required',
            'qty.*.*.numeric' => 'Invalid Qty.',
        ];
        foreach(Input::get('product.product') as $key => $val)
        {
            $messages['product.product.'.$key.'.brand_id.required'] = ' brand is required';
            $messages['product.product.'.$key.'.category_id.required'] = ' category is required';
            $messages['product.product.'.$key.'.product_id.required'] = ' product is required';
            $messages['product.product.'.$key.'.color_id.required'] = ' color is required';            
        }
        return $messages;
    }
}
