<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesPersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                    'firstname' => 'required',
                    'lastname' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                    // 'firstname' => 'required',
                    // 'lastname' => 'required',
                    // 'email' => 'required|email|unique:users,email',
                    // 'password' => 'required',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'firstname.required' => 'The firstname field is required.',
            'lastname.required' => 'The lastname field is required.',
            'email.required' => 'The email field is required.',
            'email.email' => 'Email is valid format',
            'password.required' => 'The password field is required.',
        ];
    }
}
