<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $input = $request->all();
        $percentage = $input['without_set'];
        $plans = $input['plans'];
            $id = \Request::segment(4);
                $rules = [
                    'name' => 'required',
                    'sku' => 'required|unique:products,sku,' . $id,
                    'category_id' =>  'required', 
                    'collection' => 'required',           
                    'brand_id' => 'required',
                    'original_price' => 'required',
                    'tax' => 'required',    
                    'percentage' => 'required_if:without_set,==,0',
                    'is_visible' => 'required_if:plans,==,1'
                ];
                if($percentage == 0){
                    $rules['percentage'] = 'numeric|between:0,99.99|gt:0';
                }
                // dd(Input::get('product.product'));
                if(Input::get('product.product') != '')
                {
                    //dd('1');
                    foreach(Input::get('product.product') as $key => $val)
                    {
                        $rules['product.product.'.$key.'.color_id'] = 'required';
                        foreach ($val as $pro_key => $pro_value) {
                            if($pro_key != 'color_id'){
                                if($pro_value == null){
                                 $rules['product.product.'.$pro_key.'.'.$key.'.qty'] = 'required';    
                                }
                            }
                        }
                    }
                }
                // dd($rules);
                foreach(Input::get('attribute.attribute') as $key => $val)
                {
                    $rules['attribute.attribute.'.$key.'.product_key'] = 'required';
                    $rules['attribute.attribute.'.$key.'.product_value'] = 'required';
                }
                foreach(Input::get('information.information') as $key => $val)
                {
                    $rules['information.information.'.$key.'.information_key'] = 'required';
                    $rules['information.information.'.$key.'.information_value'] = 'required';
                }
                // if($input['plans'] == 0)
                // {
                //     'is_visible' => 'required',
                // }
                return $rules;
    }
    public function messages()
    {
        $messages = [
            'name.required' => 'The name field is required.',
            'sku.required' => 'The SKU field is required.',            
            'brand_id.required' => 'The brand field is required.',
            'collection.required' => 'The collection field is required.',
            'category_id.required' => 'The category field is required.',
            'color_id.required' => 'The color field is required.',
            'original_price.required' => 'The price field is required.',
            'tax' => 'The tax field is required.',
            'size.required' => 'The quantity of small size field is required.',
            'qty_m.required' => 'The quantity of medium size field is required.',
            'qty_l.required' => 'The quantity of large size field is required.',
            'qty_xl.required' => 'The quantity of extra large size field is required.',
            'is_visible.required_if' => 'The select user plans.',
        ];
        if(Input::get('without_set') == 0)
            {
                $messages['percentage.gt'] = 'Percentage greater then 0';
            }

        if(Input::get('product.product') != ''){
            foreach(Input::get('product.product') as $key => $val)
            {
                $messages['product.product.'.$key.'.color_id.required'] = 'The color field is required.';
                foreach ($val as $pro_key => $pro_value) {
                    if($pro_key != 'color_id'){
                        if($pro_value == null){
                            $messages['product.product.'.$pro_key.'.'.$key.'.qty.required'] = 'The qty '.$pro_key.' field is required.';
                        }
                    }
                }
            }
        }

        foreach(Input::get('attribute.attribute') as $key => $val)
        {
            $messages['attribute.attribute.'.$key.'.product_key.required'] = 'The Product key field is required.';
            $messages['attribute.attribute.'.$key.'.product_value.required'] = 'The Product value field is required.';
        }
        foreach(Input::get('information.information') as $key => $val)
        {
            $messages['information.information.'.$key.'.information_key.required'] = 'The Information key field is required.';
            $messages['information.information.'.$key.'.information_value.required'] = 'The Information value field is required.';
        }
        return $messages;
    }
}
