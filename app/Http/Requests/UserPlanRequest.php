<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $input = $request->all();
        $plans = $input['product_plans'];
         //print_r($plans);
        switch ($this->method()) {

            case 'POST':
            {
                $rules =[
                'user_plan' => 'required',
                'brand_id' => 'required',
                'category_id'=> 'required',
                'discount' => 'required|numeric|between:0,99.99',             
                ]; 

                if($plans == 1){
                    // dd('hi');
                    $rules['is_visible'] = 'required';
                }
                return $rules;
            }
            case 'PATCH':
            {
                $rules = [
                'user_plan' => 'required',
                'brand_id' => 'required',
                'category_id'=> 'required',
                'discount' => 'required|numeric|between:0,99.99',

                ]; 

                if($plans == 1){
                    $rules['is_visible'] = 'required';
                }
                return $rules;
            }
        }
    }
    public function messages()
    {
        return[
            'user_plan.required' => 'The name field is required.',
            'brand_id.required' => 'The brand field is required.',
            'category_id.required' => 'The category field is required.',
            'discount.required' => 'The discount field is required.',
            'is_visible.required' => 'The select products',
        ];
    }
}
