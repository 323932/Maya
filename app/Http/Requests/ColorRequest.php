<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'name' => 'required|unique:colors,name',
                'hexa_code' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                $id = \Request::segment(3);
                
                return [
                'name' => 'required|unique:colors,name,'. $id . ',id',
                'hexa_code' => 'required',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'name.required' => 'The name field is required.',
            'hex_code.required' => 'The hex code field is required.',
        ];
    }
}
