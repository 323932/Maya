<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'name' => 'required',
                'brand_id' => 'required',
                'image' => 'required|mimes:jpeg,png',
                'is_active' => 'required',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'name' => 'required',
                'brand_id' => 'required',
                'image' => 'nullable|mimes:jpeg,png',
                'is_active' => 'required',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'name.required' => 'The name field is required.',
            'brand_id.required' => 'The brand field is select.',
            'image.required' => 'The image is required.',
            'image.mimes' => 'The image must be a file of type:jpeg,png.',
            'is_active.required' => 'The status is required.',
        ];
    }
}
