<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $data = $request->all();

        switch ($this->method()) {

            case 'POST':
            {
                if($data['is_shipment'] == 1){
                    return [
                    'shipping_title' => 'required',
                    'shipping_number' => 'required',
                    'charge' =>'required',
                    ];
                }else{
                    return [];
                }
            }
            case 'PATCH':
            {
                if($data['is_shipment'] == 1){
                    return [
                    'shipping_title' => 'required',
                    'shipping_number' => 'required',
                    'charge' =>'required',
                    ]; 
                }else{
                    return [];
                }   
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'shipping_title.required' => 'The Shipment title field is required.',
            'shipping_number.required' => 'The Shipment Number field is required.',
            'charge.required' => 'The Charge field is required.',
            'mobile.required' => 'The mobile field is required.',
        ];
    }
}
