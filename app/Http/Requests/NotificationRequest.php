<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {

            case 'POST':
            {
                return [
                'title' => 'required',
                'type' => 'required',
                'action_id' => 'required',
                'status' => 'required',
                'image' => 'required|mimes:jpg,png,jpeg,gif,bmp',
                ]; 
            }
            case 'PATCH':
            {
                return [
                'title' => 'required',
                'type' => 'required',
                'action_id' => 'required',
                'status' => 'required',
                'image' => 'nullable|mimes:jpg,png,jpeg,gif,bmp',
                ]; 
            }
            default : break;
        }
    }
    public function messages()
    {
        return[
            'title.required' => 'The title field is required.',
            'type.required' => 'The type field is select.',
            'action_id.required' => 'The name field is required.',
            'status.required' => 'The status field is required.',
            'image.required' => 'The banner is required.',
        ];
    }
}
