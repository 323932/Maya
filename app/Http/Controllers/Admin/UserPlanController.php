<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\UserPlan;
use App\Models\User;
use App\Models\Brand;
use App\Models\Category;
use Form;
use App\Models\PlanProductDetail;
use App\Events\UserPlanEvent;
use App\Http\Requests\UserPlanRequest;
use Event;

class UserPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (user_plans.user_plan like \"%{$search}%\""
                . " or brands.name like \"%{$search}%\""
                . " or categories.name like \"%{$search}%\""
                . " or user_plans.discount like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['user_plans.id','user_plans.updated_at','user_plans.user_plan','categories.name as category','brands.name as brand','user_plans.discount'];

            $companymaster_columns_count = UserPlan::select($columns)
            ->leftjoin('brands','brands.id','=','user_plans.brand_id')
            ->leftjoin('categories','categories.id','=','user_plans.category_id')
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = UserPlan::select($columns)
            ->leftjoin('brands','brands.id','=','user_plans.brand_id')
            ->leftjoin('categories','categories.id','=','user_plans.category_id')
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.user-plan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = (old('brand_id')) ? [""=>"Select Category"] + Category::where("brand_id",old('brand_id'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select Category");
        $product_list = ['' => 'Select Product'] + Product::pluck('name','id')->toArray();
        $product_flag=[];
        if(count($product_list) > 1)
        {
            $product_flag = 1;
        }
        else
        {
          $product_flag = 0;  
        }
        //dd($product_flag);
        if($request->ajax()){
            if(isset($request->brand_id) || isset($request->category_id)){
                $product_list = ['' => ''] + Product::where('category_id',$request->category_id)
                    ->where('brand_id',$request->brand_id)
                    ->pluck('name','id')
                    ->toArray();    
                //dd($product_list);
                return $product_list; 
            } 
        }
        else{
           return view('admin.user-plan.create',compact('brand','category','product_list','product_flag'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserPlanRequest $request)
    {
        $userPlan_data = $request->all();
        //dd($userPlan_data);
        $userPlan_data['user_plan'] = strip_tags($request->user_plan); 
        Event::fire(new UserPlanEvent($userPlan_data));

        //  dd($request->all());
        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('user-plan.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $userPlan_data = UserPlan::find($id);
       
        $id = $userPlan_data['id'];
        // dd($userPlan_data);
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = (old('brand_id')) ? [""=>"Select Category"] + Category::where("brand_id",old('brand_id'))->pluck("name","id")->toArray() :["" =>"Select Category"] + Category::where('brand_id',$userPlan_data['brand_id'])->pluck('name', 'id')->toArray();

        $products_ar = Product::where('category_id',$userPlan_data['category_id'])->get()->toArray();
        $products = [];
        foreach ($products_ar as $key => $value) {
            $products[$value['id']] = $value['name'].'_'.$value['sku'];
        }
        // dd($products);
        $plan_details = PlanProductDetail::select('product_id')->where('user_plan_id',$id)->get()->toArray();
        $old_product=[];
        foreach ($plan_details as $key => $value) {
            $old_product[$value['product_id']] = $value['product_id'];
        }
        $product_list = ['' => 'Select Product'] + Product::pluck('name','id')->toArray();
        // dd($product_list);
        $product_flag=[];
        if(count($product_list) > 1)
        {
            $product_flag = 1;
        }
        else
        {
          $product_flag = 0;  
        }
        // dd($category);
        return view('admin.user-plan.edit',compact('brand','category','products','userPlan_data','id','old_product','product_flag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserPlanRequest $request, $id)
    {
        $userPlan_data = $request->all();
        //dd($userPlan_data);
        $userPlan_data['user_plan'] = strip_tags($request->user_plan);
        $userPlan_data['id'] = $id;
        Event::fire(new UserPlanEvent($userPlan_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('user-plan.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        if(!empty($request->id))
        {
            $id = $request->id;
           //dd($id);
            if(count($id)>1)
            {
                foreach($id as $key=>$value)
                {
                    $multi_id = User::where('user_plan_id',$value)->update(['user_plan_id' => null]);
                    $plan_id = PlanProductDetail::where('user_plan_id',$value)->delete();
                    UserPlan::destroy($id);
                }
            }
            else
            {
                $multi_id = User::where('user_plan_id',$id)->update(['user_plan_id' => null]);
                $user_plan_id = PlanProductDetail::where('user_plan_id',$id)->delete();
                    UserPlan::destroy($id);
            }    
            return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
        }
    }

    public function getCategory(Request $request)
    {
        $brand_id = $request->get('brand_id');        

        $get_categoryIn = Category::where('brand_id', $brand_id)->pluck('name', 'id')->toArray();
        $category_list = ['' => 'Select Category'] + $get_categoryIn;
        //dd($category);

        return Form::select('category_id', $category_list, array('id' => 'category_id'));
    }

    public function getProduct(Request $request)
    {
        // dd('hi');
        $product_list = ['' => ''] + Product::pluck('name','id')->toArray();
        // dd($product_list);
        
        // if($request->ajax()){
            if(isset($request->brand_id) || isset($request->category_id)){
                // dd($request->brand_id);
                $product_list_arr = Product::where('category_id',$request->category_id)
                    ->where('brand_id',$request->brand_id)
                    ->get()->toArray();   
                if(!empty($product_list_arr)){
                    $product_list = [];    
                    foreach ($product_list_arr as $key => $value) {
                        $product_list[$value['id']] = $value['name']."_".$value['sku'];
                    }
                    // dd($product_list);
                    $res_html = view('admin.user-plan.product_chk',['products'=>$product_list])->render();
                }else{
                    $res_html = '';
                }

                return $res_html; 
            } 
    }
}
