<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\PlanProductDetail;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\BrandSize;
use App\Models\ProductSize;
use App\Models\UserPlan;
use App\Models\ProductVariant;
use App\Http\Requests\ProductRequest;
use Event;
use Form,Excel;
use App\Events\ProductEvent;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (products.name like \"%{$search}%\""
                . " or products.sku like \"%{$search}%\""
                . " or brands.name like \"%{$search}%\""
                . " or categories.name like \"%{$search}%\""
                . " or products.original_price like \"%{$search}%\""
                . " or products.tax like \"%{$search}%\""
                . ")";
            }  

            if($request->has('todate') && ($request->has('fromdate'))){
                $todate = $request->get('todate');
                $fromdate = $request->get('fromdate');
                $where_str .= " and (DATE(products.created_at) >= '$fromdate')";
                $where_str .= " and (DATE(products.created_at) <= '$todate')";
            }                                          

            $columns = ['products.created_at','products.id','products.name','products.sku','categories.name as category','brands.name as brand','products.original_price','products.tax'];

            $companymaster_columns_count = Product::select($columns)
            ->leftjoin('brands','brands.id','=','products.brand_id')
            ->leftjoin('categories','categories.id','=','products.category_id')
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Product::select($columns)
            ->leftjoin('brands','brands.id','=','products.brand_id')
            ->leftjoin('categories','categories.id','=','products.category_id')
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $color = Color::orderBy('name','asc')->pluck('name','id')->toArray();
        $user_plans = UserPlan::pluck('user_plan', 'id')->toArray();
        $user_flag=[];
        if(!empty($user_plans))
        {
            $user_flag = 1;
        }
        else
        {
          $user_flag = 0;  
        }

        $category = (old('brand_id')) ? [""=>"Select Category"] + Category::where("brand_id",old('brand_id'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select Category");

        return view('admin.product.create',compact('brand','category','color','user_plans','user_flag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product_data = $request->all();
        //print_r($product_data);

        $product_data['name'] = strip_tags($request->name);        
        $product_data['sku'] = strip_tags($request->sku);
        Event::fire(new ProductEvent($product_data));
        
        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('products.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_data = Product::find($id);
        $id = $product_data['id'];

        $product_detail = json_decode($product_data['attribute'],true);
        $information_detail = json_decode($product_data['information'],true);
        $product_key_value = [];

        foreach ($product_detail as $key => $single_product_data) {

            foreach ($single_product_data as $single_key => $single_value) {

                $product_single['product_key'] = $single_value['product_key'];
                $product_single['product_value'] = $single_value['product_value'];


                $product_key_value[] = $product_single;
            }

        }

        $information_key_value = [];

        foreach ($information_detail as $key => $single_information_data) {

            foreach ($single_information_data as $single_key => $single_value) {

                $information_single['information_key'] = $single_value['information_key'];
                $information_single['information_value'] = $single_value['information_value'];


                $information_key_value[] = $information_single;
            }

        }
        $product_variant_detail = ProductSize::select('product_sizes.qty','product_variants.color_id','product_sizes.size_id','sizes.name as name')
        ->leftjoin('product_variants','product_variants.id','=','product_sizes.product_variant_id')
        ->leftjoin('sizes','sizes.id','=','product_sizes.size_id')->where('product_variants.product_id',$id)->get()->toArray();
        
            $array = array_unique(array_column($product_variant_detail, 'color_id'));
            $unique_data = array_intersect_key($product_variant_detail, $array);

        $colorArry   = [];
       
        foreach ($product_variant_detail as $key => $value) {

            if(array_key_exists($value['color_id'], $colorArry))
            {
                $colorArry[$value['color_id']][$value['name']] = $value['qty'];
            }
            else
            {
                $colorArry[$value['color_id']][$value['name']] = $value['qty'];
            }
        }
         
        $colorArr = [];
        foreach($colorArry as $key => $value)
        {
            foreach ($value as $subKey => $subValue) {
                $colorArr[$key][$subKey] = $subValue;
                $colorArr[$key]['color_id'] = $key;
            }
        }

        $final_product_variant_arr = array_values($colorArr);
        
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $color = Color::orderBy('name','asc')->pluck('name','id')->toArray();

        $gallary_images = ProductImage::where('product_id', $id)->get()->toArray();
        
        foreach ($gallary_images as $key => $single_image) {

            $img_name = explode('/',$single_image['image'])[5];
            $image[$key]['caption'] = $img_name;
            $image[$key]['name'] = $single_image['image'];
            $image[$key]['key'] = $single_image['id'];
            $image[$key]['url'] = route('delete.image');
            $image[$key]['extra']['_token'] = csrf_token();
            $image[$key]['extra']['image_name'] = $img_name;
        }
        $multi_images = json_encode($image);
        $image_data = '';
        if(!empty($image))
        {
            $image_data = stripcslashes(json_encode($image));
        }
        
        $image = json_decode($image_data,true);
        $images = [];
        foreach ($image as $key => $value) {
            $images[] = $value['name'];
        }
        $images = json_encode($images);
        
        $user_plans = UserPlan::pluck('user_plan', 'id')->toArray();
        $user_flag=[];
        if(!empty($user_plans))
        {
            $user_flag = 1;
        }
        else
        {
          $user_flag = 0;  
        }
        $product_details = PlanProductDetail::select('user_plan_id')->where('product_id',$id)->get()->toArray();
        $old_plan = [];
        foreach ($product_details as $key => $value) {
            $old_plan[$value['user_plan_id']] = $value['user_plan_id'];
        }

        $category = (old('brand_id')) ? [""=>"Select Category"] + Category::where("brand_id",old('brand_id'))->pluck("name","id")->toArray() :["" =>"Select Category"] + Category::where('brand_id',$product_data['brand_id'])->pluck('name', 'id')->toArray();
        $data = BrandSize::where('brand_id',$product_data['brand_id'])->leftjoin('sizes','sizes.id','=','brand_sizes.size_id')->where('sizes.is_active','1')->get()->toArray();
        
        return view('admin.product.edit',compact('brand','category','color','product_data','id','images','product_key_value','information_key_value','old_plan','user_plans','product_variant_detail','multi_images','user_flag','data','unique_data','final_product_variant_arr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product_data = $request->all();
        //dd($product_data);
        $product_data['name'] = strip_tags($request->name);        
        $product_data['sku'] = strip_tags($request->sku);
        $product_data['id'] = $id;
        Event::fire(new ProductEvent($product_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('products.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        //dd('hi');
        if(!empty($request->id))
        {
            $id = $request->id;
            if(count($id)>1)
            {
                foreach($id as $key=>$value)
                {
            //dd($value);
                    $image = ProductImage::where('product_id',$value)->pluck('image');
                    foreach($image as $key1=>$v)
                    {
                        $i = $image[$key1];

                        $path = LOCAL_IMAGE_PATH."products/".$i;
                        if(file_exists($path))
                        {
                            unlink($path);
                        }
                    }

                    $multi_id = ProductImage::where('product_id',$value)->delete();
                    $product_id = PlanProductDetail::where('product_id',$value)->update(['product_id' => null]);
                    $variant_id = ProductVariant::where('product_id',$value)->delete();
                    $size_id = ProductSize::where('product_id',$value)->delete();
                    Product::destroy($value);
                }
            }
            else
            {
                //dd('by');
                $image = ProductImage::where('product_id',$id)->pluck('image');
                    foreach($image as $key=>$v)
                    {
                        $i = $image[$key];

                        $path = LOCAL_IMAGE_PATH."products/".$i;
                        if(file_exists($path))
                        {
                            unlink($path);
                        }
                    }

                    $multi_id = ProductImage::where('product_id',$id)->delete();
                    $product_id = PlanProductDetail::where('product_id',$id)->update(['product_id' => null]);
                    $variant_id = ProductVariant::where('product_id',$id)->delete();
                    $size_id = ProductSize::where('product_id',$id)->delete();
                    Product::destroy($id);
            }    
            return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
        }
    }

    public function getCategory(Request $request)
    {
        $brand_id = $request->get('brand_id');        

        $get_categoryIn = Category::where('brand_id', $brand_id)->pluck('name', 'id')->toArray();
        $category_list = ['' => 'Select Category'] + $get_categoryIn;

        return Form::select('category_id', $category_list, array('id' => 'category_id'));
    }

    public function getSize(Request $request)
    {
        $brand_id = $request->get('brand_id');        
        //dd($brand_id);

        $get_sizeIn = BrandSize::leftjoin('sizes','sizes.id','=','brand_sizes.size_id')->where('brand_id',$brand_id)->where('is_active','1')->get()->toArray();
        $count = BrandSize::where('brand_id',$brand_id)->leftjoin('sizes','sizes.id','=','brand_sizes.size_id')->where('is_active','1')->count();
        // print_r($count);exit();
        $color = Color::orderBy('name','asc')->pluck('name','id')->toArray();

        $result = view('admin.product.product_variant', ['data' => $get_sizeIn,'color'=>$color,'count'=>$count])->render();        
        return $result;
    }

    public function deleteImage(Request $request)
    {
        $image_data = $request->all();
        $image = $image_data['image_name'];

        $image_unlink_path = public_path()."/upload/products/".$image;
        
        if(file_exists($image_unlink_path))
        {
            $unlink_old_image = unlink($image_unlink_path);
        }

        $image_id = ProductImage::where('id', $image_data['key'])->delete(); 
        return response()->json(array('success' => true), 200);
    }

    public function UploadImage()
    {
        dd('test');
    }

    public function CollectionSuggetion(Request $request)
    {
        if($request->collection != null){
            $collection_all = Product::select('collection')->where('collection','like','%'.$request->collection.'%')->get()->toArray();
            //dd($collection_all);
                $append_value = '';
                foreach($collection_all as $data){
                    $collection = '"'.$data['collection'].'"';
                    $append_value .= "<li onClick='selectCollection(".$collection.")'>".$data['collection']."</li>";  
                }
                $return_data = "<ul id='shipping-list'>".$append_value."</ul>";
                return $return_data;
        }
    }

    public function CollectionSuggetionEdit(Request $request,$id)
    {
        if($request->collection != null){
            $collection_all = Product::select('collection')->where('collection','like','%'.$request->collection.'%')->get()->toArray();
            //dd($collection_all);
                $append_value = '';
                foreach($collection_all as $data){
                    $collection = '"'.$data['collection'].'"';
                    $append_value .= "<li onClick='selectCollection(".$collection.")'>".$data['collection']."</li>";  
                }
                $return_data = "<ul id='shipping-list'>".$append_value."</ul>";
                return $return_data;
        }
    }
}