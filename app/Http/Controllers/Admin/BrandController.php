<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Size;
use App\Models\BrandSize;
use Event;
use App\Events\BrandEvent;
use App\Http\Requests\BrandRequest;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (brands.name like \"%{$search}%\""
                . " or brands.hex_code like \"%{$search}%\""
                . " or brands.title_color like \"%{$search}%\""
                . " or brands.is_active like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['brands.updated_at','brands.name','brands.hex_code','brands.title_color','brands.is_active','brands.description','brands.id'];

            $companymaster_columns_count = Brand::select($columns)
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Brand::select($columns)
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.brand.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $size = Size::orderBy('name','asc')->where('is_active','1')->pluck('name','id')->toArray();
        return view('admin.brand.create',compact('size'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request)
    {
        $brand_data = $request->all();
        //dd($category_data);
        $brand_data['name'] = strip_tags($request->name);
        Event::fire(new BrandEvent($brand_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('brands.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand_data = Brand::find($id);
        $id = $brand_data['id'];

        $brand_size_details = BrandSize::select('size_id')->where('brand_id',$id)->get()->toArray();
        // dd($brand_size_details);
        $size_id = [];
        foreach ($brand_size_details as $key => $value) {
            $size_id[] = $value['size_id'];
        }
        // dd($size_id);
        $size = Size::orderBy('name','asc')->where('is_active','1')->pluck('name','id')->toArray();
        // dd($size);
        return view('admin.brand.edit',compact('brand_data','id','size','size_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, $id)
    {
        $brand_data = $request->all();
        //dd($brand_data);
        $brand_data['name'] = strip_tags($request->name);
        $brand_data['id'] = $id;
        Event::fire(new BrandEvent($brand_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('brands.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        // $brand_id = $request->get('id');

        // if(is_array($brand_id)){
        //     foreach ($brand_id as $key => $value) {
        //         Brand::where('id', $value)->delete();
        //     }
        // }
        // else{
        //     Brand::where('id', $brand_id)->delete();
        // }    
        // return back()->with('message', 'Record deleted Successfully.')
        // ->with('message_type', 'success');

        if(!empty($request->id))
        {
            $id = $request->id;
            if(count($id)>1)
            {
                foreach($id as $key=>$value)
                {
                    $size_id = BrandSize::where('brand_id',$value)->delete();
                    Brand::destroy($value);
                }
            }
            else
            {
                $multi_id = BrandSize::where('brand_id',$id)->delete();
                Brand::destroy($id);
            }    
            return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
        }
    }

    public function BrandOrder(){
        $brand_data = Brand::select('id','name','image','logo_image')->orderby('order_position')->get();
        //dd($category_data);
        return view('admin.brand.order_image',compact('brand_data'));
    }
    public function Order(Request $request){
        $order = $request->get('order');
        foreach ($order as $key => $value) {

            $index = $key + 1;

            Brand::where('id', $value)->update(array('order_position' => $index));
        }
    }
    
}
