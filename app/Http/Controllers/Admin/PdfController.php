<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
class PdfController extends Controller
{
    public function downloadpdf()
    {
    	$pdf = PDF::loadview('admin.pdf.pdf');
    	// $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/tmp']);
    	// // dd($mpdf);
     //    $mpdf->autoLangToFont = true;
     //    $mpdf->autoScriptToLang = true;
        
     //    $mpdf->WriteHTML(\View::make('admin.pdf.pdf'));

     //    $filename = 'abc.pdf';
     //    $filepath = public_path() ."/documents/" . $filename;
     //    // dd($filepath);
     //    $mpdf->output($filepath);
        return $pdf->download('abc.pdf');
    }
}
