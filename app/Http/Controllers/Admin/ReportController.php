<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\User;
use App\Models\SalesPerson;
use App\Models\State;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\BrandSize;
use App\Models\OrderSize;
use Excel;

class ReportController extends Controller
{
    public function index(Request $request){
        $order_products_data = OrderProduct::select('orders.order_no','products.name as product_name','categories.name as category_name','brands.name as brand_name')
                            ->leftjoin('orders','orders.id','=','order_products.order_id')
                            ->leftjoin('products','products.id','=','order_products.product_id')
                            ->leftjoin('brands','brands.id','=','order_products.brand_id')
                            ->leftjoin('categories','categories.id','=','order_products.category_id')
                            ->get();
        $product_data = [''=>'Select Product Wise'] + Product::pluck('name','id')
                                                        ->toArray();
        $dealer_data = [''=>'Select Dealer Wise'] + User::pluck('firstname','id')
                                                        ->toArray();
        $sales_person_data = [''=>'Select Sales Person Wise'] + SalesPerson::pluck('fullname','id')
                                                        ->toArray();
        $state_data = [''=>'Select State Wise'] + State::pluck('title','title')
                                                        ->toArray();
        $brand_data = Brand::pluck('name','id')
                                                        ->toArray();
        $category_data = [''=>'Select Category Wise'] + Category::pluck('name','id')
                                                 ->toArray();       

       
        if($request->ajax()){
            // dd('hi');
            $where_str    = "1 = ?";
            $where_params = array(1);
            $type_value = $request->type_value;
            $brand_value = $request->brand_value;
            $attr = $request->attr;
             // dd($attr);
            if($attr == 'product'){
                $where_str = " order_products.product_id = '$type_value'";
            }
            if($attr == 'dealer'){
                $where_str = " orders.user_id = '$type_value'";
            }
            if($attr == 'sale'){
                $where_str = " orders.seller_id = '$type_value'";
            }
            if($attr == 'state'){
                $where_str = " orders.billing_state = '$type_value'";
            }
            if($attr == 'category'){
                $where_str = " order_products.category_id = '$type_value'";
            }
            if($attr == 'price'){
                if($type_value != 'show-all'){
                    $sepeate_price_value = explode('-',$type_value);
                    $where_str .= " and (orders.order_price >= '$sepeate_price_value[0]')";
                    $where_str .= " and (orders.order_price <= '$sepeate_price_value[1]')";
                }
            }
            $where_str .= " and order_products.brand_id = '$brand_value'";
            if($request->todate && $request->fromdate){
                $tod = strtotime($request->todate);
                $todate = date('Y-d-m',$tod);
                $fromd = strtotime($request->fromdate);
                $fromdate = date('Y-d-m',$fromd);
                $where_str .= " and (DATE(order_products.created_at) >= '$fromdate')";
                $where_str .= " and (DATE(order_products.created_at) <= '$todate')";
            }
            if($attr == null){
                $order_products_data = OrderProduct::select('orders.order_no','products.name as product_name','categories.name as category_name','brands.name as brand_name')
                                            ->leftjoin('orders','orders.id','=','order_products.order_id')
                                            ->leftjoin('products','products.id','=','order_products.product_id')
                                            ->leftjoin('brands','brands.id','=','order_products.brand_id')
                                            ->leftjoin('categories','categories.id','=','order_products.category_id')
                                            ->whereDate('order_products.created_at', '>=',$fromdate)
                                            ->whereDate('order_products.created_at', '<=',$todate)
                                            ->get();
            }

            $product_data = OrderProduct::select('products.sku','orders.order_price','users.firstname','orders.billing_state','categories.name','orders.id','orders.order_no')
                                        ->leftjoin('products','order_products.product_id','=','products.id')
                                        ->leftjoin('orders','orders.id','=','order_products.order_id')
                                        ->leftjoin('users','users.id','=','orders.user_id')
                                        ->leftjoin('categories','categories.id','=','order_products.category_id')
                                        ->leftjoin('brands','brands.id','=','order_products.brand_id')
                                        ->whereRaw($where_str, $where_params)
                                        ->get();
// dd($product_data);
            $qty_name = [];
            foreach ($product_data as $key => $value) {
                // dd($value['id']);
                $qty_name = OrderSize::select('order_sizes.qty','sizes.name')->leftjoin('sizes','sizes.id','=','order_sizes.size_id')->where('order_sizes.order_id',$value['id'])->get()->toArray();
            }
                                        // print_r($product_data[0]['sku']);exit();
            if($attr == 'product' || $attr == 'dealer' || $attr == 'sale' || $attr == 'state' || $attr == 'brand' || $attr == 'category' || $attr == 'price'){
                $res_html = view('admin.report.product',['product_data'=>$product_data,'attr'=>$attr,'qty_name'=>$qty_name])->render();
                return response()->json(['success' => true,'response_html' => $res_html], 200);
            }
            if($attr == null){
                $res_html = view('admin.report.product',['order_products_data'=>$order_products_data,'attr'=>$attr,'qty_name'=>$qty_name])->render();
                return response()->json(['success' => true,'response_html' => $res_html], 200);
            }
        }
        else{
            return view('admin.report.index',compact('order_products_data','product_data','dealer_data','dealer_data','sales_person_data','state_data','brand_data','category_data'));
        } 
    }
    public function store(Request $request){
        // dd($request->all());
        $where_str    = "1 = ?";
        $where_params = array(1);
        $brand_value = $request->brand_data;
        // dd($brand_value);
        if($brand_value != null){
            // dd('hu');
            $attr = 'brand';
            $where_str .= " and order_products.brand_id = '$brand_value'";
        }
        if($request->product_type != null){
            $attr = 'product';
            $where_str = " order_products.product_id = '$request->product_type'";
        }
        if($request->dealer_type != null){
            $attr = 'dealer';
            $where_str = " orders.user_id = '$request->dealer_type'";
        }
        if($request->sales_person_data != null){
            $attr = 'sale';
            $where_str = " orders.seller_id = '$request->sales_person_data'";
        }
        if($request->state_data != null){
            // dd(1);
            $attr = 'state';
            // dd($attr);
            $where_str = " orders.billing_state = '$request->state_data'";
        }
        if($request->category_data != null){
            $attr = 'category';
            $where_str = " order_products.category_id = '$request->category_data'";
        }
        if($request->sales_person_data != null){
            $attr = 'sale';
            $where_str = " orders.user_id = '$request->sales_person_data'";
        }
        if($request->price_data != null){
            $attr = 'price';
            if($request->price_data != 'show-all'){
                $sepeate_price_value = explode('-',$request->price_data);
                $where_str .= " and (orders.order_price >= '$sepeate_price_value[0]')";
                $where_str .= " and (orders.order_price <= '$sepeate_price_value[1]')";
            }
        }
        if(isset($request->yoy)){
            $sepeate_date_value = explode('-',$request->yoy[0]);
            // dd($sepeate_date_value);
            $fromd = strtotime($sepeate_date_value[0]);
            $fromdate = date('Y-m-d',$fromd);//wrong format real y-m-d in view
            // dd($fromdate);
            $tod = $sepeate_date_value[1];
            // dd($tod);
            $toda = str_replace(' ', '',implode("-",array_reverse(explode("/", $tod))));
            $todate = date('Y-d-m',strtotime($toda));
            // dd($todate);
            $where_str .= " and (DATE(order_products.created_at) >= '$fromdate')";
            $where_str .= " and (DATE(order_products.created_at) <= '$todate')";
        }
        // dd($attr);
        if($attr == null){
            $attr = null;
            $order_products_data = OrderProduct::select('orders.order_no','products.name as product_name','categories.name as category_name','brands.name as brand_name')
                                            ->leftjoin('orders','orders.id','=','order_products.order_id')
                                            ->leftjoin('products','products.id','=','order_products.product_id')
                                            ->leftjoin('brands','brands.id','=','order_products.brand_id')
                                            ->leftjoin('categories','categories.id','=','order_products.category_id')
                                            ->whereDate('order_products.created_at', '>=',$fromdate)
                                            ->whereDate('order_products.created_at', '<=',$todate)
                                            ->get();
            $qty_name = [];
            $report_csv_name = 'report_'.date('Y_m_d_H_i_s');
            Excel::create($report_csv_name, function($excel) use($order_products_data,$attr,$qty_name){
                $excel->sheet('Report', function($sheet) use($order_products_data,$attr,$qty_name){
                    $sheet->cell('A1', function($cell){
                        $cell->setValue('Report Values');
                    });
                    $sheet->row(1,['Order NO','Product Name','Category Name','Brand Name']);
                    $sheet->loadView('admin.report.product')->with('order_products_data',$order_products_data)->with('attr',$attr)->with('qty_name',$qty_name);
                    $sheet->cell('A1:F1', function($cell) {
                    // Set font
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });
                    
                });
                })->export('csv');
        }else{
            // \DB::enableQueryLog();
            $product_data = OrderProduct::select('products.sku','orders.order_price','users.firstname','orders.billing_state','categories.name','orders.id','orders.order_no')
                                    ->leftjoin('products','order_products.product_id','=','products.id')
                                    ->leftjoin('orders','orders.id','=','order_products.order_id')
                                    ->leftjoin('users','users.id','=','orders.user_id')
                                    ->leftjoin('categories','categories.id','=','order_products.category_id')
                                    ->leftjoin('brands','brands.id','=','order_products.brand_id')
                                    ->whereRaw($where_str, $where_params)
                                    ->get();
                                    // print_r($product_data);exit();
                                    // dd(\DB::getQueryLog());
                                    // dd($product_data);
            $qty_name = [];
            foreach ($product_data as $key => $value) {
                // dd($value['id']);
                $qty_name = OrderSize::select('order_sizes.qty','sizes.name')->leftjoin('sizes','sizes.id','=','order_sizes.size_id')->where('order_sizes.order_id',$value['id'])->get()->toArray();
            }
            $report_csv_name = 'report_'.date('Y_m_d_H_i_s');
            Excel::create($report_csv_name, function($excel) use($product_data,$attr,$qty_name){
                $excel->sheet('Report', function($sheet) use($product_data,$attr,$qty_name){
                    $sheet->cell('A1', function($cell){
                        $cell->setValue('Report Values');
                    });
                    if($attr == 'product'){
                        $sheet->row(1,['Party','SKU']);
                    }
                    if($attr == 'state'){
                        $sheet->row(1,['Party','Rate','State Name']);
                    }
                    if($attr == 'category'){
                        $sheet->row(1,['Party','SKU','Rate']);
                    }
                    else{
                        $sheet->row(1,['Party','Rate']);
                    }
                    $sheet->loadView('admin.report.product')->with('product_data',$product_data)->with('attr',$attr)->with('qty_name',$qty_name);
                    $sheet->cell('A1:F1', function($cell) {
                    // Set font
                        $cell->setFont(array(
                            'bold' => true
                        ));
                    });
                    
                });
                })->export('csv');
        }
    }
}
