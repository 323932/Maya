<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB,Form;
use App\Models\Banner;
use App\Events\BannerEvent;
use App\Http\Requests\BannerRequest;
use Event;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (id like \"%{$search}%\""
                . " or title like \"%{$search}%\""
                . " or type like \"%{$search}%\""
                . " or status like \"%{$search}%\""
                . ")";
            }

            $columns = ['updated_at','title','description','type','image','status','action_id','id'];

            $companymaster_columns_count = Banner::select($columns)
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Banner::select($columns)
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banner = [];
        // dd(old('type'));
        if((old('type')) == 'product')
        {
            // dd(old('type'));
            $banner = (old('type')) ? [""=>"Select Product"] + Product::pluck("name","id")->toArray() :["" =>"Select Product"] + Product::pluck('name', 'id')->toArray();
            // dd($banner);
        }
        elseif (old('type') == 'brand') 
        {
            $banner = (old('type')) ? [""=>"Select Brand"] + Brand::pluck("name","id")->toArray() :["" =>"Select Brand"] + Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $banner = (old('type')) ? [""=>"Select Category"] + Category::pluck("name","id")->toArray() :["" =>"Select Category"] + Category::pluck('name', 'id')->toArray();
        }
        return view('admin.banner.create',compact('banner'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $banner_data = $request->all();
       // dd($banner_data);
        $banner_data['title'] = strip_tags($request->title);
        Event::fire(new BannerEvent($banner_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('banners.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner_data = Banner::find($id);
        $banner_image = explode('/', $banner_data['image']);
        //dd($notification_image[5]);
        $id = $banner_data['id'];
        $type = $banner_data['type'];
        $banner = [];
        // dd(old('type'));
        if((old('type')) == 'product')
        {
            // dd(old('type'));
            $banner = (old('type')) ? [""=>"Select Product"] + Product::pluck("name","id")->toArray() :["" =>"Select Product"] + Product::pluck('name', 'id')->toArray();
            // dd($notification);
        }
        elseif (old('type') == 'brand') 
        {
            $banner = (old('type')) ? [""=>"Select Brand"] + Brand::pluck("name","id")->toArray() :["" =>"Select Brand"] + Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $banner = (old('type')) ? [""=>"Select Category"] + Category::pluck("name","id")->toArray() :["" =>"Select Category"] + Category::pluck('name', 'id')->toArray();
        }
        //dd($notification);
        return view('admin.banner.edit',compact('banner_data','id','banner','banner_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id)
    {
        $banner_data = $request->all();
        //dd($notification_data);
        $banner_data['id'] = $id;
        //dd($notification_data);
        $banner_data['title'] = strip_tags($request->title);
        Event::fire(new BannerEvent($banner_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('banners.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $banner_id = $request->get('id');

        if(is_array($banner_id)){
            foreach ($banner_id as $key => $value) {
                Banner::where('id', $value)->delete();
            }
        }
        else{
            Banner::where('id', $color_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
        ->with('message_type', 'success');
    }

    public function getName(Request $request)
    {
        $type = $request->get('type');        

        if($type == 'product')
        {
            $get_nameIn = Product::pluck('name', 'id')->toArray();
        }
        else if($type == 'brand')
        {
            $get_nameIn = Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $get_nameIn = Category::pluck('name', 'id')->toArray();
        }
        
        $names_list = ['' => 'Select Name'] + $get_nameIn;

        return Form::select('action_id', $names_list, array('id' => 'action_id'));
    }

    public function BannerOrder(){
        $banner_data = Banner::select('id','title','image')->orderby('order_position')->get();
        //dd($banner_data);
        return view('admin.banner.order_image',compact('banner_data'));
    }
    public function Order(Request $request){
        $order = $request->get('order');
        foreach ($order as $key => $value) {

            $index = $key + 1;

            Banner::where('id', $value)->update(array('order_position' => $index));
        }
    }
}
