<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Order;
use App\Models\Color;
use App\Models\Product;
use App\Models\OrderProduct;
use App\Models\State;
use App\Models\OrderSize;
use App\Models\City;
use App\Models\UserPlan;
use App\Models\User;
use App\Models\BrandSize;
use App\Http\Requests\OrderRequest;
use Form;
use Event;
use App\Events\OrderEvent;
use App\Events\UpdateOrderEvent;
use App\Http\Requests\ShipmentRequest;
use App\Events\ShipmentEvent;
use App\Events\InvoiceEvent;
use App\Helpers\FileHelp;
use App\Models\Shipment;
use App\Models\ShipmentProduct;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (orders.order_no like \"%{$search}%\""
                . " or users.firstname like \"%{$search}%\""
                . " or orders.created_at like \"%{$search}%\""
                . " or orders.order_price like \"%{$search}%\""
                . " or orders.status like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['orders.created_at','orders.id','orders.order_no','users.firstname as user','orders.order_price','orders.status'];

            $companymaster_columns_count = Order::select($columns)
            ->leftjoin('users','users.id','=','orders.user_id')
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Order::select($columns)
            ->leftjoin('users','users.id','=','orders.user_id')
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = Category::orderBy('name','asc')->pluck('name','id')->toArray();
        $product = Product::orderBy('name','asc')->pluck('name','id')->toArray();
        $user = User::orderBy('firstname','asc')->pluck('firstname','id')->toArray();
        $state = State::orderBy('title','asc')->pluck('title','id')->toArray();
        $color = Color::orderBy('name','asc')->pluck('name','id')->toArray();

        $user_data = User::select('*')->get()->toArray();
        // dd($user_data);

       $billing_city = (old('billing_state')) ? [""=>"Select City"] + City::where("state_id",old('billing_state'))->orderBy('title','asc')->pluck('title', 'id')->toArray() : array(""=>"Select City");
       $shipping_city = (old('shipping_state')) ? [""=>"Select City"] + City::where("state_id",old('shipping_state'))->orderBy('title','asc')->pluck('title', 'id')->toArray() : array(""=>"Select City");

       // $product_fields = Product::select('without_set','percentage')->get()->toArray();
       //dd($product_fields);
        
        return view('admin.order.create',compact('brand','category','product','user','state','billing_city','shipping_city','color'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $order_data = $request->all();
        
        Event::fire(new OrderEvent($order_data));
        
        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('orders.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {
        $order_data = Order::find($id);
        $id = $order_data['id'];
        $u_name = User::find($order_data['user_id']);
        //dd($order_data['user_id']);

        $user_plan_detail = UserPlan::select('user_plans.user_plan')
        ->leftjoin('users','user_plans.id','=','users.user_plan_id')
        ->where('users.id',$order_data['user_id'])->first();
        //dd($user_plan_detail);

        $shipping_state_name = State::find($order_data['shipping_state']);
        $order_data['shipping_state'] = $shipping_state_name['title'];

        $shipping_city_name = City::find($order_data['shipping_city']);
        $order_data['shipping_city'] = $shipping_city_name['title'];

        $order_data['user_id'] = $u_name['firstname'];

        $order_details = Order::select('products.name as product_name','products.sku as sku','order_products.qty_s','order_products.qty_m','order_products.qty_l','order_products.qty_xl','brands.name as brand_name','order_products.unit_price as order_price','order_products.id as id','products.tax as tax')->where('orders.id',$id)->leftjoin('order_products','order_products.order_id','orders.id')->leftjoin('products','products.id','=','order_products.product_id')->leftjoin('brands','brands.id','=','order_products.brand_id')->get();
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = Category::orderBy('name','asc')->pluck('name','id')->toArray();
        $product = Product::orderBy('name','asc')->pluck('name','id')->toArray();
        $order_data_info = [];
        $total = 0;
        foreach($order_details as $key => $edit)
        {
            $get_details['id'] = $edit['id'];
            $get_details['product_name'] = $edit['product_name'];
            $get_details['brand_name'] = $edit['brand_name'];
            $get_details['sku'] = $edit['sku'];
            $get_details['qty_s'] = $edit['qty_s'];
            $get_details['qty_m'] = $edit['qty_m'];
            $get_details['qty_l'] = $edit['qty_l'];
            $get_details['qty_xl'] = $edit['qty_xl'];
            $total = $edit['qty_s'] + $edit['qty_m'] + $edit['qty_l'] + $edit['qty_xl']; 
            $get_details['order_price'] = $edit['order_price'] * $total;
            $get_details['tax'] = $edit['tax'];
            $order_data_info[] = $get_details;
        }
        
        //shipping order detail
        // dd($order_data_info);
        $shipping_detail_count = Shipment::where('order_id',$id)->count();

        $shipping_detail = [];
        $new_order_detail = [];
        $is_show_invoice = false;
        $ship_num = [];

        if($shipping_detail_count > 0){
            $is_show_invoice = true;
            $ids = Shipment::select('shipments.id as id')->where('shipments.order_id',$id)->leftjoin('shipment_products','shipment_products.shipment_id','=','shipments.id')->get()->toArray();

            $s_id = [];
            foreach ($ids as $key => $value) {
                $s_id[] = $value['id'];
            }
            // dd($id);
            $shipping_detail = ShipmentProduct::select('shipment_products.qty_s as ship_qty_s','shipment_products.qty_m as ship_qty_m','shipment_products.qty_l as ship_qty_l','shipment_products.qty_xl as ship_qty_xl','products.sku as sku','products.name as product_name','shipment_products.price as price','shipment_products.shipment_id as shipping_id','products.id as product_id')->whereIn('shipment_id',$s_id)->leftjoin('products','products.id','=','shipment_products.product_id')->get()->toArray();
            // dd($shipping_detail);
            //get data according to shipping id
            $new_arr = [];
            foreach ($shipping_detail as $shipping_key => $shipping_value)
            {
                $new_arr[$shipping_value['shipping_id']][] = $shipping_value;
            }
            //get data according to product_id 

            $new_qty_arr = [];
            foreach ($shipping_detail as $shippingkey => $shippingvalue)
            {
                $new_qty_arr[$shippingvalue['product_id']][] = $shippingvalue;
            }
            //sum of total qty
            foreach ($new_qty_arr as $key => $value) {
                $qty_s = 0;$qty_m = 0;$qty_l = 0;$qty_xl = 0;$price = 0;
                foreach ($value as $qty_key => $qty_value) {
                    $qty_s = $qty_s + $qty_value['ship_qty_s'];
                    $qty_m = $qty_m + $qty_value['ship_qty_m'];
                    $qty_l = $qty_l + $qty_value['ship_qty_l'];
                    $qty_xl = $qty_xl + $qty_value['ship_qty_xl'];
                    $price = $price + $qty_value['price'];
                    // dd($value);
                    # code...
                }
                $total_qty[$key]['ship_qty_s'] = $qty_s;
                $total_qty[$key]['ship_qty_m'] = $qty_m;
                $total_qty[$key]['ship_qty_l'] = $qty_l;
                $total_qty[$key]['ship_qty_xl'] = $qty_xl;
                $total_qty[$key]['price'] = $price;
            }

            //creating array of total qty
            
            $increment_qty = 0;
            foreach ($total_qty as $key => $value) {
               $total_qty_new[$increment_qty] = $value;
                $increment_qty = $increment_qty + 1;
            }
            // dd($total_qty_new);
            //creating simple array for remain shipping detail
            $shipping_detail = [];
            $increment = 0;
            foreach ($new_arr as $arr_key => $arr_value) {
                $shipping_detail[$increment+1] = $arr_value;
                $increment = $increment + 1;
            }
            //remain shipping qty & price
            

            foreach ($order_data_info as $order_key => $order_value) {
                // dd($order_key);
                $new_order_detail[$order_key]['id'] = $order_value['id'];
                $new_order_detail[$order_key]['product_name'] = $order_value['product_name'];
                $new_order_detail[$order_key]['brand_name'] = $order_value['brand_name'];
                $new_order_detail[$order_key]['sku'] = $order_value['sku'];
                $new_order_detail[$order_key]['qty_s'] = $order_value['qty_s'] - $total_qty_new[$order_key]['ship_qty_s'];
                $new_order_detail[$order_key]['qty_m'] = $order_value['qty_m'] - $total_qty_new[$order_key]['ship_qty_m'];
                $new_order_detail[$order_key]['qty_l'] = $order_value['qty_l'] - $total_qty_new[$order_key]['ship_qty_l'];
                $new_order_detail[$order_key]['qty_xl'] = $order_value['qty_xl'] - $total_qty_new[$order_key]['ship_qty_xl'];
                $new_order_detail[$order_key]['order_price'] = $order_value['order_price'] - $total_qty_new[$order_key]['price'];
                $new_order_detail[$order_key]['tax'] = $order_value['tax'];
            }

            $order_data_info = [];
            $order_data_info = $new_order_detail;

            //shipment num
            $ship_num = Shipment::select('shipping_number','id')->where('order_id',$id)->pluck('shipping_number','id')->toArray();
        }

        return view('admin.order.edit',compact('order_data','id','order_data_info','product','brand','category','shipping_detail','shipping_detail_count','is_show_invoice','ship_num'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShipmentRequest $request, $id)
    {
        $order_data = $request->all();

        $order_data['id'] = $id;
        
        Event::fire(new ShipmentEvent($order_data));

        if($request->save_button == 'save') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('orders.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCity(Request $request)
    {
        $state_id = $request->get('billing_state');        

        $get_cityIn = City::where('state_id', $state_id)->pluck('title', 'id')->toArray();
        $cities = ['' => 'Select City'] + $get_cityIn;
        //dd($cities);

        return Form::select('billing_city', $cities, array('id' => 'city_id'));
    }

    public function getShippingCity(Request $request)
    {
        $shipping_state_id = $request->get('shipping_state');

        $get_shippingCityIn = City::where('state_id', $shipping_state_id)->pluck('title', 'id')->toArray();

        $shippingcities = ['' => 'Select City'] + $get_shippingCityIn;
        //dd($shippingcities);

        return Form::select('shipping_city', $shippingcities, array('id' => 'shipping_city_id'));
        
    }

    public function getProductData(Request $request){
        
        $product_fields = Product::select('tax','without_set','percentage','original_price')
                                    ->where('id',$request['product_id'])
                                    ->get()->toArray();
        $product_fields = head($product_fields);
        // $product_fields = json_encode($product_fields);

        return response()->json(['success' => true, 'product_fields' => $product_fields]);
    }

    public function getSize(Request $request)
    {
        
        $brand_id = $request->get('brand_id'); 
        $b_id = $request->get('b_id');       
        $get_sizeIn = BrandSize::leftjoin('sizes','sizes.id','=','brand_sizes.size_id')->where('brand_id',$brand_id)->where('is_active','1')->get()->toArray();
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = Category::orderBy('name','asc')->pluck('name','id')->toArray();
        $product = Product::orderBy('name','asc')->pluck('name','id')->toArray();
        //print_r($get_sizeIn);
        $color = Color::orderBy('name','asc')->pluck('name','id')->toArray();
        // print_r($get_sizeIn);exit();
        $result = view('admin.order.product_variant', ['data' => $get_sizeIn,'color'=>$color,'product'=>$product,'brand'=>$brand,'category'=>$category,'b_id'=>$b_id])->render();     
        //dd($result); 
        $title = view('admin.order.product_variant_title', ['data' => $get_sizeIn,'color'=>$color,'product'=>$product,'brand'=>$brand,'category'=>$category])->render(); 
         $data = ['result'=>$result,'title'=>$title]; 
         //dd($data);
        return $data;
    }

    public function delete(Request $request)
    {        //dd('hi');
        if(!empty($request->id))
        {
            $id = $request->id;
            if(count($id)>1)
            {
                foreach($id as $key=>$value)
                {
                    $delete_id = OrderProduct::where('order_id',$value)->delete();
                    $delete_id = OrderSize::where('order_id',$value)->delete();                    
                    Order::destroy($value);
                }
            }
            else
            {
                $delete_id = OrderProduct::where('order_id',$id)->delete();
                $delete_id = OrderSize::where('order_id',$id)->delete();                    
                    Order::destroy($index);
            }    
            return back()->with('message', 'Record deleted Successfully.')
            ->with('message_type', 'success');
        }
    }

    public function getDealerData(Request $request)
    {
        $dealer_data = User::select('*')
                                    ->where('id',$request['user_id'])
                                    ->first();
                                    // print_r($dealer_data);
        return $dealer_data;
    }
    public function invoiceStore(Request $request){

        $data = $request->all();
        
        Event::fire(new InvoiceEvent($data));
        
        return response()->json('Invoice genarated Successfully');

    }
    public function OrderCancel(Request $request){

        $this->validate($request,[
            'reason' => 'required',
        ]);
        $data = $request->all();
        $order = Order::find($data['order_id']);
        $order->reason = $data['reason'];
        $order->status = config('Constant.status.cancel');
        $order->save();
        
        return response()->json('Order Cancel Successfully');
        // dd($request->all());
    }
}
