<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\CoupanCode;
use App\Events\CoupanCodeEvent;
use App\Http\Requests\CoupanCodeRequest;
use Event;
use App\Models\Brand;
use App\Models\Category;
use Form;

class CoupanCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (coupan_codes.code like \"%{$search}%\""
                . " or coupan_codes.code like \"%{$search}%\""
                . " or users.firstname like \"%{$search}%\""
                . " or coupan_codes.discount like \"%{$search}%\""
                . " or coupan_codes.percentage like \"%{$search}%\""
                . " or coupan_codes.to_date like \"%{$search}%\""
                . " or coupan_codes.from_date like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['coupan_codes.id','coupan_codes.created_at','coupan_codes.code','users.firstname as user','coupan_codes.percentage','coupan_codes.to_date','coupan_codes.from_date','coupan_codes.discount'];

            $companymaster_columns_count = CoupanCode::select($columns)
            ->leftjoin('users','users.id','=','coupan_codes.user_id')
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = CoupanCode::select($columns)
            ->leftjoin('users','users.id','=','coupan_codes.user_id')
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $cArray = $companymaster_list->get();

            foreach ($cArray as $key => $value) {
                $cArray[$key] = $value;
                $cArray[$key]['sr_no'] = $key+1;
            }

            $companymaster_list =  $cArray;

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.coupan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        $category = (old('brand_id')) ? [""=>"Select Category"] + Category::where("brand_id",old('brand_id'))->orderBy('name','asc')->pluck('name', 'id')->toArray() : array(""=>"Select Category");
        $user = User::orderBy('firstname','asc')->pluck('firstname','id')->toArray();
       
        return view('admin.coupan.create',compact('brand','category','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoupanCodeRequest $request)
    {
        $coupan_data = $request->all();
        //dd($coupan_data);
        $coupan_data['code'] = strip_tags($request->code);
        Event::fire(new CoupanCodeEvent($coupan_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('coupons.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupan_data = CoupanCode::find($id);
        $id = $coupan_data['id'];

        $code_array = explode(',', $coupan_data['user_id']);
        $user_id = [];
        foreach($code_array as $key => $value)
        {
            $user_id[] = $value;
        }

        $user = User::orderBy('firstname','asc')->pluck('firstname','id')->toArray();
        
        return view('admin.coupan.edit',compact('user','coupan_data','id','user_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CoupanCodeRequest $request, $id)
    {
        $coupan_data = $request->all();
        //dd($coupan_data);
        $coupan_data['code'] = strip_tags($request->code);
        $coupan_data['id'] = $id;
        Event::fire(new CoupanCodeEvent($coupan_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('coupons.index')->with('message','Record updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $coupan_id = $request->get('id');
        //dd($coupan_id);

        if(is_array($coupan_id)){
            foreach ($coupan_id as $key => $value) {
                CoupanCode::where('id', $value)->delete();
            }
        }
        else{
            CoupanCode::where('id', $coupan_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
        ->with('message_type', 'success');
    }
}
