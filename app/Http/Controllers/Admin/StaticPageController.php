<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StaticPage;
use App\Http\Requests\StaticPageRequest;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (title like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['created_at','id','title','description'];

            $companymaster_columns_count = StaticPage::select($columns)
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = StaticPage::select($columns)
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.static-page.index');        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.static-page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaticPageRequest $request)
    {
         $page_data= new StaticPage();

        $page_data->title= $request['title'];
        $page_data->description= $request['description'];

        $page_data->save();

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('pages.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_data = StaticPage::find($id);
        $id = $page_data['id'];

        return view('admin.static-page.edit',compact('page_data','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaticPageRequest $request, $id)
    {
        $request_data = $request->all();
       
        $page_data = StaticPage::find($id);
    
        $data =StaticPage::where('id', $id)->update([
            'title' => $request_data['title'],
            'description' =>$request_data['description']]);

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('pages.index')->with('message','Record updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $page_id = $request->get('id');

        if(is_array($page_id)){
            foreach ($page_id as $key => $value) {
                StaticPage::where('id', $value)->delete();
            }
        }
        else{
            StaticPage::where('id', $page_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
        ->with('message_type', 'success');
    }
}
