<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\User;
use App\Models\UserPlan;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserPasswordRequest;
use Mail;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (firstname like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . " or mobile like \"%{$search}%\""
                . " or gstno like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['updated_at','gstno','mobile','email','status',DB::raw('CONCAT(users.firstname, " ", users.lastname) AS name'),'id'];

            $companymaster_columns_count = User::select($columns)
            ->whereRaw($where_str, $where_params)
            ->where('type','!=','seller')
            ->count();

            $companymaster_list = User::select($columns)
            ->whereRaw($where_str, $where_params)
            ->where('type','!=','seller');

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $new_id = explode('_', $id);
        // dd($new_id);
        $user_data = User::find($new_id[1]);
        $id = $user_data['id'];
        $status = $new_id[0];
        $user_plan = UserPlan::orderBy('user_plan','asc')->pluck('user_plan','id')->toArray();
        return view('admin.user.edit',compact('user_plan','id','status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $request_data = $request->all();
        // dd('id');
        $user_data= User::find($id);

        Mail::send('admin.user.mail',[], function($message) use ($user_data) {
            $message->to($user_data['email'])->cc('Maayaa@maayaaclothing.com')->subject('Dealer Approved');

        });
        
        $status = '0';
        if($request_data['status'] == 0){
            $status = '1';
        }
        // dd($request_data['status']);
        // dd($status);
        $data = User::find($id);
        $data->status = $status;
        $data->type = $request_data['type'];
        $data->user_plan_id = $request_data['user_plan_id'];
        $data->save();
        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('dealers.index')->with('message','Record added Successfully')->with('message_type','success');
    }
    public function deActive($id){
        //dd('hi');
        $new_id = explode('_', $id);
        $status = '0';
        $data = User::find($new_id[1]);
        $data->status = $status;
        $data->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view($id)
    {
        $user_data = User::where('id',$id)->first();
        //  dd($user_data);
        return view('admin.user.view',compact('user_data'));
    }

    public function ResetApiViewFile()
    {
        return view('apimail.app_reset_password');
    }

    public function ApiResetPassword(UserPasswordRequest $request)
    {
        $input = $request->all();
        //$password = bcrypt($input['password']);
        //$user = new User();
        $user_id = User::select('id','status')->where('email',$input['email'])->first();
        //dd($user_id);
        $user_email = User::where('email','=',$input['email'])->exists();
        if($user_email == true && $user_id['status'] == 1)
        {//dd('1');
            $data =User::where('id', $user_id['id'])->update(['password' => bcrypt($input['password'])]);
            //return toast('Your password reset Successfully.');
            return back()->with('message','Your password reset Successfully.')
            ->with('message_type','success');
        }
        else
        {
          //dd('2');
            return back()->with('message','Email address does not exists.')
            ->with('message_type','success');
        }
    }
}
