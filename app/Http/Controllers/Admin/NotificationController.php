<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB,Form;
use App\Models\Notification;
use App\Events\NotificationEvent;
use App\Http\Requests\NotificationRequest;
use Event;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (id like \"%{$search}%\""
                . " or title like \"%{$search}%\""
                . " or type like \"%{$search}%\""
                . " or status like \"%{$search}%\""
                . ")";
            }

            $columns = ['id','updated_at','title','description','type','image','status','action_id'];

            $companymaster_columns_count = Notification::select($columns)
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Notification::select($columns)
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.notification.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $notification = [];
        // dd(old('type'));
        if((old('type')) == 'product')
        {
            // dd(old('type'));
            $notification = (old('type')) ? [""=>"Select Product"] + Product::pluck("name","id")->toArray() :["" =>"Select Product"] + Product::pluck('name', 'id')->toArray();
            // dd($notification);
        }
        elseif (old('type') == 'brand') 
        {
            $notification = (old('type')) ? [""=>"Select Brand"] + Brand::pluck("name","id")->toArray() :["" =>"Select Brand"] + Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $notification = (old('type')) ? [""=>"Select Category"] + Category::pluck("name","id")->toArray() :["" =>"Select Category"] + Category::pluck('name', 'id')->toArray();
        }
        return view('admin.notification.create',compact('notification'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationRequest $request)
    {
        $notification_data = $request->all();
       // dd($notification_data);
        $notification_data['title'] = strip_tags($request->title);
        Event::fire(new NotificationEvent($notification_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('notifications.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification_data = Notification::find($id);
        $notification_image = explode('/', $notification_data['image']);
        //dd($notification_image[5]);
        $id = $notification_data['id'];
        $type = $notification_data['type'];
        $notification = [];
        // dd(old('type'));
        if((old('type')) == 'product')
        {
            // dd(old('type'));
            $notification = (old('type')) ? [""=>"Select Product"] + Product::pluck("name","id")->toArray() :["" =>"Select Product"] + Product::pluck('name', 'id')->toArray();
            // dd($notification);
        }
        elseif (old('type') == 'brand') 
        {
            $notification = (old('type')) ? [""=>"Select Brand"] + Brand::pluck("name","id")->toArray() :["" =>"Select Brand"] + Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $notification = (old('type')) ? [""=>"Select Category"] + Category::pluck("name","id")->toArray() :["" =>"Select Category"] + Category::pluck('name', 'id')->toArray();
        }
        //dd($notification);
        return view('admin.notification.edit',compact('notification_data','id','notification','notification_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationRequest $request, $id)
    {
        $notification_data = $request->all();
        //dd($notification_data);
        $notification_data['id'] = $id;
        //dd($notification_data);
        $notification_data['title'] = strip_tags($request->title);
        Event::fire(new NotificationEvent($notification_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('notifications.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function delete(Request $request)
    {
        $notification_id = $request->get('id');

        if(is_array($notification_id)){
            foreach ($notification_id as $key => $value) {
                Notification::where('id', $value)->delete();
            }
        }
        else{
            Notification::where('id', $notification_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
        ->with('message_type', 'success');
    }

    public function getName(Request $request)
    {
        $type = $request->get('type');        

        if($type == 'product')
        {
            $get_nameIn = Product::pluck('name', 'id')->toArray();
        }
        else if($type == 'brand')
        {
            $get_nameIn = Brand::pluck('name', 'id')->toArray();
        }
        else
        {
            $get_nameIn = Category::pluck('name', 'id')->toArray();
        }
        
        $names_list = ['' => 'Select Name'] + $get_nameIn;

        return Form::select('action_id', $names_list, array('id' => 'action_id'));
    }
}
