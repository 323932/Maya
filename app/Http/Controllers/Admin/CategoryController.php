<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Brand;
use App\Events\CategoryEvent;
use App\Http\Requests\CategoryRequest;
use Event;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (categories.name like \"%{$search}%\""
                . " or brands.name like \"%{$search}%\""
                . " or categories.is_active like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['categories.created_at','categories.id','categories.name','brands.name as brand','categories.is_active'];

            $companymaster_columns_count = Category::select($columns)
            ->leftjoin('brands','brands.id','=','categories.brand_id')
            ->whereRaw($where_str, $where_params)
            ->count();

            $companymaster_list = Category::select($columns)
            ->leftjoin('brands','brands.id','=','categories.brand_id')
            ->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $companymaster_list = $companymaster_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $companymaster_list = $companymaster_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $companymaster_list = $companymaster_list->get();

            $response['iTotalDisplayRecords'] = $companymaster_columns_count;
            $response['iTotalRecords'] = $companymaster_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $companymaster_list->toArray();

            return $response;
        }
        return view('admin.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        return view('admin.category.create',compact('brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category_data = $request->all();
        //dd($category_data);
        $category_data['name'] = strip_tags($request->name);        
        Event::fire(new CategoryEvent($category_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('category.index')->with('message','Record added Successfully')->with('message_type','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category_data = Category::find($id);
        $id = $category_data['id'];
        $brand = Brand::orderBy('name','asc')->pluck('name','id')->toArray();
        return view('admin.category.edit',compact('brand','category_data','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category_data = $request->all();
        //dd($category_data);
        $category_data['name'] = strip_tags($request->name);
        $category_data['id'] = $id;
        Event::fire(new CategoryEvent($category_data));

        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record Updated  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('category.index')->with('message','Record Updated Successfully')->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        $category_id = $request->get('id');

        if(is_array($category_id)){
            foreach ($category_id as $key => $value) {
                Category::where('id', $value)->delete();
            }
        }
        else{
            Category::where('id', $category_id)->delete();
        }    
        return back()->with('message', 'Record deleted Successfully.')
        ->with('message_type', 'success');
    }

    public function CategoryOrder(){
        $category_data = Category::select('categories.id','categories.name','categories.image','brands.name as brand_name')->leftjoin('brands','brands.id','=','categories.brand_id')->orderby('categories.order_position')->get();
        //dd($category_data);
        return view('admin.category.order_image',compact('category_data'));
    }
    public function Order(Request $request){
        $order = $request->get('order');
        foreach ($order as $key => $value) {

            $index = $key + 1;

            Category::where('id', $value)->update(array('order_position' => $index));
        }
    }
}
