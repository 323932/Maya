<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SalesPerson;
use App\Http\Requests\SalesPersonRequest;
use App\Events\SalesPersonEvent;
use Event;
use App\Models\User;
use DB;

class SalesPersonController extends Controller
{
	public function index(Request $request) {
		if($request->ajax()){
            $where_str    = "1 = ?";
            $where_params = array(1); 

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (firstname like \"%{$search}%\""
                . " or lastname like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . ")";
            }                                            

            $columns = ['updated_at','firstname', 'lastname','email'];

            $sales_person_columns_count = User::select($columns)
                              ->where('type','=','seller')
									            ->whereRaw($where_str, $where_params)
									            ->count();

            $sales_person_list = User::select($columns)
                            ->where('type','=','seller')
           									->whereRaw($where_str, $where_params);

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $sales_person_list = $sales_person_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }          

            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $sales_person_list = $sales_person_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            } 


            $sales_person_list = $sales_person_list->get();

            $response['iTotalDisplayRecords'] = $sales_person_columns_count;
            $response['iTotalRecords'] = $sales_person_columns_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $sales_person_list->toArray();

            return $response;
        }
    	return view('admin.sales_person.index');
	}

	public function create() {
	  	return view('admin.sales_person.create');
	}

	public function store(SalesPersonRequest $request) {

	   	$sales_person_data = $request->all();
       
       $sales_person_data['firstname'] = strip_tags($request->firstname);        
        $sales_person_data['lastname'] = strip_tags($request->lastname);
        Event::fire(new SalesPersonEvent($sales_person_data));
        
        if($request->save_button == 'save_new') 
        {
            return back()->with('message','Record added  Successfully')
            ->with('message_type','success');
        }
        return redirect()->route('sales-person.index')->with('message','Record added Successfully')->with('message_type','success');
	}

	public function edit($id) {
		// $sales_person_data = SalesPerson::find($id);
		// $id = $sales_person_data['id'];

	 // 	return view('admin.sales_person.edit',compact('sales_person_data','id'));
	}

	public function update(SalesPersonRequest $request,$id) {
		// $sales_person_data = $request->all();

  //      	$sales_person_data['id'] = $id;

  //       Event::fire(new SalesPersonEvent($sales_person_data));
        
  //       if($request->save_button == 'save_new') 
  //       {
  //           return back()->with('message','Record updated  Successfully')
  //           ->with('message_type','success');
  //       }
  //       return redirect()->route('sales-person.index')->with('message','Record updated Successfully')->with('message_type','success');
	}

	public function delete(Request $request) {
		
	 // 	$id = $request->get('id');
	     
		// if (!is_array($id)) {
		// 	$id = array($id);
		// }
	    
	 //    $sales_person_delete = SalesPerson::whereIn('id',$id)->delete();

	 //    return response()->json(array('success' => true), 200);
	}
}
