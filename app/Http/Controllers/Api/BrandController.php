<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Validator;

class BrandController extends BaseController
{
    public function getAllBrands(){
        $brand_count = Brand::where('is_active',true)->count();

        if($brand_count > 0){
            $brands = Brand::where('is_active',true)->get()->toArray();
            $brands_arr = [];
            $arr = [];
            foreach ($brands as $key => $value) {
                $brands_arr['id'] = $value['id'];
                $brands_arr['name'] = $value['name'];
                $brands_arr['image'] = LOCAL_IMAGE_PATH.'brands/'.$value['image'];
                $brands_arr['hex_code'] = $value['hex_code'];
                $brands_arr['title_color'] = $value['title_color'];
                $brands_arr['logo_image'] = LOCAL_IMAGE_PATH.'brands/'.$value['logo_image'];
                $brands_arr['is_active'] = $value['is_active'];
                $arr[] = $brands_arr;
            }
            
            $success['brands'] = $arr;

            return $this->sendResponse($success,'Get all brand fetch successfully'); 
        }
        return $this->sendError('There is no record found in brand');
    }
    public function getBrandsCategory(Request $request){
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $brand_id = $input['brand_id'];

        $categories_count = Category::where('brand_id',$brand_id)->where('is_active',1)->count(); 
        
        if($categories_count > 0){
            $categories = Category::where('brand_id',$brand_id)->where('is_active',1)->get()->toArray();

            $catgories_arr = [];
            $arr = [];
            foreach ($categories as $key => $value) {
                $catgories_arr['id'] = $value['id'];
                $catgories_arr['name'] = $value['name'];
                $catgories_arr['image'] = LOCAL_IMAGE_PATH.'categories/'.$value['image'];
                $arr[] = $catgories_arr;
            }
            $success['categories'] = $arr;
            return $this->sendResponse($success,"Get all brand's category fetch successfully");
        }
        
        return $this->sendError('There is no record found in Categories of these brand'); 

    }
    public function getAllCollections(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $brand_id = $input['brand_id'];

        $collections_count = Product::select('collection')->where('brand_id',$brand_id)->count();
        //dd($collections_count);
        if($collections_count > 0){
            $collections = Product::select('collection')->where('brand_id',$brand_id)->get()->toArray();

            $success['collections'] = $collections;
        }

        $categories_count = Category::where('brand_id',$brand_id)->where('is_active',1)->count(); 
        
        if($categories_count > 0){
            $categories = Category::where('brand_id',$brand_id)->where('is_active',1)->get()->toArray();

            $catgories_arr = [];
            $arr = [];
            foreach ($categories as $key => $value) {
                $catgories_arr['id'] = $value['id'];
                $catgories_arr['name'] = $value['name'];
                $arr[] = $catgories_arr;
            }
            $success['categories'] = $arr;
            return $this->sendResponse($success,"Get all brand's collection and category fetch successfully");
        }
        
        return $this->sendError('There is no record found in Categories of these brand');
    }
}
