<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\UserPlan;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\Color;
use App\Models\User;
use App\Models\ProductSize;
use Mail;

class ProductController extends BaseController
{
	public function productDetail(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();

        $product_id = $input['product_id'];
        $user_id = $input['user_id'];

        $product_count = Product::where('id',$product_id)->where('products.status',1)->count(); 
        
        $user_count = User::where('id',$user_id)->where('status',1)->count(); 
        if($product_count > 0){

            $product_detail = Product::select('products.id','products.category_id','products.brand_id','products.sku','products.name','products.original_price','products.description','products.status','products.color_id','products.attribute','products.information','products.qty_s','products.qty_m','products.qty_l','products.qty_xl','products.tax','products.is_set_available','products.without_set','products.percentage')->with('productvariant')->with('productimage')->leftjoin('product_variants','product_variants.product_id','=','products.id')->where('products.id',$product_id)->where('products.status',1)->first()->toArray();
            $color_data = [];
            
            foreach ($product_detail['productvariant'] as $key => $value) {
                $color_data[] = $value['color_id'];
            }
            $color = Color::whereIn('id',$color_data)->get()->toArray();
            unset($product_detail['productvariant']);
            $product_detail['color'] = $color;

            $success['products'] = $product_detail;             
        }
        if ($user_count > 0) {
            
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',$user_id)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;      

        }
        return $this->sendResponse($success,"Get all product and user detail fetch successfully");
        
        if(!$product_count >0 && !$user_count>0){   
            return $this->sendError('There is no record found in Proudcts and Users');
        }
    }
    public function productDetailNew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        //dd($input);
        $product_id = $input['product_id'];
        $user_id = $input['user_id'];

        $product_count = Product::where('id',$product_id)->where('products.status',1)->count(); 
        
        $user_count = User::where('id',$user_id)->where('status',1)->count(); 
        if($product_count > 0){

            $product_detail = Product::select('products.id','products.category_id','products.brand_id','products.sku','products.name','products.original_price','products.description','products.status','products.color_id','products.attribute','products.information','products.qty_s','products.qty_m','products.qty_l','products.qty_xl','products.tax','products.is_set_available','products.without_set','products.percentage')->with('productvariant')->with('productimage')->leftjoin('product_variants','product_variants.product_id','=','products.id')->where('products.id',$product_id)->where('products.status',1)->first()->toArray();
            $color_data = [];
            $brand_id = $product_detail['brand_id'];
            $sizeList = [];
            $size = ProductSize::select('sizes.name as size','product_sizes.qty')
            ->leftjoin('sizes','sizes.id','=','product_sizes.size_id')->where('product_sizes.brand_id',$product_id)->get()->toArray();
            //dd($size);
            // $qty_s = [];
            // $qty_m = [];
            // $qty_l = [];
            // $qty_xl = [];

            foreach ($product_detail['productvariant'] as $key => $value) {
            //dd($value);
                $color_data[] = $value['color_id'];
                 $sizeList[] = $size;
                // $qty_s[] = $value['qty_s'];
                // $qty_m[] = $value['qty_m'];
                // $qty_l[] = $value['qty_l'];
                // $qty_xl[] = $value['qty_xl'];
            }
            $color = Color::whereIn('id',$color_data)->get()->toArray();
            foreach ($color as $color_key => $color_value) {
                $color[$color_key]['sizeList'] = $sizeList[$color_key];
                // $color[$color_key]['qty_s'] = $qty_s[$color_key];
                // $color[$color_key]['qty_m'] = $qty_m[$color_key];
                // $color[$color_key]['qty_l'] = $qty_l[$color_key];
                // $color[$color_key]['qty_xl'] = $qty_xl[$color_key];
            }
            unset($product_detail['productvariant']);
            $product_detail['color'] = $color;

            $success['products'] = $product_detail;             
        }
        if ($user_count > 0) {
            
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',$user_id)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;      

        }
        if($input['user_id'] == 0){
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',21)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;
        }
        return $this->sendResponse($success,"Get all product and user detail fetch successfully");
        
        if(!$product_count >0 && !$user_count>0){   
            return $this->sendError('There is no record found in Proudcts and Users');
        }
    }
    public function getProduct(Request $request){
    	$validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();

        $category_id = $input['category_id'];
        $user_id = $input['user_id'];

        $product_count = Product::where('category_id',$category_id)->where('products.status',1)->count(); 

        $user_count = User::where('id',$user_id)->where('status',1)->count();
        $product_detail = [];
        $user_detail = [];
        $success = [];
        $error = [];

        if($product_count > 0){
            
            $category = Category::where('is_active',1)->where('id',$category_id)->count();
            if($category > 0){
	            if($input['user_id'] == 0){
                   $product_detail = Product::with('defaultproductimage')->where('category_id',$category_id)->where('products.status',1)->orderBy('updated_at','desc')->limit(4)->get()->toArray();
                }
                else{
                    $product_detail = Product::with('defaultproductimage')->where('category_id',$category_id)->where('products.status',1)->orderBy('updated_at','desc')->get()->toArray();
                }
                foreach ($product_detail as $key => $value) {
                    $defaultproductimage = $value['defaultproductimage'];
                    $defaultproductimage = head($defaultproductimage);
                    $product_detail[$key]['image_url'] = $defaultproductimage['image'];
                    unset($product_detail[$key]['defaultproductimage']);
                }
                $success['product_detail'] = $product_detail;		
            }else{
            	$product_count = 0;
            }
        }

        if ($user_count > 0) {
        	
        	$user_detail = UserPlan::select('user_plans.discount')->where('users.id',$user_id)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;			

        }
        if($input['user_id'] == 0){
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',21)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;
        }

        if(!$product_count >0 && !$user_count>0){	
            return $this->sendError('There is no record found in Proudcts and Users');
        }
        return $this->sendResponse($success,"Get all product and user detail fetch successfully");
    }
    public function getAllProduct(Request $request){
        
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();

        $user_id = $input['user_id'];
        $product_count = Product::where('products.status',1)->count(); 

        $user_count = User::where('id',$user_id)->where('status',1)->count();
        $product_detail = [];
        $user_detail = [];
        $success = [];
        $error = [];

        if($product_count > 0){
            
             if($input['user_id'] == 0){
                $product_detail = Product::with('defaultproductimage')->where('products.status',1)->orderBy('updated_at','desc')->limit(10)->get()->toArray();
            }
            else{
                $product_detail = Product::with('defaultproductimage')->where('products.status',1)->orderBy('updated_at','desc')->limit(20)->get()->toArray();
            }
            foreach ($product_detail as $key => $value) {
                $defaultproductimage = $value['defaultproductimage'];
                $defaultproductimage = head($defaultproductimage);
                $product_detail[$key]['image_url'] = $defaultproductimage['image'];
                unset($product_detail[$key]['defaultproductimage']);
            }
            $success['product_detail'] = $product_detail; 
        }

        if ($user_count > 0) {
            
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',$user_id)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;           

        }
        // if($input['user_id'] == 0){
        //     $success['user_plan'] = "0%";
        // }
         if($input['user_id'] == 0){
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',21)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;
        }

        if(!$product_count >0 && !$user_count>0){   
            return $this->sendError('There is no record found in Proudcts and Users');
        }
        return $this->sendResponse($success,"Get all product and user detail fetch successfully");
    }

    public function categorywiseProduct(Request $request){
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();

        $category_id = $input['category_id'];
        $user_id = $input['user_id'];

        $product_count = Product::where('category_id',$category_id)->where('products.status',1)->count(); 
        $user_count = User::where('id',$user_id)->where('status',1)->count();

        $product_detail = [];
        $user_detail = [];
        $success = [];
        $error = [];

        if($product_count > 0){
            
            $category = Category::where('is_active',1)->where('id',$category_id)->count();

            if($category > 0){
                    $product_detail = Product::with('defaultproductimage')->where('category_id',$category_id)->where('products.status',1)->orderBy('updated_at','desc')->get()->toArray();
                    
                foreach ($product_detail as $key => $value) {
                    $defaultproductimage = $value['defaultproductimage'];
                    $defaultproductimage = head($defaultproductimage);
                    $product_detail[$key]['image_url'] = $defaultproductimage['image'];
                    unset($product_detail[$key]['defaultproductimage']);
                }
                $success['product_detail'] = $product_detail;       
            }else{
                $product_count = 0;
            }
        }

        if ($user_count > 0) {
            
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',$user_id)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;      

        }
        if($input['user_id'] == 0){
            $user_detail = UserPlan::select('user_plans.discount')->where('users.id',21)->where('users.status',1)->leftjoin('users','users.user_plan_id','=','user_plans.id')->first();
            $success['user_plan'] = $user_detail;
        }

        if(!$product_count >0 && !$user_count>0){   
            return $this->sendError('There is no record found in Proudcts and Users');
        }
        return $this->sendResponse($success,"Get all product and user detail fetch successfully");
    }

    public function FilterProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'color_id' => 'required',
            'size_id' => 'required',
            'category_id' => 'required',
            'brand_id' => 'required',
            'collection' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $input = $request->all();
        //dd($input);
        $color = $input['color_id'];
        $size = explode(',',$input['size_id']);
        $category = explode(',',$input['category_id']);
        $brand = explode(',',$input['brand_id']);
        $collection = explode(',',$input['collection']);
        // $filter_product_list = Product::select('*')->where('id',261)->with('productvariant')->get()->toArray();
        $filter_product_list = Product::with(array('productvariant' => function($query) use($color){
            $query->whereRaw("find_in_set($color,color_id)");
        }))->get()->toArray();

        $result = \DB::table("products")

               ->select("*")
               ->with('productvariant')
               ->whereRaw("find_in_set($color,'color_id')")

               ->get();

        dd($result);
    }
}

