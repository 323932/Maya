<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use Validator;

class CategoryController extends BaseController
{
    public function getAllCategories()
    {
    	$category_count = Category::where('is_active',true)->count();
        if($category_count > 0){
            $categories = Category::where('is_active',true)->get()->toArray();
            // dd($categories);
            $array = array_unique(array_column($categories, 'name'));
            $unique_data = array_intersect_key($categories, $array);

            //dd($unique_data);
            $categories_arr = [];
            $arr = [];
            foreach ($unique_data as $key => $value) {
            	$categories_arr['id'] = $value['id'];
                $categories_arr['name'] = $value['name'];
                $categories_arr['image'] = LOCAL_IMAGE_PATH.'categories/'.$value['image'];
                $arr[] = $categories_arr;
            }
            
            $success['categories'] = $arr;

            return $this->sendResponse($success,'Get all category fetch successfully'); 
        }
        return $this->sendError('There is no record found in  category');
    }

    public function brandwiseCategory(Request $request)
    {
        //dd('hi');
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $input = $request->all();

        $category_id = $input['category_id'];
        $category_name = Category::select('name')->where('id',$category_id)->first();
        $brand_id = Category::select('brand_id')->where('name',$category_name['name'])->get()->toArray();
        $ids = [];
        foreach ($brand_id as $key => $value) {
            $ids [] = $value['brand_id'];
        }

        $product = Product::whereIn('brand_id',$ids)->get()->toArray();
        //dd($product);
        // $product_count = Product::where('category_id',$category_id)->get()->toArray();
    }

    public function getCategoryBrand(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        $input = $request->all();
        $brand_count = Category::select('brand_id')->where('name',$input)->count();

        if($brand_count > 0){
            $category_id = Category::select('brand_id')->where('name',$input)->get()->toArray();
            //dd($category_id);
            $success['brands'] = $category_id;

            return $this->sendResponse($success,"Get brand detail fetch successfully");
        }
        return $this->sendError('There is no record found');
    }
}
