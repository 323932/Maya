<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\CoupanCode;
use App\Models\Notification;
use App\Models\City;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use Illuminate\Support\Facades\Auth;
use Validator,DB;

class SearchController extends BaseController
{
	public function getStates(){
		
		$states_count = State::select('id','title')->where('country_id',101)->count(); 

		if($states_count > 0){
			$states = State::select('id','title')->where('country_id',101)->get()->toArray();
			
			$success['state'] = $states;

			return $this->sendResponse($success,"Get all State fetch successfully");
		}
		return $this->sendError('There is no record found in State');
	}
	public function getCities(Request $request){

		$validator = Validator::make($request->all(), [
            'state_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
		$cities_count = City::select('id','title')->where('state_id',$input['state_id'])->count(); 

		if($cities_count > 0){
			$cities = City::select('id','title')->where('state_id',$input['state_id'])->get()->toArray();
			
			$success['cities'] = $cities;

			return $this->sendResponse($success,"Get all City fetch successfully");
		}
		return $this->sendError('There is no record found in City');
	}
	public function getCouponCode(Request $request){
		$validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'code' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $date = Date('Y-m-d');

        $coupon_count = CoupanCode::where('user_id',$input['user_id'])->where('code',$input['code'])->where('to_date','>=',$date)->where('from_date','<=',$date)->count();

        if($coupon_count > 0){
        	
        	$coupon_data = CoupanCode::where('user_id',$input['user_id'])->where('code',$input['code'])->where('to_date','>=',$date)->where('from_date','<=',$date)->first();
        	
       		$success['coupon_type'] = $coupon_data['coupon_type'];
       		$success['value'] = $coupon_data['value'];

			return $this->sendResponse($success,"Get Coupon detail fetch successfully");
		}
		return $this->sendError('Coupon code is not valid');
	}
	public function getNotification(){
		$notification_count = Notification::where('status','enable')->get()->count();

        if($notification_count > 0){
        	
        	$notification_data = Notification::select('id','title','description','type','action_id','image')->where('status','enable')->get()->toArray();
        	
       		$success['notification'] = $notification_data;

			return $this->sendResponse($success,"Get notification detail fetch successfully");
		}
		return $this->sendError('There is no record found in notification');
	}
	public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'search_text' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $search_text = $input['search_text'];
        $search = [];
        $search_arr = [];
        $temp_search_arr = [];

        //brand
        $brand_search_count = Brand::where('name', 'like', '%' . $search_text . '%')->count();
        if($brand_search_count > 0){
            $brand_search =  Brand::where('name', 'like', '%' . $search_text . '%')->get()->toArray();

            foreach ($brand_search as $brand_key => $brand_value) {
                $brand_search[$brand_key]['action'] = 'Brand';
            }   
            array_push($search, $brand_search);
        }
        //category
        $category_search_count = Category::where('name', 'like', '%' . $search_text . '%')->count();

        if($category_search_count > 0){
            $category_search = Category::where('name', 'like', '%' . $search_text . '%')->get()->toArray();

            foreach ($category_search as $category_key => $category_value) {
                $category_search[$category_key]['action'] = 'Category';
            }
            array_push($search, $category_search);
        }
        //product
        $color_data_count = Color::select('product_variants.product_id')->where('colors.name', 'like', '%' . $search_text . '%')->leftjoin('product_variants','product_variants.color_id','colors.id')->count();

        if($color_data_count > 0){
            $color_data = Color::select('product_variants.product_id')->where('colors.name', 'like', '%' . $search_text . '%')->leftjoin('product_variants','product_variants.color_id','colors.id')->get()->toArray();

            $product_data = Product::whereIn('id',$color_data)->get()->toArray();

            foreach ($product_data as $color_key => $color_value) {
                $product_data[$color_key]['action'] = 'Product';
            }
            array_push($search, $product_data);
        }
        $product_search_count = Product::where('sku', 'like', '%' . $search_text . '%')->orWhere('name', 'like', '%' . $search_text . '%')->orWhere('description', 'like', '%' . $search_text . '%')->count();
        if($product_search_count > 0){
            $product_search = Product::where('sku', 'like', '%' . $search_text . '%')->orWhere('name', 'like', '%' . $search_text . '%')->orWhere('description', 'like', '%' . $search_text . '%')->get()->toArray();

            foreach ($product_search as $product_key => $product_value) {
                $product_search[$product_key]['action'] = 'Product';
            }
            array_push($search, $product_search);
            
        }
        foreach ($search as $search_key => $search_value) {
            foreach ($search_value as $s_key => $s_value) {
                $temp_search_arr[] = $s_value;
            }
        }
        foreach ($temp_search_arr as $key => $value) {
            $search_arr[$key]['title'] = $value['name'];
            $search_arr[$key]['action_id'] = $value['id'];
            $search_arr[$key]['type'] = $value['action'];
        }

        if($product_search_count == 0 && $brand_search_count==0 && $category_search_count==0 && $color_data_count==0){   
            return $this->sendError('There is no record found in Proudcts and Category and Brand');
        }else{
            
            return $this->sendResponse($search_arr,"Search record successfully");
        }
    }
}	