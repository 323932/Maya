<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;

class HomeScreenController extends BaseController
{
    public function getAllBanners()
    {
    	$brand_count = Brand::where('is_active',true)->count();
        if($brand_count > 0)
        {
            $brands = Brand::where('is_active',true)->get()->toArray();

            $brands_arr = [];
            $arr = [];
            foreach ($brands as $key => $value) {
            	$brands_arr['id'] = $value['id'];
            	$brands_arr['name'] = $value['name'];
                $brands_arr['image'] = LOCAL_IMAGE_PATH.'brands/'.$value['image'];
                $brands_arr['logo_image'] = LOCAL_IMAGE_PATH.'brands/'.$value['logo_image'];
                $arr[] = $brands_arr;
            }
            //dd($arr);
            
            $success['brands'] = $arr;
        }

        $category_count = Category::where('is_active',true)->count();
        if($category_count > 0)
        {
            $categories = Category::where('is_active',true)->get()->toArray();
            // dd($categories);
            $array = array_unique(array_column($categories, 'name'));
            $unique_data = array_intersect_key($categories, $array);

            //dd($unique_data);
            $categories_arr = [];
            $arr = [];
            foreach ($unique_data as $key => $value) {
            	$categories_arr['id'] = $value['id'];
                $categories_arr['name'] = $value['name'];
                $categories_arr['image'] = LOCAL_IMAGE_PATH.'categories/'.$value['image'];
                $arr[] = $categories_arr;
            }
            
            $success['categories'] = $arr;
        }

    	$banner_count = Banner::where('status',true)->count();
        if($banner_count > 0)
        {
            $banners = Banner::where('status',true)->get()->toArray();;
            $banners_arr = [];
            $arr = [];
            foreach ($banners as $key => $value) {
            	$banners_arr['id'] = $value['id'];
            	$banners_arr['type'] = $value['type'];
                $banners_arr['action_id'] = $value['action_id'];
                $banners_arr['image'] = $value['image'];
                $arr[] = $banners_arr;
            //dd($banners_arr);
            }
            
            $success['banners'] = $arr;

        }
        return $this->sendResponse($success,'Get all brand,category and banner fetch successfully'); 
        if(!$brand_count >0 && !$banner_count>0 && !$category_count>0){   
            return $this->sendError('There is no record found in Brands,Categories and Banners');
        }
    }
}
