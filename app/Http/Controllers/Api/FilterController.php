<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Color;
use App\Models\Size;
use App\Models\Category;
use App\Models\Brand;

class FilterController extends BaseController
{
    public function FilterList()
    {
    	$color_count = Color::where('is_active',true)->count();
        if($color_count > 0)
        {
            $colors = Color::where('is_active',true)->get()->toArray();

            $colors_arr = [];
            $arr = [];
            foreach ($colors as $key => $value) {
            	$colors_arr['id'] = $value['id'];
            	$colors_arr['name'] = $value['name'];
            	$colors_arr['hexa_code'] = $value['hexa_code'];
                $arr[] = $colors_arr;
            }
            
            $success['colors'] = $arr;
        }

        $size_count = Size::where('is_active',true)->count();
        if($size_count > 0)
        {
            $sizes = Size::where('is_active',true)->get()->toArray();

            $sizes_arr = [];
            $arr = [];
            foreach ($sizes as $key => $value) {
            	$sizes_arr['id'] = $value['id'];
            	$sizes_arr['name'] = $value['name'];
                $arr[] = $sizes_arr;
            }
            
            $success['sizes'] = $arr;
        }

        $category_count = Category::where('is_active',true)->count();
        if($category_count > 0)
        {
            $categories = Category::where('is_active',true)->get()->toArray();
            // dd($categories);
            $array = array_unique(array_column($categories, 'name'));
            $unique_data = array_intersect_key($categories, $array);

            //dd($unique_data);
            $categories_arr = [];
            $arr = [];
            foreach ($unique_data as $key => $value) {
            	$categories_arr['id'] = $value['id'];
                $categories_arr['name'] = $value['name'];
                $arr[] = $categories_arr;
            }
            
            $success['categories'] = $arr;
        }

    	$brand_count = Brand::where('is_active',true)->count();
        if($brand_count > 0)
        {
            $brands = Brand::where('is_active',true)->get()->toArray();

            $brands_arr = [];
            $arr = [];
            foreach ($brands as $key => $value) {
            	$brands_arr['id'] = $value['id'];
            	$brands_arr['name'] = $value['name'];
                $arr[] = $brands_arr;
            }
            //dd($arr);
            
            $success['brands'] = $arr;
        }
        return $this->sendResponse($success,'Get all brand,category,color and size fetch successfully'); 
        if(!$brand_count >0 && !$size_count>0 && !$category_count>0 && !$color_count>0){   
            return $this->sendError('There is no record found in Brands,Categories,Colors and Sizes');
        }
    }
}
