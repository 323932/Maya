<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\User;
use App\Models\Shipment;
use App\Models\ShipmentProduct;
use Illuminate\Support\Facades\Auth;
use Validator,Mail;

class OrderController extends BaseController
{
	public function placeOrder(Request $request){
		$validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'seller_id' => 'required',
            'billing_name' => 'required',
            'billing_address' => 'required',
            'billing_area' => 'required',
            'billing_state' => 'required',
            'billing_city' => 'required',
            'billing_pincode' => 'required',
            'billing_email' => 'required',
            'billing_mobile' => 'required',
            'billing_company_name' => 'required',
            'shipping_name' => 'required',
            'shipping_company_name' => 'required',
            'shipping_mobile_no' => 'required',
            'shipping_email' => 'required',
            'shipping_address' => 'required',
            'shipping_state' => 'required',
            'shipping_city' => 'required',
            'shipping_pincode' => 'required',
            'shipping_area' => 'required',
            'gstno' => 'required',
            'coupon_code' => 'required',
            'discount' => 'required',
            'order_price' => 'required',
            'payment_type' => 'required',
            'tax' => 'required',
            'order_product'=>'required',

        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $date = strtotime("+7 day");
        $date = date('Y-m-d', $date);
        
        $last_order = Order::select('order_no')->orderBy('id','desc')->first();
        $order_no = 'MY'. str_pad((substr($last_order['order_no'],2,8))+1,7,'0',STR_PAD_LEFT);
        // dd($order_no);
        $order = new Order();
        $order->fill($input);
        $order->name = $input['billing_name'];
        $order->email = $input['billing_email'];
        $order->mobile = $input['billing_mobile'];
        $order->order_no = $order_no;
        $order->order_type = "full";
        $order->shipping_price = 0.00;
        $order->delivery_date = $date;
        $order->save();

        $order_products = json_decode($input['order_product'],true);
        
        foreach ($order_products as $key => $value) {
            $OrderProduct = new OrderProduct();
            $OrderProduct->fill($value);
            $OrderProduct->order_id = $order->id;
            $OrderProduct->save();
        }
        $success['order_no'] = $order['order_no'];
        $success['price'] = $order['order_price'];
        $success['order_date'] = $order['created_at'];
        //mail to shipping email
        // $user_data = User::where('id',$input['user_id'])->first();

        // if($user_data['type'] == 'Company Name' || $user_data['type'] == 'seller'){
            
        //     $view = 'apimail.dealer_place_order';
        //     $subject = 'ORDER CONFIRMED';
        //     $email = $input['shipping_email'];
            
        //     $data['order_no'] = $order['order_no'];
        //     $data['payment_mode'] = $order['payment_type'];
        //     $data['delivery_date'] = date('D,F d,Y',strtotime($order['delivery_date']));
        //     $data['shipping_address'] = $order['shipping_address'];
        //     $data['shipping_city'] = $order['shipping_city'];
        //     $data['shipping_pincode'] = $order['shipping_pincode'];
        //     $data['shipping_area'] = $order['shipping_area'];
        //     $data['shipping_state'] = $order['shipping_state'];

        //     $order_detail = Order::where('orders.id',$order['id'])->with('orderproduct')->first()->toArray();
        //     $order_item  = [];
        //     foreach ($order_detail['orderproduct'] as $key => $value) {
        //         $order_item[$key]['name'] = $value['product_name'];
        //         $order_item[$key]['qty'] = $value['qty_s'] + $value['qty_m'] + $value['qty_l'] + $value['qty_xl'];
        //     }
        //     $data['order_item'] = $order_item;

        //     Mail::send($view,['data'=>$data], function ($message) use ($email,$subject) {
        //             $message->to($email);
        //             $message->subject($subject);
        //             $message->from('Apporders@mayaclothing.com');
        //     });
        // }   

        return $this->sendResponse($success,"Order Created Successfully");
	}
    public function getMyOrders(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'type' => 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();

        $user_id = $input['user_id'];
        $type = $input['type'];
        if($type == 'seller'){
            $order_count = Order::where('seller_id',$user_id)->count();   
        }else{
            $order_count = Order::where('user_id',$user_id)->count();   
        }

        if($order_count > 0){
            if($type == 'seller'){
                $orders = Order::with('orderproduct')->where('seller_id',$user_id)->orderBy('id', 'DESC')->get()->toArray();
            }else{
                $orders = Order::with('orderproduct')->where('user_id',$user_id)->orderBy('id', 'DESC')->get()->toArray();
            }
            // dd($orders);
            $orders_data = [];
                // dd($orders);
            foreach ($orders as $key => $value) {
                $count = 0;
                // $total = 0;
                $orders_data[$key]['order_id'] = $value['id'];
                $orders_data[$key]['order_no'] = $value['order_no'];
                $total = 0;
                foreach ($value['orderproduct'] as $order_key => $order_value) {
                    $count = $order_value['qty_s'] + $order_value['qty_m'] + $order_value['qty_l'] + $order_value['qty_xl'];
                    $total = $total + $count;
                    $orders_data[$key]['no_of_items'] = $total; 
                }
                $orders_data[$key]['total_price'] = $value['order_price'] + $value['shipping_price'];
                $orders_data[$key]['order_status'] = $value['status'];
                $orders_data[$key]['shipment_status'] = $value['shipment_status'];
                $orders_data[$key]['order_date'] = $value['created_at'];

            }            
                    dd($orders_data);    
            $success['order_detail'] = $orders_data;

            return $this->sendResponse($success,'Get Order Fetch Successfully'); 
        }else{
            return $this->sendError('There is no record found in order for these user');
        }
    }
    public function getOrderDetail(Request $request){
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
        ]);
        
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $order_id = $input['order_id'];

        $arr = [];
        $shipment_detail = \DB::table('shipments')->selectRaw('shipments.id,shipments.shipping_title,shipments.shipping_number,shipments.charge,shipments.total_price,SUM(shipment_products.qty_s) as qty_s,SUM(shipment_products.qty_m) as qty_m,SUM(shipment_products.qty_l) as qty_l,SUM(shipment_products.qty_xl) as qty_xl')->where('shipments.order_id',$order_id)->leftjoin('shipment_products','shipment_products.shipment_id','=','shipments.id')->groupBy('shipments.id')->get()->toArray();

        //dd($shipment_detail);
        foreach ($shipment_detail as $key => $value) {
            // dd($value->id);
           $arr[$key]['shipment_id'] = $value->id;
           $arr[$key]['shippind_title'] = $value->shipping_title;
           $arr[$key]['shipping_number'] = $value->shipping_number;
           $arr[$key]['charge'] = $value->charge;
           $arr[$key]['total_price'] = $value->total_price;
           $arr[$key]['total_qty'] = $value->qty_s + $value->qty_m + $value->qty_l + $value->qty_xl; 
        }

        $order_count = Order::where('id',$order_id)->count();

        if($order_count > 0){
           $order_detail = Order::where('orders.id',$order_id)->with('orderproduct')->first()->toArray();

            $orders_data = [];
            $orders_data['order_id'] = $order_detail['id'];
            $orders_data['order_no'] = $order_detail['order_no'];
            $orders_data['order_date'] = $order_detail['created_at'];
            $orders_data['shipping_address'] = $order_detail['shipping_address'];
            $orders_data['shipping_name'] = $order_detail['shipping_name'];
            $orders_data['shipping_company_name'] = $order_detail['shipping_company_name'];
            $orders_data['shipping_mobile_no'] = $order_detail['shipping_mobile_no'];
            $orders_data['shipping_email'] = $order_detail['shipping_email'];
            $orders_data['shipping_state'] = $order_detail['shipping_state'];
            $orders_data['shipping_city'] = $order_detail['shipping_city'];
            $orders_data['shipping_pincode'] = $order_detail['shipping_pincode'];
            $orders_data['shipping_area'] = $order_detail['shipping_area'];
            
            $orders_data['billing_address'] = $order_detail['billing_address'];
            $orders_data['billing_name'] = $order_detail['name'];
            $orders_data['billing_company_name'] = $order_detail['billing_company_name'];
            $orders_data['billing_mobile_no'] = $order_detail['mobile'];
            $orders_data['billing_email'] = $order_detail['email'];
            $orders_data['billing_state'] = $order_detail['billing_state'];
            $orders_data['billing_city'] = $order_detail['billing_city'];
            $orders_data['billing_pincode'] = $order_detail['billing_pincode'];
            $orders_data['billing_area'] = $order_detail['billing_area'];
            
            $orders_data['payment_method'] = $order_detail['payment_type'];
            $orders_data['shipping_charges'] = $order_detail['shipping_price'];
            $orders_data['grand_total'] = $order_detail['order_price'];
            
            $order_item  = [];
            foreach ($order_detail['orderproduct'] as $key => $value) {
                $order_item[$key]['name'] = $value['product_name'];
                $order_item[$key]['color'] = $value['color_name'];
                $order_item[$key]['qty'] = $value['qty_s'] + $value['qty_m'] + $value['qty_l'] + $value['qty_xl'];
            }
            $orders_data['order_item'] = $order_item;
            $orders_data['shipment_detail'] =$arr;
            $success['order_detail'] = $orders_data;

            return $this->sendResponse($success,'Get Order Detail Fetch Successfully');  
        
        }else{
            return $this->sendError('There is no record found in order');
        }

    }
}
