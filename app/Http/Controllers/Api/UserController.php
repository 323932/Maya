<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Models\User;
use Mail;

class UserController extends BaseController
{
	public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'mobile' => 'required|unique:users,mobile',
            'gstno' => 'required',
            'address' => 'required',
            'area' => 'required',
            'city' => 'required',
            'pincode' => 'required',
            'state' => 'required',
            'company_name' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        
        $input['password'] = bcrypt($input['password']);
        
        $user = new User();
        $user->fill($input);
        // $user['remember_token'] = $user->createToken('MyApp')->accessToken;
        // $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->firstname;

        $user->save();        

        //mail to registered user
        $view = 'apimail.signup_mail';
        $subject = 'New User Added';
        $email = $input['email'];

        Mail::send($view,[], function ($message) use ($email,$subject) {
                $message->to($email);
                $message->subject($subject);
                $message->from('Apporders@mayaclothing.com');
        });
        
        return $this->sendResponse($success, 'User register successfully.');
    }
	public function login(Request $request){

		$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        	if(Auth::user()->status == true){
	            $user = Auth::user(); 
	            // $success['token'] =  $user->createToken('MyApp')-> accessToken; 
                $success['id'] =  $user->id;
                $success['type'] =  $user->type;
                $success['user_plan_id'] =  $user->user_plan_id;
                $success['firstname'] =  $user->firstname;
                $success['lastname'] =  $user->lastname;
                $success['gstno'] =  $user->gstno;
                $success['mobile'] =  $user->mobile;
                $success['area'] =  $user->area;
                $success['address'] =  $user->address;
                $success['pincode'] =  $user->pincode;
                $success['state'] =  $user->state;
                $success['city'] =  $user->city;
                $success['email'] =  $user->email;
                $success['company_name'] =  $user->company_name;
                $success['password'] =  $user->password;
                $success['status'] =  $user->status;

                return $this->sendResponse($success,'User Logged In successfully'); 
        	}else{
                return $this->sendError('Your account is not activated');   
        	}
        } 
        else{ 
            return $this->sendError('These credentials do not match our records.');    
        } 
	}

    public function checkUserStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $user_id = $input['user_id'];

        $user_count = User::where('id',$user_id)->count(); 
        
        if($user_count > 0){

            $users = User::find($user_id);
            $success['status'] = $users['status'];

            return $this->sendResponse($success,'User status fetch successfully'); 
        }
        return $this->sendError('There is no record found in users');
    }
    public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $email = $input['email'];

        if (User::where('email', '=', $email)->where('status',1)->count() > 0) {
            $view = 'apimail.forgotpassword_mail';
            $subject = 'Admin Reset Password Notification';
            Mail::send($view, [], function ($message) use ($email,$subject) {
                    $message->to($email);
                    $message->subject($subject);
                    $message->from('noreply@thinktanker.in');
            });
            return response()->json(['success'=>'Email link is send successfully'], 201);
        }else{

            return response()->json(['error'=>'Email is not exist'], 401); 
        }
    }
    public function getUserAddresses(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $user_id = $input['user_id'];

        $address_count = Address::where('user_id',$user_id)->count();

        if($address_count > 0){
            $address = Address::where('user_id',$user_id)->get()->toArray();
            $success['address'] = $address;

            return $this->sendResponse($success,"Get address of user fetch successfully");
        }
        return $this->sendError('There is no record found in user address');
    }
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'old_password' => 'required',
            'new_password' => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $input = $request->all();
        $user = User::find($input['user_id']);
        $email = $user['email'];

        if(Auth::attempt(['email' => $email, 'password' => $input['old_password']])){
            if(Auth::user()->status == true){
                
                $user->password = bcrypt($input['new_password']);
                $user->save();

                $success['id'] =  $user->id;
                $success['type'] =  $user->type;
                $success['user_plan_id'] =  $user->user_plan_id;
                $success['firstname'] =  $user->firstname;
                $success['lastname'] =  $user->lastname;
                $success['gstno'] =  $user->gstno;
                $success['mobile'] =  $user->mobile;
                $success['area'] =  $user->area;
                $success['email'] =  $user->email;
                $success['password'] =  $user->password;
                $success['status'] =  $user->status;
                
                return $this->sendResponse($success,'User password change successfully'); 
            }else{
                return $this->sendError('Your account is not activated');  
            }
        }else{ 
            return $this->sendError('These credentials do not match our records.');    
        }
    }
    public function editProfile(Request $request){
        $input = $request->all();
        
        $user_id = '';
        if(isset($input['user_id'])){
            $user_id = $input['user_id'];
        }
        
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users,email,'.$user_id,
            'mobile' => 'required|unique:users,mobile,'.$user_id,
            'gstno' => 'required|unique:users,gstno,'.$user_id,
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        $user_count = User::where('id',$user_id)->count();

        if($user_count > 0){
            $user = User::find($user_id);
            
            $user->fill($input);
            $user->save();
            
            $success['id'] =  $user->id;
            $success['firstname'] =  $user->firstname;
            $success['lastname'] =  $user->lastname;
            $success['gstno'] =  $user->gstno;
            $success['mobile'] =  $user->mobile;
            $success['email'] =  $user->email;

            return $this->sendResponse($success,'User profile has been updated successfully'); 
        }else{
            return $this->sendError('There is no record found in user');   
        }
    }
    public function getShopOwner(Request $request){
        $user_count = User::where('status',1)->where('type','Company Name')->get()->count();
        if($user_count > 0){
            
            $user_data = User::select('id','type','user_plan_id','company_name','firstname','lastname','state','gstno','mobile','address','pincode','city','area','email')->where('status',1)->where('type','Company Name')->get()->toArray();
            
            $success['user_data'] = $user_data;

            return $this->sendResponse($success,"Get Shop owner in user fetch successfully");
        }
        return $this->sendError('There is no record found in user of shop owner');
    }

}