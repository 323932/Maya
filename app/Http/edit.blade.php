@extends('admin.layout.layout')
@section('start_form')
    <?= Form::model($order_data,['route'=>['orders.update',$id],'role'=>'form','class'=>'m-0','files'=>true,'method'=>'PATCH']) ?>
@stop
@section('top_fixed_content')
    <?=Html::style('backend/css/jquery.fileuploader.css')?>
    <?=Html::style('backend/css/jquery.fileuploader-theme-thumbnails.css')?>
    <?=Html::style('backend/css/bootstrap-fileupload.css')?>
    <?= Html::style('backend/css/datepicker.css') ?>
    <?= Html::style('backend/css/custome.css') ?>
    <nav class="navbar navbar-static-top">
        <div class="title">
            <h4></h4>
        </div>        
        <div class="pl-10">
            <button type="button" name="save_button" class="btn btn-primary btn-sm disabled-btn" id="shipment" title="Save and add new">Add Shipment</button>
            <button type="button" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit" id="invoice">Add Invoice</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn save_invoice" title="Save and exit" disabled="true">Save</button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Back to users Page">Back</a>
        </div>
    </nav>
@stop 
@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('user_id')) {{ 'has-error' }} @endif">
                            <label>Order Date <sup class="text-danger">*</sup></label>
                            <?=Form::text('created_at',old('created_at'), ['class' => 'form-control created_at','id' => 'created_at','placeholder'=>'Order Date','readOnly'=>true]);?>
                            <span id="category_id_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('payment_type')) {{ 'has-error' }} @endif">
                            <label>Order Status<sup class="text-danger">*</sup></label>
                            <?=Form::text('status',old('status'), ['class' => 'form-control status','id' => 'status','readOnly'=>true]);?>
                            <span id="sku_error" class="help-inline text-danger"><?= $errors->first('payment_type') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('order_price')) {{ 'has-error' }} @endif">
                            <label>Dealer Name<sup class="text-danger">*</sup></label>
                            <?= Form::text('user_id', null, ['class' => 'form-control','id'=>'user_id','readOnly'=>true]); ?>
                            <span id="order_price_error" class="help-inline text-danger"><?= $errors->first('user_id') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('email')) {{ 'has-error' }} @endif">
                            <label>Email<sup class="text-danger">*</sup></label>
                            <?=Form::text('email',old('email'), ['class' => 'form-control email','id' => 'email','readOnly'=>true]);?>
                            <span id="tax_error" class="help-inline text-danger"><?= $errors->first('email') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('category')) {{ 'has-error' }} @endif">
                            <label>Category<sup class="text-danger">*</sup></label>
                            <?= Form::text('category', null, ['class' => 'form-control','readOnly'=>true]); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('category') ?></span>
                        </div>
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('payment_type')) {{ 'has-error' }} @endif">
                            <label>Payment Type<sup class="text-danger">*</sup></label>
                            <?= Form::text('payment_type', null, ['class' => 'form-control','readOnly'=>true]); ?>
                            <span id="name_error" class="help-inline text-danger"><?= $errors->first('payment_type') ?></span>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>    
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Billing Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('billing_address')) {{ 'has-error' }} @endif">
                            <label>Address<sup class="text-danger">*</sup></label>
                            <?= Form::textarea('billing_address', null, ['class' => 'form-control', 'placeholder' => 'Address','rows'=>6,'readOnly'=>true]); ?>
                            <span id="billing_address_error" class="help-inline text-danger"><?= $errors->first('billing_address') ?></span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_area')) {{ 'has-error' }} @endif">
                                    <label>Area<sup class="text-danger">*</sup></label>
                                    <?= Form::text('billing_area', null, ['class' => 'form-control', 'placeholder' => 'Area','readOnly'=>true]); ?>
                                    <span id="billing_area_error" class="help-inline text-danger"><?= $errors->first('billing_area') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_state')) {{ 'has-error' }} @endif">
                                    <label>State<sup class="text-danger">*</sup></label>
                                     <?=Form::text('billing_state',old('billing_state'), ['class' => 'form-control status','id' => 'billing_state','placeholder'=>'Select State','readOnly'=>true]);?>
                                    <span id="billing_state_error" class="help-inline text-danger"><?= $errors->first('billing_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_city')) {{ 'has-error' }} @endif">
                                    <label>City<sup class="text-danger">*</sup></label>
                                     <?=Form::text('billing_city',old('billing_city'), ['class' => 'form-control status','id' => 'billing_city','placeholder'=>'Select City','readOnly'=>true]);?>
                                    <span id="billing_city_error" class="help-inline text-danger"><?= $errors->first('billing_city') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('billing_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode<sup class="text-danger">*</sup></label>
                                    <?= Form::text('billing_pincode', null, ['class' => 'form-control number_only', 'placeholder' => 'Pincode','maxlength'=>6,'readOnly'=>true]); ?>
                                    <span id="billing_pincode_error" class="help-inline text-danger"><?= $errors->first('billing_pincode') ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="card-sub-title">
                    <h4>Shipping Address</h4>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('billind_address')) {{ 'has-error' }} @endif">
                            <label>Address<sup class="text-danger">*</sup></label>
                            <?= Form::textarea('shipping_address', null, ['class' => 'form-control', 'placeholder' => 'Address','rows'=>5,'readOnly'=>true]); ?>
                            <span id="billind_address_error" class="help-inline text-danger"><?= $errors->first('billind_address') ?></span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_area')) {{ 'has-error' }} @endif">
                                    <label>Area<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_area', null, ['class' => 'form-control', 'placeholder' => 'Area','readOnly'=>true]); ?>
                                    <span id="shipping_area_error" class="help-inline text-danger"><?= $errors->first('shipping_area') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_state')) {{ 'has-error' }} @endif">
                                    <label>State<sup class="text-danger">*</sup></label>
                                     <?=Form::text('shipping_state',old('shipping_state'), ['class' => 'form-control status','id' => 'shipping_state','placeholder'=>'Select State','readOnly'=>true]);?>
                                    <span id="shipping_state_error" class="help-inline text-danger"><?= $errors->first('shipping_state') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_city')) {{ 'has-error' }} @endif">
                                    <label>City<sup class="text-danger">*</sup></label>
                                     <?=Form::text('shipping_city',old('shipping_city'), ['class' => 'form-control status','id' => 'shipping_city','placeholder'=>'Select City','readOnly'=>true]);?>
                                    <span id="shipping_city_error" class="help-inline text-danger"><?= $errors->first('shipping_city') ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if($errors->has('shipping_pincode')) {{ 'has-error' }} @endif">
                                    <label>Pincode<sup class="text-danger">*</sup></label>
                                    <?= Form::text('shipping_pincode', null, ['class' => 'form-control number_only', 'placeholder' => 'Pincode','maxlength'=>6,'readOnly'=>true]); ?>
                                    <span id="shipping_pincode_error" class="help-inline text-danger"><?= $errors->first('shipping_pincode') ?></span>
                                </div>
                            </div>
                </div>
            </div>
        @include('admin.layout.overlay')
        </div>
    </div>
    <div class="card ">
        <div class="card-body">
            <div class="row">
                <h4>Line Items</h4>
                <div class="col-md-12">
                    <table style="width: 100%" class="table table-bordered">
                        <tr>
                            <td rowspan="2"> Product Name </td>
                            <td rowspan="2"> SKU </td>
                            <td rowspan="2"> Brand Name </td>
                            <td> Quantity </td>
                            <td rowspan="2"> Price </td>
                            <td class="qty_ship">Qty to Ship</td>
                            <td rowspan="2" class="price_ship"> Price </td>
                        </tr>
                        <tr>
                            <td style="padding: 0">
                                <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                    <tr>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px ">S</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px  ">M</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px  ">L</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 28px  ">XL</td>
                                    </tr>
                                </table>
                            </td>
                            <td style="padding: 0" class="qty_ship">
                                <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                    <tr>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px ">S</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px  ">M</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 28px  ">L</td>
                                        <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 28px  ">XL</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        @foreach($order_data_info as $key=>$value)
                            <tr>
                                <td>{{$value['product_name']}}</td>
                                <td>{{$value['sku']}}</td>
                                <td>{{$value['brand_name']}}</td>
                                <td style="padding: 0">
                                    <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                        <tr>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px" id="fix_s<?=$value['id']?>">{{$value['qty_s']}}</td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px" id="fix_m<?=$value['id']?>">{{$value['qty_m']}}</td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px" id="fix_l<?=$value['id']?>">{{$value['qty_l']}}</td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 1px" id="fix_xl<?=$value['id']?>">{{$value['qty_xl']}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>{{$value['order_price']}}</td>
                                <td style="padding: 0" class="qty_ship">
                                    <table class="table table-bordered" style="margin-bottom: 0;border: medium none;">
                                        <tr>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px">
                                                <input type="text" id="qty_s_<?=$value['id']?>" name="ship_qty_s" style="width:45px;" min="0" data-attr="{{$value['qty_s']}}" class="ship_qty_s number_only" data-price="{{$value['order_price']}}" data-id="{{$value['id']}}" placeholder="0"/>
                                            </td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px"><input type="text" id="qty_m_<?=$value['id']?>" name="ship_qty_m" style="width:45px;" data-attr="{{$value['qty_m']}}" class="ship_qty_m number_only" min="0" data-price="{{$value['order_price']}}" data-id="{{$value['id']}}" placeholder="0"/></td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;width: 1px"><input type="text" id="qty_l_<?=$value['id']?>" name="ship_qty_l" class="ship_qty_l number_only"  data-attr="{{$value['qty_l']}}" style="width:45px;" min="0" data-price="{{$value['order_price']}}" data-id="{{$value['id']}}" placeholder="0"/></td>
                                            <td style="border-top: medium none;border-left:medium none;border-bottom: medium none;border-right:medium none;width: 1px"><input type="text" id="qty_xl_<?=$value['id']?>" name="ship_qty_xl"  class="ship_qty_xl number_only" data-attr="{{$value['qty_xl']}}" style="width:45px;" min="0" data-price="{{$value['order_price']}}" data-id="{{$value['id']}}" placeholder="0"/></td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="price_ship" id="{{$value['id']}}">0.00</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="card shipment_detail">
        <div class="card-body">
            <div class="row">
                <h4>Shipping Information</h4>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('shipping_title')) {{ 'has-error' }} @endif">
                            <label>Title<sup class="text-danger">*</sup></label>
                            <?=Form::text('shipping_title',old('shipping_title'), ['class' => 'form-control shipping_title','id' => 'shipping_title','placeholder'=>'Title']);?>
                            <span id="shipping_title_error" class="help-inline text-danger"><?= $errors->first('shipping_title') ?></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @if($errors->has('shipping_number')) {{ 'has-error' }} @endif">
                            <label>Number<sup class="text-danger">*</sup></label>
                            <?=Form::text('shipping_number',old('shipping_number'), ['class' => 'form-control shipping_number','id' => 'shipping_number']);?>
                            <span id="shipping_number_error" class="help-inline text-danger"><?= $errors->first('shipping_number') ?></span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="card invoice_detail">
        <div class="card-body">
            <div class="row">
                <h4>Upload Invoice</h4>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="inputfile-box" id="product_image_error_div">
                            <input class="inputfile ng-isolate-scope" id="file" type="file" name="file">
                            <label for="file"><span class="file-box" id="file-name"></span><span class="file-button">Upload</span></label>
                            <span id="file_error" class="help-inline text-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="text-right">
        <button type="submit" name="save_button" value="save_new" class="btn btn-primary btn-sm disabled-btn" title="Save and add new">Save & New</button>
            <button type="submit" name="save_button" value="save_exit" class="btn btn-primary btn-sm disabled-btn" title="Save and exit">Save & exit </button>
            <a href="<?= route('orders.index') ?>" class="btn btn-default btn-sm" title="Back to users Page">Cancel</a>
    </div> -->
@stop
@section('end_form')
    <?= Form::close() ?>
@stop

@section('script') 
    <script type="text/javascript">
        $(".number_only").keypress(function(h){if(8!=h.which&&0!=h.which&& 32!=h.which&&(h.which<48||h.which>57))return!1});
        $('.shipment_detail').css('display','none');
        $('.qty_ship').css('display','none');
        $('.invoice_detail').css('display','none');
        // $('.price_ship').css('display','none');
        $('#shipment').click( function(){
            $('.shipment_detail').css('display','block');
            $('.qty_ship').css('display','block');
            // $('.price_ship').css('display','block');

        });
        $('#invoice').click(function(){
            $('.invoice_detail').css('display','block');
        });
        $('#file').change(function(){
            $('#file-name').text($(this)[0].files[0].name);
        });

        function calculate_total(val,msg){
            var value = val.val();
            var min_val = parseInt(val.attr('data-attr'));
            var price = parseInt(val.attr('data-price'));
            var id = val.attr('data-id');
            // for fix quantity
            var fix_s = $('#fix_s'+id).text();
            var fix_m = $('#fix_m'+id).text();
            var fix_l = $('#fix_l'+id).text();
            var fix_xl = $('#fix_xl'+id).text();
            var fix_total_qty = parseInt(fix_s) + parseInt(fix_m) + parseInt(fix_l) + parseInt(fix_xl);
            
            // for per quantity value
            var price = (price/fix_total_qty);
            var s = ($('#qty_s_'+id).val() != '') ? $('#qty_s_'+id).val() : 0;
            var m = ($('#qty_m_'+id).val() != '') ? $('#qty_m_'+id).val() : 0;
            var l = ($('#qty_l_'+id).val() != '') ? $('#qty_l_'+id).val() : 0;
            var xl = ($('#qty_xl_'+id).val() != '') ? $('#qty_xl_'+id).val() : 0;
            var total_qty = parseInt(s) + parseInt(m) + parseInt(l) + parseInt(xl);
            
            $('#'+id).text(((price)*total_qty).toFixed(2));

            if(value > min_val){
                $('.save_invoice').attr('disabled',true);
                toastr.error(msg);
            }else{
                $('.save_invoice').attr('disabled',false);
            }
        }

        $('.ship_qty_s').keyup(function(){
            var msg = 'Ship Qty S is not greater than qty';
            calculate_total($(this),msg);
        });
        $('.ship_qty_m').keyup(function(){
            var msg = 'Ship Qty M is not greater than qty';
            calculate_total($(this),msg);
        });
        $('.ship_qty_l').keyup(function(){
            var msg = 'Ship Qty L is not greater than qty';
            calculate_total($(this),msg);
        });
        $('.ship_qty_xl').keyup(function(){
            var msg = 'Ship Qty XL is not greater than qty';
            calculate_total($(this),msg);
        });
    </script> 
    @include('admin.layout.alert')
@stop