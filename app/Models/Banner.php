<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = "banners";
    protected $fillable = ['id','title','description','type','status','action_id','image','order_position'];

    public function getImageAttribute($value){
    	return LOCAL_IMAGE_PATH.'banners/'.$value;
    }
}
