<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $table = "shipments";
    protected $fillable = ['id','shipping_title','shipping_number','charge','order_id','total_price','invoice'];
}