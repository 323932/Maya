<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
   protected $table = "brands";
   protected $fillable = ['id','name','image','logo_image','description','is_active','hex_code','username','title_color','order_position'];
}
