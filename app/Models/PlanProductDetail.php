<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanProductDetail extends Model
{
    protected $table = "plan_product_details";
    protected $fillable = ['id','user_plan_id','product_id','is_visible'];
}
