<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';
    protected $fillable = ['id','product_id','image'];

    public function getImageAttribute($value){
    	return LOCAL_IMAGE_PATH.'products/'.$value;
    }
}
