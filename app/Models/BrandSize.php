<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSize extends Model
{
   protected $table = "brand_sizes";
   protected $fillable = ['brand_id','size_id'];
}
