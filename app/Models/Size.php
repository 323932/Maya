<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = "sizes";
    protected $fillable = ['name','is_active'];

    public function getnameAttribute($name)
    {
    	$set_name = strtoupper($name);
    	return $set_name;
    }
}
