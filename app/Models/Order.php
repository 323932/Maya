<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderProduct;

class Order extends Model
{
   protected $table = "orders";
   protected $fillable = ['id','user_id','name','billing_address','billing_area','billing_state','billing_city','billing_pincode','billing_company_name','shipping_name','shipping_company_name','shipping_address','shipping_mobile_no','shipping_email','discount','coupon_code','shipping_state','shipping_city','shipping_pincode','shipping_area','email','mobile','gstno','status','order_type','order_price','shipping_price','parent_order_id','payment_type','delivery_date','tax','tax_type','shipment_status','seller_id','grand_total'];

   public function orderproduct()
	{
		return $this->hasMany('App\Models\OrderProduct','order_id','id');
	}
	public function getCreatedAtAttribute($created_at){
    	$date = date('d-m-Y',strtotime($created_at));
    	return $date;
    }
}
