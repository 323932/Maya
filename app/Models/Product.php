<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="products";
    protected $fillable = ['id','category_id','brand_id','sku','name','original_price','description','is_available','color_id','attribute','information','qty_s','qty_m','qty_l','qty_xl','is_set_available','tax','percentage','status','collection'];

    public function productimage()
	{
		return $this->hasMany('App\Models\ProductImage','product_id','id');

	}
	public function color()
	{
		return $this->hasMany('App\Models\Color','id','color_id');

	}
	public function defaultproductimage()
	{
		return $this->hasMany('App\Models\ProductImage','product_id','id')->where('is_default','=', 1);

	}
	public function productvariant()
	{
		return $this->hasMany('App\Models\ProductVariant','product_id','id');
	}
}
