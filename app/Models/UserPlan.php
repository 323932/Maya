<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    protected $table="user_plans";
    protected $fillable= ['id','user_plan','discount','brand_id','category_id'];
}
