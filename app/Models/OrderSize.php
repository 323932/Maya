<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSize extends Model
{
    protected $table = "order_sizes";
    protected $fillable = ['order_id','product_id','order_product_id','brand_id','size_id','qty'];
}
