<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
   protected $table = "order_products";

   protected $fillable = ['id','order_id','product_id','product_name','brand_id','category_id','qty_s','qty_m','qty_l','qty_xl','color_id','color_name','percentage	'];
}

