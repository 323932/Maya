<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notifications";
    protected $fillable = ['id','title','description','type','name','status','action_id','image'];

    public function getImageAttribute($value){
    	return LOCAL_IMAGE_PATH.'notification/'.$value;
    }
}
