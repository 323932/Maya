<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";
    protected $fillable = ['id','type','user_plan_id','firstname','lastname','state','gstno','mobile','area','email','password','status','pincode','city','state','address','notification_id','company_name','created_at'];

}
