<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoupanCode extends Model
{
    protected $table = "coupan_codes";
    protected $fillable = ['id','user_id','code','coupon_type','value','time','to_date','from_date','percentage','discount'];

    public function settoDateAttribute($value) {
		$this->attributes['to_date'] = date('Y-m-d', strtotime($value));
	}

	public function setfromDateAttribute($value) {
		$this->attributes['from_date'] = date('Y-m-d', strtotime($value));
	}

	public function gettoDateAttribute($value) {
		return date('d-m-Y', strtotime($value));
	}

	public function getfromDateAttribute($value) {
		return date('d-m-Y', strtotime($value));
	}
}
