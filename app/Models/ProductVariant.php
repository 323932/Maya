<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $table = 'product_variants';
    protected $fillable = ['id','product_id','color_id','qty_s','qty_m','qty_l','qty_xl'];

    
}
