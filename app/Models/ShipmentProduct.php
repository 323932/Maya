<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShipmentProduct extends Model
{
    protected $table = "shipment_products";
    protected $fillable = ['id','shipment_id','product_id','qty_s','qty_m','qty_l','qty_xl','price'];
}